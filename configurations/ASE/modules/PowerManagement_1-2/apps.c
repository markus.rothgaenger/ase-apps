/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   ASE configuration application container.
 */

#include "apps.h"
#include <math.h>
#include <canbridge.h>
#include <ring.h>
#include <touchsensors.h>
// #include <ProximityMapping.h>
#include <RandomWalk.h>
/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Forward declarations
 */
// static int _printPath_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
// static int _printMap_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
// static int _printCorners_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
// static int _setGains_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _ring_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);

/*
 *   Shell commands
 */
// static AOS_SHELL_COMMAND(_printPath_shellcmd, "PM:printPath", _printPath_shellcb);
// static AOS_SHELL_COMMAND(_printMap_shellcmd, "PM:printMap", _printMap_shellcb);
// static AOS_SHELL_COMMAND(_printCorners_shellcmd, "PM:printCorners", _printCorners_shellcb);
// static AOS_SHELL_COMMAND(_setGains_shellcmd, "PM:setGains", _setGains_shellcb);
static AOS_SHELL_COMMAND(_ring_shellcmd, "RING:Test", _ring_shellcb);
static AOS_SHELL_COMMAND(_can_shellcmd, "CAN:Test", _can_shellcb);

/*
 * Topics for the different data
 */
static urt_topic_t _proximity_ring_topic;
static urt_topic_t _ambient_ring_topic;
static urt_topic_t _proximity_floor_topic;
static urt_topic_t _ambient_floor_topic;
static urt_topic_t _odom_topic;
static urt_topic_t _touch_topic;

/*
 * Payloads of the different data
 */
static ring_sensors_t ring_proximity_payload;
static ring_sensors_t ring_ambient_payload;
static floor_sensors_t proximity_floor_payload;
static floor_sensors_t ambient_floor_payload;
static position_cv_si odom_payload;
static touch_sensors_t touch_payload;

/*
 * Driver frequency (in Hz).
 */
const float frequency = 50;

/*
 * Ring node object.
 */
static ring_node_t _ring;

/*
 * CAN node object of the PowerManagement.
 */
static can_node_t _can;

/*
 * TouchSensors node object.
 */
static touchsensors_node_t _touchSensors;

/*
 * LineFollowing node object.
 */
// static linefollowing_node_t _lf;

/*
 * HelloWorld node object.
 */
// static ProximityMapping_node_t _pm;
static RandomWalk_node_t _rw;

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
static int _ring_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  (void)stream;
  return AOS_OK;
}

static int _can_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  (void)argc;
  (void)argv;
  (void)stream;

  urtPrintf("Ring amb: %u \t prox: %u \n", _can.sub_data[1].ring_data.values.data[0], _can.sub_data[0].ring_data.values.data[0]);
  return AOS_OK;
}

// static int _printMap_shellcb(BaseSequentialStream* stream, int argc, char* argv[]) {
//   return pmPrintMap(&_pm, stream, argc, argv);
// }
// static int _printCorners_shellcb(BaseSequentialStream* stream, int argc, char* argv[]) {
//   return pmPrintCorners(&_pm, stream, argc, argv);
// }
// static int _setGains_shellcb(BaseSequentialStream* stream, int argc, char* argv[]) {
//   return pmSetGains(&_pm, stream, argc, argv);
// }
// static int _printPath_shellcb(BaseSequentialStream* stream, int argc, char* argv[]) {
//   return pmPrintPath(&_pm, stream, argc, argv);
// }
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all App from the PowerManagement
 */
void appsInit(void)
{
  //initialize all topics
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &ring_proximity_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &ring_ambient_payload);
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &proximity_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &ambient_floor_payload);
  urtTopicInit(&_odom_topic, ODOM_TOPICID, &odom_payload);
  urtTopicInit(&_touch_topic, TOUCH_SENSORS_TOPICID, &touch_payload);

#if (CAN_SUB_PROX_ENABLE == true)
  //Subscriber subscribes to the proximity ring Topic and should transmit to the ring id
  _can.sub_data[0].mask = TRIGGEREVENT;
  _can.sub_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.sub_data[0].size = sizeof(ring_proximity_payload);
#endif /* CAN_SUB_PROX_ENABLE == true */

#if (CAN_SUB_AMB_ENABLE == true)
  //Subscriber subscribes to the ambient ring Topic and should transmit to the ring id
  _can.sub_data[1].mask = TRIGGEREVENT;
  _can.sub_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.sub_data[1].size = sizeof(ring_ambient_payload);
#endif /* CAN_SUB_AMB_ENABLE == true */

#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
  //Publisher gets data from the floor and publishs them to the floor Topic
  _can.pub_floor_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.pub_floor_data[0].size = sizeof(proximity_floor_payload);
#endif /* CAN_PUB_PROX_FLOOR_ENABLE == true */

#if (CAN_PUB_AMB_FLOOR_ENABLE == true)
  _can.pub_floor_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.pub_floor_data[1].size = sizeof(ambient_floor_payload);
#endif /* CAN_PUB_AMB_FLOOR_ENABLE == true */

#if (CAN_PUB_ODOM_ENABLE == true)
  _can.pub_odom_data.topicid = ODOM_TOPICID;
  _can.pub_odom_data.size = sizeof(odom_payload);
#endif /* CAN_PUB_ODOM_ENABLE == true */

#if (CAN_LIGHT_SERVICE_ENABLE == true)
  //Service gets light request from the ring
  _can.light_service_data.id = LIGHT_SERVICE_ID;
#endif /* CAN_LIGHT_SERVICE_ENABLE == true */
#if (CAN_MOTOR_SERVICE_ENABLE == true)
  //Service gets motor request from the ring
  _can.motor_service_data.id = DMC_SERVICE_ID;
#endif /*CAN_MOTOR_SERVICE_ENABLE == true */

#if (CAN_PUB_PROX_FLOOR_ENABLE == true && CAN_PUB_AMB_FLOOR_ENABLE == true && CAN_PUB_ODOM_ENABLE == true)
  id_map_t ids;
  ids.cnt = 0;
  ids.id = ~(_can.pub_floor_data[0].topicid ^ _can.pub_floor_data[1].topicid ^ _can.pub_odom_data.topicid); //ID must match
  ids.type = ~0; //Type must match

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_floor_data[0].topicid | _can.pub_floor_data[1].topicid | _can.pub_odom_data.topicid;
  filter_id.type = CAN_TOPIC_DATA;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw; //Equal bit of these two masks must match other not
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw; // should only receive messages from this ID
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;
#endif /* (CAN_PUB_PROX_FLOOR_ENABLE == true && CAN_PUB_AMB_FLOOR_ENABLE == true && CAN_PUB_ODOM_ENABLE == true) */

  // initialize the CAN App
  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

  // set the ring driver frequency
  _ring.driver_data.frequency = frequency;
  // initialize the ring app
  ringInit(&_ring,
           AMBIENT_RING_TOPICID,
           PROXIMITY_RING_TOPICID,
           URT_THREAD_PRIO_NORMAL_MIN);

  // set the touch sensor driver frequency
  _touchSensors.driver_data.frequency = frequency;
  // initialize the Touch Sensors App
  touchsensorsInit(&_touchSensors,
                   TOUCH_SENSORS_TOPICID,
                   URT_THREAD_PRIO_NORMAL_MIN);

  // set the linefollowing services and data IDs  

  //set the helloworld services and data IDs
  // _pm.motor_data.service = &_can.motor_service_data.service;  
  // _pm.floor_prox.topicid = PROXIMITY_FLOOR_TOPICID;  
  // _pm.ring_prox.topicid = PROXIMITY_RING_TOPICID;
  // _pm.touch_data.topicid = TOUCH_SENSORS_TOPICID;
  // _pm.odom_data.topicid = ODOM_TOPICID;

  _rw.motor_data.service = &_can.motor_service_data.service;  
  _rw.floor_prox.topicid = PROXIMITY_FLOOR_TOPICID;  
  _rw.ring_prox.topicid = PROXIMITY_RING_TOPICID;
  _rw.touch_data.topicid = TOUCH_SENSORS_TOPICID;  
  _rw.floor_amb.topicid = AMBIENT_FLOOR_TOPICID;  
  // ProximityMappingInit(&_pm,
  //                URT_THREAD_PRIO_NORMAL_MIN);
  RandomWalkInit(&_rw, URT_THREAD_PRIO_NORMAL_MIN);

  // add shell commands
  // aosShellAddCommand(&_printMap_shellcmd);
  // aosShellAddCommand(&_printCorners_shellcmd);
  // aosShellAddCommand(&_printPath_shellcmd);
  // aosShellAddCommand(&_setGains_shellcmd);
  aosShellAddCommand(&_ring_shellcmd);
  aosShellAddCommand(&_can_shellcmd);  


  return;
}
