/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    ASE_urtconf.h
 * @brief   µRT configuration file for the ASE configuration.
 * @details Contains the configuration specific µRT settings.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef ASE_URTCONF_H
#define ASE_URTCONF_H

/*
 * compatibility guards
 */
#define _URT_CFG_
#define URT_CFG_VERSION_MAJOR                   0
#define URT_CFG_VERSION_MINOR                   1

#include <stdbool.h>

/*===========================================================================*/
/**
 * @name General parameters and options.
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Width of the urt_delay_t data type.
 *
 * @details Possible values are 32 and 64 bits.
 *          By definition time is represented in microseconds.
 */
#define URT_CFG_DELAY_WIDTH                     32

/**
 * @brief   Width of the urt_nodestage_t data type.
 *
 * @details Possible values are 8, 16, 32, and 64 bits.
 */
#define URT_CFG_NODESTAGE_WIDTH                 16

/**
 * @brief   Synchronization groups API enable flag.
 */
#define URT_CFG_SYNCGROUPS_ENABLED              false

/** @} */

/*===========================================================================*/
/**
 * @name General debug options.
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Debug enable flag.
 */
#define URT_CFG_DEBUG_ENABLED                   true

/** @} */

/*===========================================================================*/
/**
 * @name OSAL parameters and configuration.
 * @{
 */
/*===========================================================================*/

/*
 * URT_CFG_OSAL_HEADER is defined globally in middleware.mk file.
 */

/**
 * @brief   Flag to enable timeout functionality for condition variables.
 * @details If the OS supports this feature, urtPublisherPublish() can be called
            with a timeout. Otherwise it will block indefinitely under certain
            conditions.
 */
#define URT_CFG_OSAL_CONDVAR_TIMEOUT            false

/** @} */

/*===========================================================================*/
/**
 * @name Publish-subscribe (pubsub) configuration.
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Enable flag for the publish-subscribe system.
 */
#define URT_CFG_PUBSUB_ENABLED                  true

/**
 * @brief   Width of the urt_topicid_t data type.
 *
 * @details Possible values are 8, 16, 32, and 64 bits.
 */
#define URT_CFG_PUBSUB_TOPICID_WIDTH            16

/**
 * @brief   Flag to enable profiling of the publish-subscribe system.
 */
#define URT_CFG_PUBSUB_PROFILING                true

/**
 * @brief   Flag to enable deadline QoS for the publish-subscribe system.
 */
#define URT_CFG_PUBSUB_QOS_DEADLINECHECKS       true

/**
 * @brief   Flag to enable rate QoS for the publish-subscribe system.
 */
#define URT_CFG_PUBSUB_QOS_RATECHECKS           true

/**
 * @brief   Flag to enable jitter QoS for the publish-subscribe system
 */
#define URT_CFG_PUBSUB_QOS_JITTERCHECKS         true

/** @} */

/*===========================================================================*/
/**
 * @name Remote procedure calls (RPC) configuration.
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Enable flag for remote procedure calls.
 */
#define URT_CFG_RPC_ENABLED                     true

/**
 * @brief   Width of the urt_serviceid_t data type.
 *
 * @details Possible values are 8, 16, 32, and 64 bits.
 */
#define URT_CFG_RPC_SERVICEID_WIDTH             16

/**
 * @brief   Flag to enable profiling of remote procedure calls.
 */
#define URT_CFG_RPC_PROFILING                   true

/**
 * @brief   Flag to enable deadline QoS for remote procedure calls.
 */
#define URT_CFG_RPC_QOS_DEADLINECHECKS          true

/**
 * @brief   Flag to enable jitter QoS for remote procedure calls.
 */
#define URT_CFG_RPC_QOS_JITTERCHECKS            true

/** @} */

#endif /* ASE_URTCONF_H */
