/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   ASE configuration application container.
 */

#include "apps.h"
#include <math.h>
#include <amiroos.h>
#include <differentialmotionestimator.h>
#include <differentialmotorcontrol.h>
#include <canbridge.h>
#include <odometry.h>
#include <floor.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#if !defined(M_PI)
#define M_PI                                      3.14159265358979323846
#endif

/**
 * @brief   Wheel diameter.
 * @todo    Should be two values (left & right) stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_DIAMETER                            55710

/**
 * @brief   Wheel diameter.
 * @todo    Should be stored at EEPROM, so that individual robots can have individual values.
 */
#define WHEEL_OFFSET                              (34000 + (9000/2))

/**
 * @brief   Frequency of the DME.
 */
#define DME_FREQUENCY                             100

/**
 * @brief   Topic ID for DME motion information.
 */
#define DME_TOPIC_ID                              5

/**
 * @brief   Service ID for setting DMC target information.
 */
#define DMC_TARGETSERVICE_ID                      7

/**
 * @brief   Service ID for executing DMC auto calibration.
 */
#define DMC_CALIBSERVICE_ID                       8

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Forward declarations.
 */
static int _appsDmcShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[]);
static int _appsDmcShellCmdCb_setGains(BaseSequentialStream* stream, int argc, char* argv[]);
static int _appsDmcShellCmdCb_getGains(BaseSequentialStream* stream, int argc, char* argv[]);

/*
 * Shell commands.
 */
static AOS_SHELL_COMMAND(_appsDmcSehhCmd, "DMC:setVelocity", _appsDmcShellCmdCb);
static AOS_SHELL_COMMAND(_appsDmcShellCmd_setGains, "DMC:setGains", _appsDmcShellCmdCb_setGains);
static AOS_SHELL_COMMAND(_appsDmcShellCmd_getGains, "DMC:getGains", _appsDmcShellCmdCb_getGains);

/*
 * Topics for the different data.
 */
// static urt_topic_t _odom_reset_topic;
static urt_topic_t _dme_motion_topic;
static urt_topic_t _proximity_floor_topic;
static urt_topic_t _ambient_floor_topic;
static urt_topic_t _proximity_ring_topic;
static urt_topic_t _ambient_ring_topic;
static urt_topic_t _odom_topic;

/*
 * Payloads of the different data.
 */
// static odom_reset_t _odom_reset_payload;
static dme_motionpayload_t _dme_motion_topic_payload;
static floor_sensors_t _proximity_floor_payload;
static floor_sensors_t _ambient_floor_payload;
static ring_sensors_t _proximity_ring_payload;
static ring_sensors_t _ambient_ring_payload;
static position_cv_si _odom_topic_payload;

/*
 * DME instance.
 */
static dme_t _dme;

/*
 * DMC instance.
 */
static dmc_t _dmc;

/*
 * Floor node instance.
 */
static floor_node_t _floor;

/*
 * Driver frequency (in Hz) of the floor sensors.
 */
const float frequency = 50;

/*
 * CAN node instance.
 */
static can_node_t _can;

/*
 * Odometry node instance.
 */
static odom_t _odom;

/*
 * DME configuration.
 */
static const dme_config_t _dme_config = {
  .left = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_LEFT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .right = {
    .circumference = (M_PI * WHEEL_DIAMETER) + 0.5f,
    .offset = WHEEL_OFFSET,
    .qei = {
      .driver = &MODULE_HAL_QEI_RIGHT_WHEEL,
      .increments_per_revolution = MODULE_HAL_QEI_INCREMENTS_PER_REVOLUTION,
    },
  },
  .interval = MICROSECONDS_PER_SECOND / DME_FREQUENCY,
};

/*
 * DMC configuration.
 */
static const dmc_config_t _dmc_config = {
  .motors = {
    .left = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_LEFT_BACKWARD,
      },
    },
    .right = {
      .forward = {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_FORWARD,
      },
      .reverse= {
        .driver = &MODULE_HAL_PWM_DRIVE,
        .channel = MODULE_HAL_PWM_DRIVE_CHANNEL_RIGHT_BACKWARD,
      },
    },
  },
  .lpf = {
    .factor = 10.0f,
    .max = {
      .steering = 0.0f,
      .left = 0.0f,
      .right = 0.0f,
    },
  },
};


/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)

/**
 * @brief   DMC shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of command arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb(BaseSequentialStream* stream, int argc, char* argv[])
{
  return dmcShellCallback_setVelocity(stream, argc, argv, urtCoreGetService(DMC_TARGETSERVICE_ID));
}

/**
 * @brief   DMC get gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_getGains(BaseSequentialStream* stream, int argc, char* argv[])
{
  urtPrintf("dmc: %f \n", _dmc.callback);
  return dmcShellCallback_getGains(stream, argc, argv, &_dmc);
}

/**
 * @brief   DMC set gains shell command callback.
 *
 * @param[in] stream  Shell I/O stream.
 * @param[in] argc    Number of arguments.
 * @param[in] argv    Argument list.
 *
 * @return  The operation result.
 */
static int _appsDmcShellCmdCb_setGains(BaseSequentialStream* stream, int argc, char* argv[])
{
  return dmcShellCallback_setGains(stream, argc, argv, &_dmc);
}
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all Apps from the DiWheelDrive.
 */
void appsInit(void)
{
  // initialize all topics
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &_proximity_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &_ambient_floor_payload);
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &_proximity_ring_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &_ambient_ring_payload);
  urtTopicInit(&_dme_motion_topic, DME_TOPIC_ID, &_dme_motion_topic_payload);
  urtTopicInit(&_odom_topic, ODOM_TOPICID, &_odom_topic_payload);

  // initialize DME app
  dmeInit(&_dme, &_dme_config, DME_TOPIC_ID, URT_THREAD_PRIO_RT_MAX);
  // initialize DMC app
#if (DMC_CALIBRATION_ENABLE == true)
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, DMC_CALIBSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#else
  dmcInit(&_dmc, &_dmc_config, DME_TOPIC_ID, DMC_TARGETSERVICE_ID, URT_THREAD_PRIO_RT_MAX);
#endif

  // initialize Floor app
  _floor.driver_data.frequency = frequency;
  floorInit(&_floor,
            AMBIENT_FLOOR_TOPICID,
            PROXIMITY_FLOOR_TOPICID,
            URT_THREAD_PRIO_NORMAL_MIN);

#if (CAN_SUB_PROX_ENABLE == TRUE)
  //Subscriber subscribes to the proximity Floor Topic and should transmit to the floor id
  _can.sub_data[0].mask = TRIGGEREVENT;
  _can.sub_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.sub_data[0].size = sizeof(floor_sensors_t);
#endif /* CAN_SUB_PROX_ENABLE == True */

#if (CAN_SUB_AMB_ENABLE == true)
  //Subscriber subscribes to the ambient Floor Topic and should transmit to the floor id
  _can.sub_data[1].mask = TRIGGEREVENT;
  _can.sub_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.sub_data[1].size = sizeof(floor_sensors_t);
#endif /* CAN_SUB_AMB_ENABLE == true */

#if (CAN_SUB_ODOM_ENABLE == true)
  //Subscriber subcribes to the odom Topic and should transmit to the odom id
  _can.sub_odom_data.mask = ODOMETRYEVENT;
  _can.sub_odom_data.topicid = ODOM_TOPICID;
  _can.sub_odom_data.size = sizeof(position_cv_si);
#endif /* CAN_SUB_ODOM_ENABLE == true */

#if (CAN_PUB_PROX_RING_ENABLE == true)
  //Publisher gets data from the RING and publishs them to the RING Topic
  _can.pub_ring_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.pub_ring_data[0].size = sizeof(ring_sensors_t);
#endif /* CAN_PUB_PROX_RING_ENABLE == true */

#if (CAN_PUB_AMB_RING_ENABLE == true)
  _can.pub_ring_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.pub_ring_data[1].size = sizeof(ring_sensors_t);
#endif /* CAN_PUB_AMB_RING_ENABLE == true */

#if (CAN_REQUEST_ENABLE == true)
  //Request signals the motor service
  _can.request_data.serviceToSignal = &_dmc.service_targetspeed;
#endif /* CAN_REQUEST_ENABLE == true */

  // specify CAN Bus filter to get only for this app relevant messages
  id_map_t ids;
  ids.cnt = 0;
  ids.id = ~(_can.pub_ring_data[0].topicid ^ _can.pub_ring_data[1].topicid ^ DMC_SERVICE_ID); //ID must match
  ids.type = ~(CAN_REQUEST ^ CAN_TOPIC_DATA); //Type must match
  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_ring_data[0].topicid | _can.pub_ring_data[1].topicid | DMC_SERVICE_ID;
  filter_id.type = CAN_REQUEST | CAN_TOPIC_DATA;
  _can.can_filter.mask.ext.id = ids.raw;
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw;
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;

  // Initialize the CAN App
  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

  // Initialize the Odometry App
  odometryInit(&_odom,
               URT_THREAD_PRIO_NORMAL_MIN,
               DME_TOPIC_ID,
               ODOM_TOPICID);

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
  // add shell commands
  aosShellAddCommand(&_appsDmcSehhCmd);
  aosShellAddCommand(&_appsDmcShellCmd_getGains);
  aosShellAddCommand(&_appsDmcShellCmd_setGains);
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */
  return;
}

