/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    urtconf.h
 * @brief   µRT configuration file for the DiWheelDrive (v1.1) AMiRo module.
 * @details Contains the module specific µRT settings.
 *
 * @addtogroup ASE_DiWheelDrive_1-1
 * @{
 */

#ifndef URTCONF_H
#define URTCONF_H

#include <ASE_urtconf.h>

/*===========================================================================*/
/**
 * @name CAN Flags.
 * @{
 */
/*===========================================================================*/

/**
 * @brief   Flag of the CAN proximity Subscriber.
 */
#define CAN_SUB_PROX_ENABLE                true

/**
 * @brief   Flag of the CAN ambient Subscriber.
 */
#define CAN_SUB_AMB_ENABLE                 true

/**
 * @brief   Flag of the odometry Subscriber.
 */
#define CAN_SUB_ODOM_ENABLE                true

/**
 * @brief   Flag of the CAN proximity and ambientFloor publisher.
 */
#define CAN_PUB_PROX_FLOOR_ENABLE          false
#define CAN_PUB_AMB_FLOOR_ENABLE           false

/**
* @brief   Flag of the CAN proximity and ambient Ring publisher.
*/
#define CAN_PUB_PROX_RING_ENABLE           true
#define CAN_PUB_AMB_RING_ENABLE            true

/**
* @brief   Flag of the CAN odometry publisher.
*/
#define CAN_PUB_ODOM_ENABLE                false

/**
 * @brief   Flag of the CAN Request.
 */
#define CAN_REQUEST_ENABLE                 true

/**
 * @brief   Flag of the CAN Service.
 */
#define CAN_LIGHT_SERVICE_ENABLE           false

/**
 * @brief   Flag of the CAN Service.
 */
#define CAN_MOTOR_SERVICE_ENABLE           false



#endif /* URTCONF_H */

/** @} */
