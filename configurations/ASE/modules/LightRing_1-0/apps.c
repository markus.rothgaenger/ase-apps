/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    apps.c
 * @brief   ASE configuration application container.
 */

#include "apps.h"
#include <canbridge.h>
#include <light.h>
#include <stdlib.h>
#include <string.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/*
 * Forward declarations.
 */
static int _light_shellcb(BaseSequentialStream* stream, int argc, char* argv[]);

/*
 * Shell commands.
 */
static AOS_SHELL_COMMAND(_light_shellcmd, "CAN:Test", _light_shellcb);

/*
 * Topics for the different data.
 */
static urt_topic_t _proximity_ring_topic;
static urt_topic_t _ambient_ring_topic;
static urt_topic_t _proximity_floor_topic;
static urt_topic_t _ambient_floor_topic;

/*
 * Payloads of the different data.
 */
static ring_sensors_t prox_ring_payload;
static ring_sensors_t amb_ring_payload;
static floor_sensors_t prox_floor_payload;
static floor_sensors_t amb_floor_payload;

/*
 * CAN node object.
 */
static can_node_t _can;

/*
 * Light data node object.
 */
static light_data_t _light;

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__)
static int _light_shellcb(BaseSequentialStream* stream, int argc, char* argv[])
{
  for (int sensor = 0; sensor < 8; sensor++) {
      chprintf(stream, "Ring %i: %u \t amb: %u \n",
               sensor,
               _can.pub_ring_data[0].ring_data[0].values.data[sensor],
               _can.pub_ring_data[1].ring_data[0].values.data[sensor]);
  }

  for (int sensor = 0; sensor < 4; sensor++) {
      chprintf(stream, "Floor %i: %u \t amb: %u \n",
               sensor,
               _can.pub_floor_data[0].floor_data[0].values.data[sensor],
               _can.pub_floor_data[1].floor_data[0].values.data[sensor]);
  }

  return AOS_OK;
}
#endif /* (AMIROOS_CFG_SHELL_ENABLE == true) || defined(__DOXYGEN__) */

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Initializes all data applications for the AMiRo default configuration.
 */
void appsInit(void)
{
  // initialize all topics
  urtTopicInit(&_proximity_ring_topic, PROXIMITY_RING_TOPICID, &prox_ring_payload);
  urtTopicInit(&_ambient_ring_topic, AMBIENT_RING_TOPICID, &amb_ring_payload);
  urtTopicInit(&_proximity_floor_topic, PROXIMITY_FLOOR_TOPICID, &prox_floor_payload);
  urtTopicInit(&_ambient_floor_topic, AMBIENT_FLOOR_TOPICID, &amb_floor_payload);

  // initialize Light App
  lightInit(&_light,
            URT_THREAD_PRIO_NORMAL_MIN,
            LIGHT_SERVICE_ID);

#if (CAN_PUB_PROX_RING_ENABLE == true)
  //Publisher gets proximity data from the ring and publishs them to the proximity ring Topic
  _can.pub_ring_data[0].topicid = PROXIMITY_RING_TOPICID;
  _can.pub_ring_data[0].size = sizeof(ring_sensors_t);
#endif /* CAN_PUB_PROX_RING_ENABLE == true */

#if (CAN_PUB_AMB_RING_ENABLE == true)
  //Publisher gets ambient data from the ring and publishs them to the ambient ring Topic
  _can.pub_ring_data[1].topicid = AMBIENT_RING_TOPICID;
  _can.pub_ring_data[1].size = sizeof(ring_sensors_t);
#endif /* CAN_PUB_AMB_RING_ENABLE == true */

#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
  //Publisher gets proximity data from the floor and publishs them to the proximity floor Topic
  _can.pub_floor_data[0].topicid = PROXIMITY_FLOOR_TOPICID;
  _can.pub_floor_data[0].size = sizeof(floor_sensors_t);
#endif /* CAN_PUB_PROX_FLOOR_ENABLE == true */

#if (CAN_PUB_AMB_FLOOR_ENABLE == true)
  //Publisher gets ambient data from the floor and publishs them to the ambient floor Topic
  _can.pub_floor_data[1].topicid = AMBIENT_FLOOR_TOPICID;
  _can.pub_floor_data[1].size = sizeof(floor_sensors_t);
#endif /* CAN_PUB_AMB_FLOOR_ENABLE == true */

#if (CAN_REQUEST_ENABLE == true)
  //Request signals the light service
  _can.request_data.serviceToSignal = &_light.light_service;
#endif /* CAN_REQUEST_ENABLE == true */

#if (CAN_PUB_PROX_RING_ENABLE == true && CAN_PUB_AMB_RING_ENABLE == true && CAN_PUB_PROX_FLOOR_ENABLE == true && CAN_PUB_AMB_FLOOR_ENABLE == true)
  id_map_t ids;
  ids.cnt = 0;
  ids.id =~(_can.pub_ring_data[0].topicid ^ _can.pub_ring_data[1].topicid
           ^ _can.pub_floor_data[0].topicid ^ _can.pub_floor_data[1].topicid ^
           LIGHT_SERVICE_ID);
  ids.type = ~(CAN_TOPIC_DATA ^ CAN_REQUEST);

  id_map_t filter_id;
  filter_id.cnt = 0;
  filter_id.id = _can.pub_ring_data[0].topicid | _can.pub_ring_data[1].topicid
                | _can.pub_floor_data[0].topicid | _can.pub_floor_data[1].topicid
                | LIGHT_SERVICE_ID;
  filter_id.type = CAN_TOPIC_DATA | CAN_REQUEST;

  // specify CAN Bus filter to get only for this app relevant messages
  _can.can_filter.mask.ext.id = ids.raw; //Equal bit of these two masks must match other not
  _can.can_filter.mask.ext.ide = ~0;
  _can.can_filter.mask.ext.rtr = ~0;
  _can.can_filter.filter.ext.id = filter_id.raw; // should only receive messages from this ID
  _can.can_filter.filter.ext.ide = CAN_IDE_EXT;
  _can.can_filter.filter.ext.rtr = CAN_RTR_DATA;
  _can.can_filter.cbparams = &_can;
#endif /* (CAN_PUB_PROX_RING_ENABLE == true && CAN_PUB_AMB_RING_ENABLE == true
  && CAN_PUB_PROX_FLOOR_ENABLE == true && CAN_PUB_AMB_FLOOR_ENABLE == true) */

  // initialize the CAN App
  canLogicInit(&_can,
               URT_THREAD_PRIO_NORMAL_MIN);

  // add shell commands
  aosShellAddCommand(&_light_shellcmd);

  return;
}
