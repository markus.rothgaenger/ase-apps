/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    DWD_floordata.h
 * @brief   Data type describing the payload of the four floor sensors.
 *
 * @addtogroup DWD_FloorData
 * @{
 */


#ifndef DWD_FLOORDATA_H
#define DWD_FLOORDATA_H

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/


/**
 * @brief   Union to represent vcnl floor values.
 */
typedef union {
  uint16_t data[4];
  struct {
    uint16_t left_wheel;
    uint16_t left_front;
    uint16_t right_front;
    uint16_t right_wheel;
  }values;
} floor_sensors_t;

/**
 * @brief   Structure to handle the floor sensor data.
 */
typedef struct floor_data {
  floor_sensors_t values;
}floor_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* DWD_FLOORDATA_H */
