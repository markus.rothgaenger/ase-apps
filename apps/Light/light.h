/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    helloworld.h
 * @brief   A simple "Hello world!" application.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef LIGHT_H
#define LIGHT_H

#include <urt.h>
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/LightRing_leddata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(LIGHT_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of light threads.
 */
#define LIGHT_STACKSIZE             256
#endif /* !defined(LIGHT_STACKSIZE) */

/**
 * @brief   Event mask to be set on service events.
 */
#define SERVICEEVENT                  (urt_osEventMask_t)(1<< 1)


/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Light data.
 * @struct  light_data
 */
typedef struct light_data {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, LIGHT_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief   Driver related data.
   */
  struct {
    /**
     * @brief   24 channel PWM LED driver.
     */
    TLC5947Driver* driver;

    /**
     * @brief buffer for the LED driver
     */
    tlc5947_lld_buffer_t buffer;
  } driver_data;

#if (URT_CFG_RPC_ENABLED == true) || defined(__DOXYGEN__)
  urt_service_t light_service;
#endif /* (URT_CFG_RPC_ENABLED == true) */
  light_led_data_t light_values;
} light_data_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void lightInit(light_data_t* light, urt_osThreadPrio_t prio, urt_serviceid_t id);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* LIGHT_H */

/** @} */
