/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <light.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of light nodes.
 */
static const char _light_name[] = "Light";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

/**
 * @brief   Setup callback function for light nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] light    Pointer to the light structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _light_Setup(urt_node_t* node, void* light)
{
  urtDebugAssert(light != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_light_name);

  memset(((light_data_t*)light)->driver_data.buffer.data, 0, TLC5947_LLD_BUFFER_SIZE);
  tlc5947_lld_write(((light_data_t*)light)->driver_data.driver, &((light_data_t*)light)->driver_data.buffer);
  tlc5947_lld_update(((light_data_t*)light)->driver_data.driver);

  // set light on
  tlc5947_lld_setBlank(((light_data_t*)light)->driver_data.driver, TLC5947_LLD_BLANK_DISABLE);

  return SERVICEEVENT;
}

/**
 * @brief   Loop callback function for light nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] light   Pointer to the light structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _light_Loop(urt_node_t* node, urt_osEventMask_t event, void* light)
{
  urtDebugAssert(light != NULL);
  (void)node;

  switch(event) {
    case SERVICEEVENT:
    {
      // get the request
      urt_service_dispatched_t dispatched = urtServiceDispatch(&((light_data_t*)light)->light_service,
                                                               &((light_data_t*)light)->light_values,
                                                               sizeof(((light_data_t*)light)->light_values));
      urt_service_dispatched_t* disp;
      disp = &dispatched;

      memset(((light_data_t*)light)->driver_data.buffer.data, 0, TLC5947_LLD_BUFFER_SIZE);
      tlc5947_lld_write(((light_data_t*)light)->driver_data.driver, &((light_data_t*)light)->driver_data.buffer);
      tlc5947_lld_update(((light_data_t*)light)->driver_data.driver);

      while(disp->request != NULL) {
        uint8_t arraySize = sizeof(((light_data_t*)light)->light_values.colors) / sizeof(((light_data_t*)light)->light_values.colors[0]);
        uint8_t values[3];
        for (uint8_t i = 0; i < arraySize; i++) {
          switch(((light_data_t*)light)->light_values.colors[i]) {
          case OFF: {
            values[0] = 0;
            values[1] = 0;
            values[2] = 0;
            break;
          }
          case RED: {
            values[0] = 255;
            values[1] = 0;
            values[2] = 0;
            break;
          }
          case GREEN: {
            values[0] = 0;
            values[1] = 255;
            values[2] = 0;
            break;
          }
          case BLUE: {
            values[0] = 0;
            values[1] = 0;
            values[2] = 255;
            break;
          }
          default: break;
          }

          for (uint8_t channel = i*3; channel < i*3+3; ++channel) {
            tlc5947_lld_setBuffer(&((light_data_t*)light)->driver_data.buffer, channel, values[channel%3]);
            tlc5947_lld_write(((light_data_t*)light)->driver_data.driver, &((light_data_t*)light)->driver_data.buffer);
            tlc5947_lld_update(((light_data_t*)light)->driver_data.driver);
          }
        }

        // signal the request
        if (dispatched.request != NULL) {
          if (urtServiceTryAcquireRequest(&((light_data_t*)light)->light_service, &dispatched) == URT_STATUS_OK) {
            urtServiceRespond(&dispatched);
          }
        }

        urt_service_dispatched_t dispatched = urtServiceDispatch(&((light_data_t*)light)->light_service,
                                                                 &((light_data_t*)light)->light_values,
                                                                 sizeof(((light_data_t*)light)->light_values));
        disp = &dispatched;
       }

      break;
    }
    default: break;
  }

  return SERVICEEVENT;
}

/**
 * @brief   Shutdown callback function for light nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] light  Pointer to the light structure.
 *                    Must nor be NULL.
 */
void _light_Shutdown(urt_node_t* node, urt_status_t reason, void* light)
{
  urtDebugAssert(light != NULL);

  (void)node;
  (void)reason;

  // set light off
  tlc5947_lld_setBlank(((light_data_t*)light)->driver_data.driver, TLC5947_LLD_BLANK_ENABLE);

  return;
}

void lightInit(light_data_t* light, urt_osThreadPrio_t prio, urt_serviceid_t id)
{
  urtDebugAssert(light != NULL);

  light->driver_data.driver = &moduleLldLedPwm;

  // initialize the node
  urtNodeInit(&light->node, (urt_osThread_t*)light->thread, sizeof(light->thread), prio,
              _light_Setup, light,
              _light_Loop, light,
              _light_Shutdown, light);

#if (URT_CFG_RPC_ENABLED == true)
  urtServiceInit(&light->light_service, id, light->node.thread, SERVICEEVENT);
#endif /* (URT_CFG_RPC_ENABLED == true) */

  return;
}
