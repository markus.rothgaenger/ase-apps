/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    helloworld.h
 * @brief   A simple "Hello world!" application.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef FLOOR_H
#define FLOOR_H

#include <amiroos.h>
#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(FLOOR_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of floor threads.
 */
#define FLOOR_STACKSIZE             256
#endif /* !defined(FLOOR_STACKSIZE) */

/**
 * @brief   Event flag to identify trigger events related to publish-subscribe.
 */
#define TRIGGERFLAG_FLOOR           (urt_osEventFlags_t)(1 << 0)

/**
 * @brief   Event mask to set on a trigger event.
 */
#define TRIGGEREVENT                (urt_osEventMask_t)(1<< 1)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Floor node.
 * @struct  floor_node
 */
typedef struct floor_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, FLOOR_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief Id of the topic where the proximity/ambient ring data are published on.
   */
  urt_topicid_t prox_topicid;
  urt_topicid_t amb_topicid;

  /**
   * @brief   Driver related data.
   */
  struct {
    /**
     * @brief  driver for all four floor sensors
     */
    VCNL4020Driver* driver;

    /**
     * @brief  mux for all four floor sensors
     */
    PCA9544ADriver* mux;

    /**
     * @brief timeout of the functions
     */
    apalTime_t timeout;

    /**
     * @brief node frequency in Hz.
     */
    float frequency;

    /**
     * @brief Proximity/ambient data of the vcnl4020 driver
     */
    floor_sensors_t prox_data;
    floor_sensors_t amb_data;
  } driver_data;

  /**
   * @brief   Trigger related data.
   */
  struct {
    /**
     * @brief   Pointer to the trigger event source.
     */
    urt_osEventSource_t source;

    /**
     * @brief   Event listener for trigger events.
     */
    urt_osEventListener_t listener;

    /**
     * @brief   Timer to trigger floor data.
     */
    aos_timer_t timer;
  } trigger;

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  /**
   * @brief   Publisher to publish the proximity/ambient ring data.
   */
  urt_publisher_t floor_prox_publisher;
  urt_publisher_t floor_amb_publisher;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

} floor_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void floorInit(floor_node_t* floor, urt_topicid_t amb_topicid, urt_topicid_t prox_topicid, urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* FLOOR_H */

/** @} */
