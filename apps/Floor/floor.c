/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "floor.h"
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of floor nodes.
 */
static const char _floor_name[] = "Floor";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _floor_triggercb(void* params)
{
  (void)params;

  // broadcast ring event
  chEvtBroadcastFlagsI(&((floor_node_t*)params)->trigger.source, (urt_osEventFlags_t)0);

  return;
}

/**
 * @brief   Setup callback function for floor nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] floor    Pointer to the floor structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _floor_Setup(urt_node_t* node, void* floor)
{
  urtDebugAssert(floor != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_floor_name);

  vcnl4020_lld_proxratereg_t proxrate;
  if (((floor_node_t*)floor)->driver_data.frequency <= 1.95) {
    proxrate = VCNL4020_LLD_PROXRATEREG_1_95_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 3.90625) {
    proxrate = VCNL4020_LLD_PROXRATEREG_3_90625_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 7.8125) {
    proxrate = VCNL4020_LLD_PROXRATEREG_7_8125_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 15.625) {
    proxrate = VCNL4020_LLD_PROXRATEREG_15_625_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 31.25) {
    proxrate = VCNL4020_LLD_PROXRATEREG_31_25_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 62.5) {
    proxrate = VCNL4020_LLD_PROXRATEREG_62_5_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 125) {
    proxrate = VCNL4020_LLD_PROXRATEREG_125_HZ;
  } else if (((floor_node_t*)floor)->driver_data.frequency <= 250) {
    proxrate = VCNL4020_LLD_PROXRATEREG_250_HZ;
  }


  //Start and initialize every sensor
  for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {
    pca9544a_lld_setchannel(((floor_node_t*)floor)->driver_data.mux, channel, MICROSECONDS_PER_SECOND);
    // set prox rate frequency
    vcnl4020_lld_writereg(((floor_node_t*)floor)->driver_data.driver,
                          VCNL4020_LLD_REGADDR_PROXRATE,
                          proxrate,
                          ((floor_node_t*)floor)->driver_data.timeout);
    // set led current
    vcnl4020_lld_writereg(((floor_node_t*)floor)->driver_data.driver,
                          VCNL4020_LLD_REGADDR_LEDCURRENT,
                          VCNL4020_LLD_LEDCURRENTREG_200_mA,
                          ((floor_node_t*)floor)->driver_data.timeout);
    // start sensor
    vcnl4020_lld_writereg(((floor_node_t*)floor)->driver_data.driver,
                          VCNL4020_LLD_REGADDR_CMD,
                          (VCNL4020_LLD_CMDREG_ALSEN | VCNL4020_LLD_CMDREG_PROXEN | VCNL4020_LLD_CMDREG_SELFTIMED),
                          ((floor_node_t*)floor)->driver_data.timeout);
  }

  // register trigger event
  urtEventRegister(&((floor_node_t*)floor)->trigger.source, &((floor_node_t*)floor)->trigger.listener, TRIGGEREVENT, 0);

  // activate the timer
  aosTimerPeriodicInterval(&((floor_node_t*)floor)->trigger.timer, (1.0f / ((floor_node_t*)floor)->driver_data.frequency) * MICROSECONDS_PER_SECOND, _floor_triggercb, floor);

  return TRIGGEREVENT;
}

/**
 * @brief   Loop callback function for floor nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] floor   Pointer to the floor structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _floor_Loop(urt_node_t* node, urt_osEventMask_t event, void* floor)
{
  urtDebugAssert(floor != NULL);

  (void)node;

  switch(event) {
    case TRIGGEREVENT:
    {
      int realChannel = 0;
      // go through every channel
      for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {
        switch(channel){
        case 0: {
          realChannel = 2;
          break;
        }
        case 1: {
          realChannel = 3;
          break;
        }
        case 2: {
          realChannel = 0;
          break;
        }
        case 3: {
          realChannel = 1;
          break;
        }
        default: break;
        }

        pca9544a_lld_setchannel(&moduleLldI2cMultiplexer, realChannel, MICROSECONDS_PER_SECOND);
        vcnl4020_lld_readalsandprox(&moduleLldProximity,
                                    &((floor_node_t*)floor)->driver_data.amb_data.data[channel],
                                    &((floor_node_t*)floor)->driver_data.prox_data.data[channel],
                                    MICROSECONDS_PER_SECOND);
      }

      // publish ambient/proximity data
      urtPublisherTryPublish(&((floor_node_t*)floor)->floor_prox_publisher, &((floor_node_t*)floor)->driver_data.prox_data, sizeof(((floor_node_t*)floor)->driver_data.prox_data), urtTimeNow());
      urtPublisherTryPublish(&((floor_node_t*)floor)->floor_amb_publisher, &((floor_node_t*)floor)->driver_data.amb_data, sizeof(((floor_node_t*)floor)->driver_data.amb_data), urtTimeNow());

      break;
    }
    default: break;
  }

  return TRIGGEREVENT;
}

/**
 * @brief   Shutdown callback function for floor nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] master  Pointer to the floor structure.
 *                    Must nor be NULL.
 */
void _floor_Shutdown(urt_node_t* node, urt_status_t reason, void* floor)
{
  urtDebugAssert(floor != NULL);

  (void)node;
  (void)reason;

  // unregister trigger event
  urtEventUnregister(&((floor_node_t*)floor)->trigger.source, &((floor_node_t*)floor)->trigger.listener);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void floorInit(floor_node_t* floor, urt_topicid_t amb_topicid, urt_topicid_t prox_topicid, urt_osThreadPrio_t prio)
{
  urtDebugAssert(floor != NULL);

  // set/initialize event data
  urtEventSourceInit(&floor->trigger.source);
  urtEventListenerInit(&floor->trigger.listener);
  // initialize the timer
  aosTimerInit(&floor->trigger.timer);
  // set the topicid
  floor->prox_topicid = prox_topicid;
#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize the publisher for the floor data
  urtPublisherInit(&floor->floor_prox_publisher, urtCoreGetTopic(prox_topicid), NULL);
  urtPublisherInit(&floor->floor_amb_publisher, urtCoreGetTopic(amb_topicid), NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  // set the driver
  floor->driver_data.driver = &moduleLldProximity;
  // set the mux
  floor->driver_data.mux = &moduleLldI2cMultiplexer;
  // set the timeout
  floor->driver_data.timeout = MICROSECONDS_PER_SECOND;

  // initialize the node
  urtNodeInit(&floor->node, (urt_osThread_t*)floor->thread, sizeof(floor->thread), prio,
              _floor_Setup, floor,
              _floor_Loop, floor,
              _floor_Shutdown, floor);

  return;
}
