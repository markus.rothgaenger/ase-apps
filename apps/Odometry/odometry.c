/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include <odometry.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

#define CIRCLE_180  3.14

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of odometry nodes.
 */
static const char _odometry_name[] = "Odometry";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Setup callback function for odometry nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] odometry    Pointer to the odometry structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _odometry_Setup(urt_node_t* node, void* odom)
{
  urtDebugAssert(odom != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_odometry_name);

  // subscribe to the motion topic
  {
    // kind of hack to save memory
    urt_topic_t* const motor_topic = ((odom_t*)odom)->nrt.topic;
    ((odom_t*)odom)->nrt.topic = NULL;
    urtNrtSubscriberSubscribe(&((odom_t*)odom)->nrt, motor_topic, MOTIONEVENT, NULL);
  }

  ((odom_t*)odom)->odomData.location.axes[0] = 0; // set the x axis to 0
  ((odom_t*)odom)->odomData.location.axes[1] = 0; // set the y axis to 0
  ((odom_t*)odom)->odomData.oriantation.angle = 0; // set the orientation angle to 0

  ((odom_t*)odom)->lastTime = urtTimeNow();

  return MOTIONEVENT;
}

/**
 * @brief   Loop callback function for odometry nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] odometry   Pointer to the odometry structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _odometry_Loop(urt_node_t* node, urt_osEventMask_t event, void* odom)
{
  urtDebugAssert(odom != NULL);

  (void)node;

  while (event & MOTIONEVENT) {
    // local variables
    float vx, vy, vth, dt;
    urt_osTime_t currentTime;

    // fetch latest motion message
    {
      if (urtNrtSubscriberFetchLatestMessage(&((odom_t*)odom)->nrt,
                                             &((odom_t*)odom)->motionData,
                                             sizeof(((odom_t*)odom)->motionData),
                                             &currentTime, NULL) != URT_STATUS_OK) {
        break;
      }
    }

    vx = ((odom_t*)odom)->motionData.translation.axes[0];
    vy = ((odom_t*)odom)->motionData.translation.axes[1];
    vth = ((odom_t*)odom)->motionData.rotation.vector[2];

    //check if the motion data changed
    if (vx == ((odom_t*)odom)->old_motionData.translation.axes[0] &&
        vy == ((odom_t*)odom)->old_motionData.translation.axes[1] &&
        vth == ((odom_t*)odom)->old_motionData.rotation.vector[2]) {
      return MOTIONEVENT;;
    }

    ((odom_t*)odom)->old_motionData = ((odom_t*)odom)->motionData;

    //Calculate the time difference in seconds because vx,vy,vth are per second
    dt = ((float)(currentTime - ((odom_t*)odom)->lastTime))/((float)MICROSECONDS_PER_SECOND);
    ((odom_t*)odom)->lastTime = currentTime;

    ((odom_t*)odom)->odomData.location.axes[0] += (vx * cosf(((odom_t*)odom)->odomData.oriantation.angle)
                                                   - vy * sinf(((odom_t*)odom)->odomData.oriantation.angle)) * dt;
    ((odom_t*)odom)->odomData.location.axes[1] += (vx * sinf(((odom_t*)odom)->odomData.oriantation.angle)
                                                   - vy * cosf(((odom_t*)odom)->odomData.oriantation.angle)) * dt;
    ((odom_t*)odom)->odomData.oriantation.angle += vth * dt;

    if (((odom_t*)odom)->odomData.oriantation.angle > CIRCLE_180) {
      ((odom_t*)odom)->odomData.oriantation.angle = (-2)*CIRCLE_180 + ((odom_t*)odom)->odomData.oriantation.angle;
    } else if (((odom_t*)odom)->odomData.oriantation.angle < ((-1)*CIRCLE_180)) {
      ((odom_t*)odom)->odomData.oriantation.angle = 2*CIRCLE_180 + ((odom_t*)odom)->odomData.oriantation.angle;
    }

    // publish the odometry data
    urtPublisherTryPublish(&((odom_t*)odom)->odom_publisher,
                           &((odom_t*)odom)->odomData,
                           sizeof(((odom_t*)odom)->odomData),
                           urtTimeNow());
  }

  return MOTIONEVENT;
}

/**
 * @brief   Shutdown callback function for odometry nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] odometry  Pointer to the odometry structure.
 *                    Must nor be NULL.
 */
void _odometry_Shutdown(urt_node_t* node, urt_status_t reason, void* odom)
{
  urtDebugAssert(odom != NULL);

  (void)node;
  (void)reason;

  // Unsubscribe from motion topic
  urtNrtSubscriberUnsubscribe(&((odom_t*)odom)->nrt);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void odometryInit(odom_t* odom, urt_osThreadPrio_t prio, urt_topicid_t motion_topicid, urt_topicid_t odom_topicid)
{
#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize the motion subscriber
  urtNrtSubscriberInit(&odom->nrt);
  odom->nrt.topic = urtCoreGetTopic(motion_topicid); // kind of hack to save memory

  // initialize the odometry publisher
  urtPublisherInit(&odom->odom_publisher, urtCoreGetTopic(odom_topicid), NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  // initialize the node
  urtNodeInit(&odom->node, (urt_osThread_t*)odom->thread, sizeof(odom->thread), prio,
              _odometry_Setup, odom,
              _odometry_Loop, odom,
              _odometry_Shutdown, odom);

  return;
}
