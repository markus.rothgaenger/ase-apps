/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    helloworld.h
 * @brief   A simple "Hello world!" application.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef ODOMETRY_H
#define ODOMETRY_H

#include <urt.h>
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/positiondata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(ODOMETRY_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of odometry threads.
 */
#define ODOMETRY_STACKSIZE             256
#endif /* !defined(ODOMETRY_STACKSIZE) */

/**
 * @brief   Event mask to set on a trigger event.
 */
#define MOTIONEVENT                 (urt_osEventMask_t)(1<< 1)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/


typedef struct odom {
  URT_THREAD_MEMORY(thread, ODOMETRY_STACKSIZE);
  urt_node_t node;

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  /**
   * @brief   Publisher to publish the odometry data
   */
  urt_publisher_t odom_publisher;
  /**
   * @brief Subscriber to get the motion data
   */
  urt_nrtsubscriber_t nrt;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  position_cv_si odomData;

  motion_ev_csi motionData;
  motion_ev_csi old_motionData;
  urt_osTime_t lastTime;
}odom_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void odometryInit(odom_t *odom, urt_osThreadPrio_t prio, urt_topicid_t motion_topicid, urt_topicid_t odom_topicid);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* ODOMETRY_H */

/** @} */
