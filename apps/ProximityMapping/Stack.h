#ifndef STACK_H
#define STACK_H

#include <stdlib.h>

typedef struct Stack {
  int data[10];
  int index;
} Stack_t;

Stack_t* initStack(void);

int emptyStack(Stack_t* stack);

void pushStack(Stack_t* stack, int _data);

int popStack(Stack_t* stack);

int topStack(Stack_t* stack);

#endif