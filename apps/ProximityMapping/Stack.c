#include "Stack.h"

Stack_t* initStack(void) {
  Stack_t* stack = malloc(sizeof(*stack));
  stack->index = 0;

  return stack;
}

int emptyStack(Stack_t* stack) { return stack->index < 1; }

void pushStack(Stack_t* stack, int _data) {
  stack->data[stack->index++] = _data;
}

int popStack(Stack_t* stack) {
  if (emptyStack(stack)) {
    return -1;
  }

  return stack->data[--stack->index];
}

int topStack(Stack_t* stack) {
  if (emptyStack(stack)) {
    return -1;
  }

  return stack->data[stack->index - 1];
}