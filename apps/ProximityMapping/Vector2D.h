#ifndef VECTOR2D_H
#define VECTOR2D_H

#include <stdlib.h>

typedef struct Vector2D {
  float x;
  float y;
} Vector2D_t;

Vector2D_t* createVec(float _x, float _y);

Vector2D_t* sub(Vector2D_t* lhs, Vector2D_t* rhs);
Vector2D_t* add(Vector2D_t* lhs, Vector2D_t* rhs);

float dotVec(const Vector2D_t* lhs, const Vector2D_t* rhs);

Vector2D_t* mulS(Vector2D_t* p, const float s);
Vector2D_t* divS(Vector2D_t* p, const float s);

float sqLength(Vector2D_t* p);

#endif