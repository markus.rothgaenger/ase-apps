#ifndef PRIO_QUEUE_H
#define PRIO_QUEUE_H

#define MAX_SIZE 1000

typedef struct PrioQueueNode {
    void *data;
    int prio;
} PrioQueueNode_t;

// returns the index of the parent node
int parent(int i);

// return the index of the left child
int left_child(int i);

// return the index of the right child
int right_child(int i);

void swap(PrioQueueNode_t *x, PrioQueueNode_t *y);

// insert the item at the appropriate position
void enqueue(PrioQueueNode_t a[], void *data, int prio, int *n);

// moves the item at position i of array a
// into its appropriate position
void min_heapify(PrioQueueNode_t a[], int i, int n);
// returns the  maximum item of the heap
PrioQueueNode_t get_min(PrioQueueNode_t a[]);

// deletes the max item and return
PrioQueueNode_t dequeue(PrioQueueNode_t a[], int *n);

// prints the heap
void print_heap(PrioQueueNode_t a[], int n);

#endif