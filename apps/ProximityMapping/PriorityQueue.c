#include "PriorityQueue.h"

// returns the index of the parent node
int parent(int i) {
    return (i - 1) / 2;
}

// return the index of the left child
int left_child(int i) {
    return 2 * i + 1;
}

// return the index of the right child
int right_child(int i) {
    return 2 * i + 2;
}

void swap(PrioQueueNode_t *x, PrioQueueNode_t *y) {
    PrioQueueNode_t temp = *x;
    *x = *y;
    *y = temp;
}

// insert the item at the appropriate position
void enqueue(PrioQueueNode_t a[], void *data, int prio, int *n) {
    if (*n >= MAX_SIZE) {
        // printf("%s\n", "The heap is full. Cannot insert");
        return;
    }
    // first insert the time at the last position of the array
    // and move it up

    a[*n].data = data;
    a[*n].prio = prio;
    *n = *n + 1;

    // move up until the heap property satisfies
    int i = *n - 1;
    while (i != 0 && a[parent(i)].prio > a[i].prio) {
        swap(&a[parent(i)], &a[i]);
        i = parent(i);
    }
}

// moves the item at position i of array a
// into its appropriate position
void min_heapify(PrioQueueNode_t a[], int i, int n) {
    // find left child node
    int left = left_child(i);

    // find right child node
    int right = right_child(i);

    // find the largest among 3 nodes
    int smallest = i;

    // check if the left node is larger than the current node
    if (left <= n && a[left].prio < a[smallest].prio) {
        smallest = left;
    }

    // check if the right node is larger than the current node
    if (right <= n && a[right].prio < a[smallest].prio) {
        smallest = right;
    }

    // swap the largest node with the current node
    // and repeat this process until the current node is larger than
    // the right and the left node
    if (smallest != i) {
        PrioQueueNode_t temp = a[i];
        a[i] = a[smallest];
        a[smallest] = temp;
        min_heapify(a, smallest, n);
    }

}

// returns the  maximum item of the heap
PrioQueueNode_t get_min(PrioQueueNode_t a[]) {
    return a[0];
}

// deletes the max item and return
PrioQueueNode_t dequeue(PrioQueueNode_t a[], int *n) {
    PrioQueueNode_t min_item = a[0];

    // replace the first item with the last item
    a[0] = a[*n - 1];
    *n = *n - 1;

    // maintain the heap property by heapifying the
    // first item
    min_heapify(a, 0, *n);
    return min_item;
}