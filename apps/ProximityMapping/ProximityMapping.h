/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo)
platform. Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    ProximityMapping.h
 * @brief   Proximity Mapping..
 *
 * @addtogroup ProximityMapping
 * @{
 */

#ifndef PROXIMITYMAPPING_H
#define PROXIMITYMAPPING_H

#include <urt.h>

#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/TouchSensordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/positiondata.h"
#include "Stack.h"
#include "Vector2D.h"
#include "PriorityQueue.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#ifndef M_PI
#define M_PI 3.14159265358979323846
#define M_PI_2 (M_PI / 2.0)
#endif

#if !defined(PROXIMITYMAPPING_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of ProximityMapping threads.
 */
#define PROXIMITYMAPPING_STACKSIZE 512
#endif /* !defined(PROXIMITYMAPPING_STACKSIZE) */

#define PROXRINGEVENT (urt_osEventMask_t)(1 << 1)
#define TOUCHEVENT (urt_osEventMask_t)(1 << 2)
// #define AMBRINGEVENT (urt_osEventMask_t)(1 << 2)
#define PROXFLOOREVENT (urt_osEventMask_t)(1 << 3)
#define ODOMEVENT (urt_osEventMask_t)(1 << 4)

#define ALIGNED_THRESHOLD 260
#define DISTANCE_ALIGNED_THRESHOLD 80
#define TARGET_DISTANCE 4800
#define MIN_SENSOR_READ 2800
#define FREE_SENSOR_READ (MIN_SENSOR_READ + 400)
#define MAX_PATH_RECORD 1000
#define PF_MAX_PATH_LENGTH 200
#define PF_MAX_QUEUE_LENGTH 100
#define MAX_CORNERS 10
#define LEFT_PHI_OFFSET_FACTOR 0.8  // 0.9 seems to be good
#define RIGHT_PHI_OFFSET_FACTOR 0.91  // 0.9 seems to be good
#define STRIP_LIMIT_INCREMENT 0.1
#define ROT_SPEED 1.3
#define MOTOR_BUFFER_SIZE 15

#define mapResolution 0.03
#define dimensionX 3.0
#define dimensionY 3.0
enum MAP_DIM {
  X = (int)(dimensionX / mapResolution) + 1,
  Y = (int)(dimensionY / mapResolution) + 1
};

#define P 1.0
#define I 0.0
#define D 0.0

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief Trace states
 */
typedef enum TraceState { PM_BOUNDARY, PM_IDENTIFY } TraceState_t;

/**
 * @brief Mapping states
 */
typedef enum MappingState {
  PM_ALIGN_BOUNDARY,
  PM_ALIGN_GOAL,
  PM_ALIGN,
  PM_FIND_CORNER,
  PM_FIND_WALL,
  PM_INIT,
  PM_MOVE_GOAL,
  PM_RESET,
  PM_SET_OUTER_CORNER,
  PM_SET_DISTANCE_CORNER,
  PM_SET_DISTANCE,
  PM_STOP,
  PM_STRIP_CORRECT,
  PM_STRIP_CROSS,
  PM_STRIP_MAP,
  PM_STRIP_TRACE,
  PM_TRACE_BOUNDARY,
  PM_TURN_90,
  PM_FIND_PATH,
  PM_FOLLOW_PATH,
  PM_IDENTIFY_OBJECT,
} MappingState_t;

/**
 * @brief Wall Direction
 */
typedef enum WallDirection {
  PM_BACK,
  PM_FRONT,
  PM_LEFT,
  PM_RIGHT,
  PM_UNKNOWN,
} WallDirection_t;

typedef enum ObstacleType {
  PM_I,
  PM_L,
  PM_J,
  PM_O,
  PM_S,
  PM_Z,
  PM_T,
} ObstacleType_t;

/**
 * @brief ObstacleDirection
 */
typedef enum ObstacleDirection {
  OD_RIGHT,
  OD_STRAIGHT,
  OD_LEFT,
} ObstacleDirection_t;

typedef struct Point {
  Vector2D_t vec;
  float phi;
  WallDirection_t wallDir;
} Point_t;

typedef struct Node {
    uint8_t i;
    uint8_t j;
    uint8_t value;
} Node_t;

/**
 * @brief   Trigger related data of the floor sensors.
 */
typedef struct floor {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  floor_sensors_t data;
} floor_t;

/**
 * @brief  Data of the motor
 */
typedef struct motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t *service;
} motor_t;

/**
 * @brief  Data of the light
 */
typedef struct light {
  urt_nrtrequest_t request;
  urt_service_t *service;
  light_led_data_t data;
} light_t;

/**
 * @brief   Trigger related data of the ring sensors.
 */
typedef struct ring {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  ring_sensors_t data;
} ring_t;

/**
 * @brief Odometry data of the AMiRo
 */
typedef struct odom {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  position_cv_si data;
} odom_t;

/**
 * @brief  Data of the touch sensors
 */
typedef struct touch {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  touch_data_t data;
} touch_t;

typedef struct MappingOdom {
  Vector2D_t vec;
  float phi;
} MappingOdom_t;

/**
 * @brief   HelloWorld node.
 * @struct  ProximityMapping_node
 */
typedef struct ProximityMapping_node {
  URT_THREAD_MEMORY(thread, PROXIMITYMAPPING_STACKSIZE);
  urt_node_t node;

  floor_t floor_prox;
  ring_t ring_prox;
  motor_t motor_data;
  odom_t odom_data;
  touch_t touch_data;

  float translationBuffer[MOTOR_BUFFER_SIZE];
  float rotationBuffer[MOTOR_BUFFER_SIZE];

  MappingState_t state;
  TraceState_t traceState;
  WallDirection_t wallDir;
  WallDirection_t previousWallDirection;

  Stack_t stateStack;

  Point_t goal;
  bool goalSet;
  Node_t *pathfindingPath[PF_MAX_PATH_LENGTH];
  PrioQueueNode_t prioQueue[PF_MAX_QUEUE_LENGTH];
  int prioQueueN;
  Vector2D_t path[X];
  int pathLength;
  int pathIndex;

  Vector2D_t recordedPath[MAX_PATH_RECORD];
  int recordedPathLength;
  int pathIter;

  Vector2D_t corners[MAX_CORNERS];
  int cornerIndex;

  Point_t identifyObjectStart;
  int identifyObjectConsecutiveLeftTurns;

  Vector2D_t *stripRootCorner;

  float stripLimit;
  float stripMapDistance;
  int tracePIDIntegral;
  int tracePIDPreviousError;

  urt_osTime_t lastMotorSignal;
  float previousRotation;
  float previousTranslation;

  MappingOdom_t mappingOdom;

  Node_t map[X][Y];
  Node_t* cameFrom[X][Y];

  float _p;
  float _i;
  float _d;
  int ODDirection;
  urt_osTime_t ODStartTime;
} ProximityMapping_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/
int pmPrintMap(ProximityMapping_node_t *node, BaseSequentialStream* stream, int argc, char* argv[]);
int pmPrintPath(ProximityMapping_node_t *node, BaseSequentialStream* stream, int argc, char* argv[]);
int pmPrintCorners(ProximityMapping_node_t *node, BaseSequentialStream* stream, int argc, char* argv[]);
int pmSetGains(ProximityMapping_node_t* node, BaseSequentialStream* stream, int argc, char* argv[]);

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
void ProximityMappingInit(ProximityMapping_node_t *ProximityMapping,
                          urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* HELLOWORLD_H */

/** @} */
