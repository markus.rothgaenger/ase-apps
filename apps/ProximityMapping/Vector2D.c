#include "Vector2D.h"

Vector2D_t* createVec(float _x, float _y)
{
  Vector2D_t *vec = malloc(sizeof(*vec));

  vec->x = _x;
  vec->y = _y;

  return vec;
}

Vector2D_t* sub(Vector2D_t* lhs, Vector2D_t* rhs) {
  return createVec(lhs->x - rhs->x, lhs->y - rhs->y);  
};
Vector2D_t* add(Vector2D_t* lhs, Vector2D_t* rhs) {
  return createVec(lhs->x + rhs->x, lhs->y + rhs->y);  
};

float dotVec(const Vector2D_t* lhs, const Vector2D_t* rhs) {
  return lhs->x * rhs->x + lhs->y * rhs->y;
};

Vector2D_t* mulS(Vector2D_t* p, const float s) {
  p->x *= s;
  p->y *= s;

  return p;
};
Vector2D_t* divS(Vector2D_t* p, const float s) {
  p->x /= s;
  p->y /= s;

  return p;
};

float sqLength(Vector2D_t* p) { return p->x * p->x + p->y * p->y; };