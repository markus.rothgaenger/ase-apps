/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo)
platform. Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "ProximityMapping.h"

#include <amiroos.h>
#include <math.h>
#include <stdlib.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/
const double RIGHT_ANGLE_VALUES[5] = {0.0, M_PI_2, M_PI, 3 * M_PI_2, 2 * M_PI};

const double robotOffsetX = dimensionX / 2.0f;
const double robotOffsetY = dimensionY / 2.0f;

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/
int ishape[] = {1, 2, 1, 2, 2, 2, 2, 1, 2, 1, 2, 2, 2, 2, 0, 0, 0, 0};
int jshape[] = {1, 2, 1, 2, 2, 3, 2, 1, 2, 1, 2, 2, 1, 2, 2, 2, 0, 0};
int lshape[] = {1, 2, 2, 1, 2, 1, 2, 3, 2, 2, 1, 2, 1, 2, 2, 2, 0, 0};
int oshape[] = {1, 2, 2, 1, 2, 2, 1, 2, 2, 1, 2, 2, 0, 0, 0, 0, 0, 0};
int sshape[] = {1, 2, 3, 2, 1, 2, 1, 2, 2, 1, 2, 3, 2, 1, 2, 1, 2, 2};
int tshape[] = {1, 2, 1, 2, 3, 2, 1, 2, 1, 2, 3, 2, 1, 2, 1, 2, 2, 2};
int zshape[] = {1, 2, 1, 2, 3, 2, 1, 2, 2, 1, 2, 1, 2, 3, 2, 1, 2, 2};
/**
 * @brief   Name of ProximityMapping nodes.
 */
static const char _ProximityMapping_name[] = "ProximityMapping";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

char* printWallDir(WallDirection_t _wallDir) {
  switch (_wallDir) {
    case PM_BACK:
      return "BACK";
    case PM_FRONT:
      return "FRONT";
    case PM_LEFT:
      return "LEFT";
    case PM_RIGHT:
      return "RIGHT";
    case PM_UNKNOWN:
      return "UNKNOWN";
    default:
      return "";
  }
}

char* printState(MappingState_t _state) {
  switch (_state) {
    case PM_ALIGN_BOUNDARY:
      return "ALIGN_BOUNDARY";
    case PM_ALIGN_GOAL:
      return "ALIGN_GOAL";
    case PM_ALIGN:
      return "ALIGN";
    case PM_FIND_CORNER:
      return "FIND_CORNER";
    case PM_FIND_WALL:
      return "FIND_WALL";
    case PM_INIT:
      return "INIT";
    case PM_MOVE_GOAL:
      return "MOVE_GOAL";
    case PM_RESET:
      return "RESET";
    case PM_SET_OUTER_CORNER:
      return "SET_OUTER_CORNER";
    case PM_SET_DISTANCE_CORNER:
      return "SET_DISTANCE_CORNER";
    case PM_SET_DISTANCE:
      return "SET_DISTANCE";
    case PM_STOP:
      return "STOP";
    case PM_STRIP_CORRECT:
      return "STRIP_CORRECT";
    case PM_STRIP_CROSS:
      return "STRIP_CROSS";
    case PM_STRIP_MAP:
      return "STRIP_MAP";
    case PM_STRIP_TRACE:
      return "STRIP_TRACE";
    case PM_TRACE_BOUNDARY:
      return "TRACE_BOUNDARY";
    case PM_TURN_90:
      return "TURN_90";
    case PM_FIND_PATH:
      return "FIND_PATH";
    case PM_FOLLOW_PATH:
      return "FOLLOW_PATH";
    case PM_IDENTIFY_OBJECT:
      return "IDENTIFY_OBJECT";
    default:
      return "";
  }
}

void setWallDir(ProximityMapping_node_t* node, WallDirection_t _wallDir) {
  if (_wallDir == node->wallDir) return;

  if (node->wallDir != PM_BACK) node->previousWallDirection = node->wallDir;

  node->wallDir = _wallDir;
  urtPrintf("wallDir updated %s\n", printWallDir(node->wallDir));
}

void setState(ProximityMapping_node_t* node, MappingState_t _state) {
  node->state = _state;
  urtPrintf("state updated %s\n", printState(node->state));
}

void printStateStackTop(ProximityMapping_node_t* node) {
  if (!emptyStack(&node->stateStack)) {
    urtPrintf("top of stack: %s\n", printState(topStack(&node->stateStack)));
  }
}

// Signal the service with the new motor data
void signalMotorService(ProximityMapping_node_t* ProximityMapping) {
  urtDebugAssert(ProximityMapping != NULL);

  // shift buffer array one element to the right and set the first element to
  // the new value
  memmove(
      ProximityMapping->translationBuffer + 1,
      ProximityMapping->translationBuffer,
      (MOTOR_BUFFER_SIZE - 1) * sizeof(*ProximityMapping->translationBuffer));
  ProximityMapping->translationBuffer[0] =
      ProximityMapping->motor_data.data.translation.axes[0];
  memmove(ProximityMapping->rotationBuffer + 1,
          ProximityMapping->rotationBuffer,
          (MOTOR_BUFFER_SIZE - 1) * sizeof(*ProximityMapping->rotationBuffer));
  ProximityMapping->rotationBuffer[0] =
      ProximityMapping->motor_data.data.rotation.vector[2];

  float translationSum = 0.0;
  float rotationSum = 0.0;

  for (int i = 0; i < MOTOR_BUFFER_SIZE; i++) {
    translationSum += ProximityMapping->translationBuffer[i];
    rotationSum += ProximityMapping->rotationBuffer[i];
  }

  ProximityMapping->motor_data.data.translation.axes[0] =
      translationSum / (float)MOTOR_BUFFER_SIZE;
  ProximityMapping->motor_data.data.rotation.vector[2] =
      rotationSum / (float)MOTOR_BUFFER_SIZE;

  if (urtNrtRequestTryAcquire(&ProximityMapping->motor_data.request) ==
      URT_STATUS_OK) {
    urtNrtRequestSubmit(&ProximityMapping->motor_data.request,
                        ProximityMapping->motor_data.service, 0);
  }
}

void getData(ProximityMapping_node_t* ProximityMapping) {
  urtDebugAssert(ProximityMapping != NULL);

  // local variables
  urt_status_t status;

  // fetch NRT of the floor proximity data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &ProximityMapping->floor_prox.nrt, &ProximityMapping->floor_prox.data,
        sizeof(ProximityMapping->floor_prox.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the ring proximity data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &ProximityMapping->ring_prox.nrt, &ProximityMapping->ring_prox.data,
        sizeof(ProximityMapping->ring_prox.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the node->mappingOdometry data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &ProximityMapping->odom_data.nrt, &ProximityMapping->odom_data.data,
        sizeof(ProximityMapping->odom_data.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the touch sensor data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &ProximityMapping->touch_data.nrt, &ProximityMapping->touch_data.data,
        sizeof(ProximityMapping->touch_data.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  return;
}

void align(ProximityMapping_node_t* node) {
  float diff = 100.0;
  float angle = 0.0;

  for (int i = 0; i < 5; i++) {
    float currentDiff =
        fabs((double)RIGHT_ANGLE_VALUES[i] -
             (double)fmod((double)(node->mappingOdom.phi + 2 * M_PI),
                          (double)2 * M_PI));
    if (currentDiff < diff) {
      diff = currentDiff;
      angle = RIGHT_ANGLE_VALUES[i];
    }
  }

  node->mappingOdom.phi = fmod((double)angle, (double)2 * M_PI);
}

void turnAround(ProximityMapping_node_t* node) {
  if (node->wallDir == PM_LEFT)
    setWallDir(node, PM_RIGHT);
  else if (node->wallDir == PM_RIGHT)
    setWallDir(node, PM_LEFT);
  else {
    if (node->previousWallDirection == PM_UNKNOWN ||
        node->previousWallDirection == PM_BACK)
      urtPrintf("can not turn around if wallDir unknown or back!\n");
    else
      setWallDir(node, node->previousWallDirection);
  }
}

bool isNearBoundary(ProximityMapping_node_t* node, Vector2D_t* p) {
  // distance to line segment, reference
  // https://www.iquilezles.org/www/articles/distfunctions2d/distfunctions2d.htm
  for (int i = 0; i < node->cornerIndex; i++) {
    Vector2D_t* A = &node->corners[i];
    Vector2D_t* B =
        i + 1 < node->cornerIndex ? &node->corners[i + 1] : &node->corners[0];

    Vector2D_t* BA = sub(B, A);
    Vector2D_t* PA = sub(p, A);

    float hUnclamped = dotVec(PA, BA) / dotVec(BA, BA);
    float h = hUnclamped < 0.0 ? 0.0 : hUnclamped > 1.0 ? 1.0 : hUnclamped;

    Vector2D_t* orthVec = sub(PA, mulS(BA, h));
    float distance = sqLength(orthVec);

    urtPrintf("isNearBoundary: distance to (%d, %d): %f\n", i,
              i + 1 < node->cornerIndex ? i + 1 : 0, distance);

    if (distance < 0.01) {
      return true;
    }

    free(BA);
    free(PA);
    free(orthVec);
  }

  return false;
}

bool compareSequence(int detectedSeq[], int detectedSeqSize,
                     int tetrisBlock[]) {
  for (int i = 0; i < detectedSeqSize; i++) {
    if (detectedSeq[i] != tetrisBlock[i]) {
      return false;
    }
  }
  return true;
}

int identifyObstacle(int recordedSeq[], int recordedSeqSize) {
  int recordedSeqRealLength;
  int j = 0;
  int temp;

  for (int i = 0; i < recordedSeqSize; i++) {
    if (recordedSeq[i] == 0) {
      recordedSeqRealLength = i;
      break;
    }
  }

  while (j < recordedSeqSize) {
    if (compareSequence(recordedSeq, recordedSeqSize, ishape)) {
      return PM_I;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, jshape)) {
      return PM_J;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, lshape)) {
      return PM_L;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, oshape)) {
      return PM_O;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, sshape)) {
      return PM_S;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, tshape)) {
      return PM_T;
    }
    if (compareSequence(recordedSeq, recordedSeqSize, zshape)) {
      return PM_Z;
    }

    temp = recordedSeq[0];

    for (int i = 0; i < recordedSeqRealLength; i++) {
      recordedSeq[i] = recordedSeq[i + 1];
    }

    recordedSeq[recordedSeqRealLength - 1] = temp;

    j++;
  }
  return 0;
}

void roundObject(ProximityMapping_node_t* node) {
  int objectSequence[18];
  int objectSequenceIndex = 0;

  if (node->ODStartTime < 1) {
    node->ODStartTime = urtTimeNow();
  }

  switch (node->ODDirection) {
    case OD_STRAIGHT: {
      int S2S3Diff =
          node->ring_prox.data.data[2] - node->ring_prox.data.data[3];
      // int S1S3Diff =
      //     node->ring_prox.data.data[1] - node->ring_prox.data.data[3];
      int TolS1 = node->ring_prox.data.data[1] + 100;

      node->motor_data.data.translation.axes[0] = 0.1;
      node->motor_data.data.rotation.vector[2] = 0.1;

      if (TolS1 < node->ring_prox.data.data[2]) {
        node->motor_data.data.rotation.vector[2] = 0.2;
      }

      if (TolS1 > node->ring_prox.data.data[2]) {
        node->motor_data.data.rotation.vector[2] = -0.1;
      }

      if (S2S3Diff < 100 && node->ring_prox.data.data[3] <= 3500) {
        urt_osTime_t endTime = urtTimeNow();
        double seconds = ((double)endTime - node->ODStartTime) /
                         ((double)MICROSECONDS_PER_SECOND);
        for (int i = 0; i < ((int)seconds); i++) {
          objectSequence[objectSequenceIndex] = 2;
          objectSequenceIndex = objectSequenceIndex + 1;
          if (identifyObstacle(objectSequence, 18) != 0) {
            int detectedObstacle = identifyObstacle(objectSequence, 18);
            urtPrintf("Identified Obstacle: %d \n", detectedObstacle);
          }
        }
        node->ODDirection = OD_LEFT;
      }
      /*

      if (S1S3Diff < 100 && node->ring_prox.data.data[1] > 2600 &&
          node->ring_prox.data.data[2] < 3000) {
            urt_osTime_t endTime = urtTimeNow();
            double seconds = ((double)endTime - node->ODStartTime) /
      ((double)MICROSECONDS_PER_SECOND); for (int i = 0; i < ((int)seconds);
      i++) { objectSequence[objectSequenceIndex] = 2; objectSequenceIndex =
      objectSequenceIndex + 1; if (identifyObstacle(objectSequence, 18) != 0){
                 int detectedObstacle = identifyObstacle(objectSequence, 18);
               }
             }
        node->ODDirection = OD_LEFT;
      }
      */

      if (node->ring_prox.data.data[7] > 5000 &&
          node->ring_prox.data.data[0] > 5000) {
        urt_osTime_t endTime = urtTimeNow();
        double seconds = ((double)endTime - node->ODStartTime) /
                         ((double)MICROSECONDS_PER_SECOND);
        for (int i = 0; i < ((int)seconds); i++) {
          objectSequence[objectSequenceIndex] = 2;
          objectSequenceIndex = objectSequenceIndex + 1;
          if (identifyObstacle(objectSequence, 18) != 0) {
            int detectedObstacle = identifyObstacle(objectSequence, 18);
            urtPrintf("Identified Obstacle: %d \n", detectedObstacle);
          }
        }
        node->ODDirection = OD_RIGHT;
      }

      break;
    }

    case OD_RIGHT: {
      int S2S3Diff =
          node->ring_prox.data.data[2] - node->ring_prox.data.data[3];
      node->motor_data.data.translation.axes[0] = 0.0;
      node->motor_data.data.rotation.vector[2] = -1.0;

      if (S2S3Diff <= 100 && node->ring_prox.data.data[1] >= 3500) {
        objectSequence[objectSequenceIndex] = 3;
        objectSequenceIndex = objectSequenceIndex + 1;
        if (identifyObstacle(objectSequence, 18) != 0) {
          int detectedObstacle = identifyObstacle(objectSequence, 18);
          urtPrintf("Identified Obstacle: %d \n", detectedObstacle);
        }
        node->ODStartTime = urtTimeNow();
        node->ODDirection = OD_STRAIGHT;
      }
      break;
    }

    case OD_LEFT: {
      node->motor_data.data.translation.axes[0] = 0.0;
      node->motor_data.data.rotation.vector[2] = 1.0;

      int diffS0S1 =
          node->ring_prox.data.data[1] - node->ring_prox.data.data[0];
      if (diffS0S1 <= 100 && node->ring_prox.data.data[1] >= 3000) {
        objectSequence[objectSequenceIndex] = 1;
        objectSequenceIndex = objectSequenceIndex + 1;
        if (identifyObstacle(objectSequence, 18) != 0) {
          int detectedObstacle = identifyObstacle(objectSequence, 18);
          urtPrintf("Identified Obstacle: %d \n", detectedObstacle);
        }
        node->ODStartTime = urtTimeNow();
        node->ODDirection = OD_STRAIGHT;
      }
      break;
    }
  }
}

// TODO: use internal node->mappingOdom instead?
void recordPath(ProximityMapping_node_t* node) {
  if (!(node->pathIter++ % 10)) {
    if (!(node->recordedPathLength < MAX_PATH_RECORD)) {
      urtPrintf("cannot record path: MAX_PATH_RECORD exceeded!\n");
      return;
    }
    if (node->recordedPathLength) {
      Vector2D_t* prevRecord =
          &node->recordedPath[node->recordedPathLength - 1];
      // discard records with very little change..
      if (fabs((double)(prevRecord->x - node->mappingOdom.vec.x)) +
              fabs((double)(prevRecord->y - node->mappingOdom.vec.y)) <
          0.01)
        return;
    }

    node->recordedPath[node->recordedPathLength++] = node->mappingOdom.vec;
  }
}

// TODO: add hit for walls/obstacles..
void writePath(ProximityMapping_node_t* node, float xDeviation,
               float yDeviation) {
  if (node->recordedPathLength < 1) return;

  Vector2D_t* deviationFraction =
      divS(createVec(xDeviation, yDeviation), (float)node->recordedPathLength);

  urtPrintf("deviation and fraction: (%f, %f) -> (%f, %f)\n", xDeviation,
            yDeviation, deviationFraction->x, deviationFraction->y);

  for (int i = 0; i < node->recordedPathLength; i++) {
    Vector2D_t* point = &node->recordedPath[i];

    Vector2D_t* scaledFraction =
        mulS(createVec(deviationFraction->x, deviationFraction->y), i);

    // TODO: determine direction of travel and wall direction respectively..
    Vector2D_t* correctedPoint = add(point, scaledFraction);

    int xi = round((correctedPoint->x + robotOffsetX) / mapResolution);
    int yi = round((correctedPoint->y + robotOffsetY) / mapResolution);

    node->map[xi][yi].value += 5;

    free(correctedPoint);
    free(scaledFraction);
  }

  // Vector2D_t* newOdom = add(&node->mappingOdom.vec, goalDeviation);
  node->mappingOdom.vec.x = node->mappingOdom.vec.x + xDeviation;
  node->mappingOdom.vec.y = node->mappingOdom.vec.y + yDeviation;

  // no need to clear memory..
  node->recordedPathLength = 0;

  // free(newOdom);
  free(deviationFraction);
}

void signalMotorRecord(ProximityMapping_node_t* node) {
  urt_osTime_t now = urtTimeNow();
  if (node->lastMotorSignal < 1) node->lastMotorSignal = now;

  double seconds =
      ((double)now - node->lastMotorSignal) / ((double)MICROSECONDS_PER_SECOND);
  if (seconds > 0) {
    float deltaPhi = seconds * node->previousRotation;
    float deltaX =
        seconds * node->previousTranslation * cos(node->mappingOdom.phi);
    float deltaY =
        seconds * node->previousTranslation * sin(node->mappingOdom.phi);

    // error terms are added in writePath()
    node->mappingOdom.vec.x += deltaX;
    node->mappingOdom.vec.y += deltaY;
    node->mappingOdom.phi += deltaPhi > 0.0
                                 ? LEFT_PHI_OFFSET_FACTOR * deltaPhi
                                 : RIGHT_PHI_OFFSET_FACTOR * deltaPhi;
    node->mappingOdom.phi = fmod(node->mappingOdom.phi + 2 * M_PI, 2 * M_PI);

    if (node->state == PM_STRIP_TRACE) {
      // TODO: switch to squared distance?
      node->stripMapDistance += sqrt(pow(deltaX, 2) + pow(deltaY, 2));
    }
  }

  signalMotorService(node);
  node->lastMotorSignal = now;
  node->previousRotation = node->motor_data.data.rotation.vector[2];
  node->previousTranslation = node->motor_data.data.translation.axes[0];

  recordPath(node);
}

//
// MAPPING MOVEMENT
//
void setDistance(ProximityMapping_node_t* node) {
  float distance =
      (node->ring_prox.data.values.nne + node->ring_prox.data.values.nnw) / 2.0;
  int distanceDiff = round(distance) - TARGET_DISTANCE;

  if (abs(distanceDiff) < DISTANCE_ALIGNED_THRESHOLD) {
    urtPrintf("distance reached\n");
    align(node);
    node->wallDir = node->previousWallDirection != PM_UNKNOWN
                        ? node->previousWallDirection
                        : PM_LEFT;

    if (emptyStack(&node->stateStack)) {
      setState(node, PM_ALIGN_BOUNDARY);
    } else {
      setState(node, popStack(&node->stateStack));
    }

    return;
  }

  if (distanceDiff < 0.0) {
    node->motor_data.data.translation.axes[0] = 0.03;
  } else {
    node->motor_data.data.translation.axes[0] = -0.03;
  }
}

void findWall(ProximityMapping_node_t* node) {
  unsigned int sensorReadout = UINT16_MAX;
  switch (node->wallDir) {
    case PM_LEFT:
      sensorReadout =
          (node->ring_prox.data.values.wnw + node->ring_prox.data.values.wsw) /
          2.0;
      break;
    case PM_RIGHT:
      sensorReadout =
          (node->ring_prox.data.values.ene + node->ring_prox.data.values.ese) /
          2.0;
      break;
    default:
      urtPrintf("warning findWall wallDir invalid\n");
      break;
  }

  // TODO: to defined value
  if (sensorReadout > TARGET_DISTANCE - 1500) {
    if (emptyStack(&node->stateStack)) {
      setState(node, PM_TRACE_BOUNDARY);
    } else {
      setState(node, popStack(&node->stateStack));
    }
  }

  node->motor_data.data.translation.axes[0] =
      node->traceState == PM_BOUNDARY ? 0.1 : 0.04;
}

void turn90Deg(ProximityMapping_node_t* node) {
  float alpha = 0.0;
  switch (node->wallDir) {
    case PM_LEFT:
      alpha = node->mappingOdom.phi + M_PI_2;
      break;
    case PM_RIGHT:
      alpha = node->mappingOdom.phi - M_PI_2;
      break;
    default:
      urtPrintf("error turn90Deg invalid wallDir!\n");
      break;
  }

  node->goal.phi = (float)fmod((double)(alpha + 2 * M_PI), (double)(2 * M_PI));
  setState(node, PM_ALIGN);
}

void setOuterCorner(ProximityMapping_node_t* node) {
  node->tracePIDIntegral = 0;
  node->tracePIDPreviousError = 0;

  if (node->cornerIndex % 2 == 0) {
    for (int i = 0; i <= node->cornerIndex; i++) {
      Vector2D_t* point = &node->corners[i];
      float distance = fabs(node->mappingOdom.vec.x - point->x) +
                       fabs(node->mappingOdom.vec.y - point->y);

      urtPrintf("distance to %dth corner: %f\n", i, distance);
      if (node->stripRootCorner == NULL && distance < 0.2) {
        Vector2D_t* goalDeviation = sub(point, &node->mappingOdom.vec);
        writePath(node, goalDeviation->x, goalDeviation->y);

        node->stripRootCorner = &node->corners[i + 2];
        // goal.vec = *stripRootCorner;
        // goalSet = true;

        pushStack(&node->stateStack, PM_TRACE_BOUNDARY);
        pushStack(&node->stateStack, PM_FIND_WALL);
        setState(node, PM_TURN_90);

        free(goalDeviation);
        return;
      }
    }
  }

  if (node->traceState == PM_BOUNDARY) {
    urtPrintf("marked %dth corner at (%f, %f)\n", node->cornerIndex,
              node->mappingOdom.vec.x, node->mappingOdom.vec.y);
    node->corners[node->cornerIndex++] = node->mappingOdom.vec;

    Vector2D_t* goalDeviation;
    Vector2D_t* currentCorner = &node->corners[node->cornerIndex - 1];
    if (node->cornerIndex < 2) {
      goalDeviation = createVec(0.0, 0.0);
    } else {
      Vector2D_t* prevCorner = &node->corners[node->cornerIndex - 2];

      if (fabs(prevCorner->x - currentCorner->x) < 0.5) {
        goalDeviation = createVec(prevCorner->x - currentCorner->x, 0.0);
      } else {
        goalDeviation = createVec(0.0, prevCorner->y - currentCorner->y);
      }
    }

    writePath(node, goalDeviation->x, goalDeviation->y);

    free(goalDeviation);
  } else {
    node->identifyObjectConsecutiveLeftTurns++;
    urtPrintf("corner at (%f, %f)\n", node->mappingOdom.vec.x,
              node->mappingOdom.vec.y);
  }

  pushStack(&node->stateStack, PM_TRACE_BOUNDARY);
  pushStack(&node->stateStack, PM_FIND_WALL);
  setState(node, PM_TURN_90);
}

void setDistanceCorner(ProximityMapping_node_t* node) {
  setDistance(node);

  if (node->state == PM_ALIGN_BOUNDARY) {
    node->tracePIDIntegral = 0;
    node->tracePIDPreviousError = 0;

    if (node->cornerIndex % 2 == 0) {
      for (int i = 0; i < node->cornerIndex; i++) {
        Vector2D_t* point = &node->corners[i];
        float distance = fabs(node->mappingOdom.vec.x - point->x) +
                         fabs(node->mappingOdom.vec.y - point->y);
        urtPrintf("distance to %dth corner: %f\n", i, distance);
        if (distance < 0.2) {
          Vector2D_t* goalDeviation = sub(point, &node->mappingOdom.vec);
          writePath(node, goalDeviation->x, goalDeviation->y);

          node->mappingOdom.vec.x = point->x;
          node->mappingOdom.vec.y = point->y;
          // if at first corner again..
          if (node->stripRootCorner == NULL && i == 0) {
            node->stripRootCorner = point;

            turnAround(node);
            pushStack(&node->stateStack, PM_STRIP_MAP);
          } else if (node->stripRootCorner != NULL &&
                     point == node->stripRootCorner) {
            pushStack(&node->stateStack, PM_STRIP_MAP);
          }

          free(goalDeviation);

          return;
        }
      }
    }

    if (node->traceState == PM_BOUNDARY) {
      // Vector2D_t* goalDeviation = createVec(0.0, 0.0);
      float xDeviation = 0.0;
      float yDeviation = 0.0;
      Vector2D_t* currentCorner = &node->mappingOdom.vec;
      if (node->cornerIndex < 1) {
        // goalDeviation = createVec(0.0, 0.0);
      } else {
        Vector2D_t* prevCorner = &node->corners[node->cornerIndex - 1];

        urtPrintf("prev: (%f, %f), current: (%f, %f)\n", prevCorner->x,
                  prevCorner->y, currentCorner->x, currentCorner->y);

        if (fabs(prevCorner->x - currentCorner->x) < 0.1) {
          // goalDeviation = createVec(prevCorner->x - currentCorner->x, 0.0);
          xDeviation = prevCorner->x - currentCorner->x;
        } else {
          // goalDeviation = createVec(0.0, prevCorner->y - currentCorner->y);
          yDeviation = prevCorner->y - currentCorner->y;
        }
      }

      if ((node)->cornerIndex >= 1) {
        writePath(node, xDeviation, yDeviation);
      }

      urtPrintf("marked %dth corner at (%f, %f)\n", node->cornerIndex,
                node->mappingOdom.vec.x, node->mappingOdom.vec.y);
      node->corners[node->cornerIndex++] = node->mappingOdom.vec;
      // if (node->cornerIndex >= 4) {
      //   setState(node, PM_STOP);
      // }

      // free(goalDeviation);
    } else {
      node->identifyObjectConsecutiveLeftTurns = 0;
      urtPrintf("corner at (%f, %f)\n", node->mappingOdom.vec.x,
                node->mappingOdom.vec.y);
    }
  }
}

void traceBoundary(ProximityMapping_node_t* node) {
  if (node->ring_prox.data.values.nne > TARGET_DISTANCE - 200 ||
      node->ring_prox.data.values.nnw > TARGET_DISTANCE - 200) {
    if (node->stripLimit > 0 && node->stripMapDistance < node->stripLimit) {
      setState(node, PM_STOP);
      urtPrintf("assume mapping done!\n");
      return;
    }

    setState(node, PM_SET_DISTANCE_CORNER);
    return;
  }

  if (node->stripLimit > 0 && node->stripMapDistance > node->stripLimit) {
    urtPrintf("trace limit (%f) reached!\n", node->stripLimit);
    align(node);
    setState(node, PM_ALIGN_BOUNDARY);
    setWallDir(node, PM_BACK);
    pushStack(&node->stateStack, PM_STRIP_CROSS);
    return;
  }

  if (node->traceState == PM_IDENTIFY &&
      node->identifyObjectConsecutiveLeftTurns > 1) {
    Vector2D_t* diffVec =
        sub(&node->mappingOdom.vec, &node->identifyObjectStart.vec);

    // TODO: add condition that checks wether the object was identified
    // correctly..
    if (fabs(diffVec->x) < 0.01 || fabs(diffVec->y) < 0.01) {
      pushStack(&node->stateStack, PM_STRIP_CROSS);
      node->goal.phi = node->identifyObjectStart.phi;
      node->wallDir = PM_BACK;
      setState(node, PM_ALIGN);
      node->traceState = PM_BOUNDARY;
    }

    free(diffVec);
  }

  int sensorDiff;
  int distanceError;
  bool sideFree = false;
  bool sidePossiblyFree = false;

  switch (node->wallDir) {
    case PM_LEFT:
      sidePossiblyFree = node->ring_prox.data.values.wnw < FREE_SENSOR_READ ||
                         node->ring_prox.data.values.wsw < FREE_SENSOR_READ;
      sideFree = node->ring_prox.data.values.wnw < MIN_SENSOR_READ &&
                 node->ring_prox.data.values.wsw < MIN_SENSOR_READ;

      sensorDiff = (int)node->ring_prox.data.values.wsw -
                   (int)node->ring_prox.data.values.wnw;
      distanceError =
          TARGET_DISTANCE - (int)((node->ring_prox.data.values.wsw +
                                   node->ring_prox.data.values.wnw) /
                                  2.0);
      break;
    case PM_RIGHT:
      sidePossiblyFree = node->ring_prox.data.values.ene < FREE_SENSOR_READ ||
                         node->ring_prox.data.values.ese < FREE_SENSOR_READ;
      sideFree = node->ring_prox.data.values.ene < MIN_SENSOR_READ &&
                 node->ring_prox.data.values.ese < MIN_SENSOR_READ;

      sensorDiff = (int)node->ring_prox.data.values.ene -
                   (int)node->ring_prox.data.values.ese;
      distanceError = (int)((node->ring_prox.data.values.ene +
                             node->ring_prox.data.values.ese) /
                            2.0) -
                      TARGET_DISTANCE;
      break;
    default:
      break;
  }

  if (sideFree) {
    setState(node, PM_SET_OUTER_CORNER);
    return;
  }

  if (!sidePossiblyFree) {
    distanceError = distanceError > 150.0    ? 2000
                    : distanceError < -150.0 ? -2000
                                             : 0;

    int error = sensorDiff + distanceError;
    int derivative = error - node->tracePIDPreviousError;
    node->tracePIDIntegral += error;

    float out = 1e-4 * (node->_p * error + node->_d * derivative +
                        node->_i * node->tracePIDIntegral);

    out = out > 0.0 ? fmin(0.2, out) : fmax(-0.2, out);
    // urtPrintf("PID: %f \n", out);

    node->motor_data.data.rotation.vector[2] = out;
  }

  node->motor_data.data.translation.axes[0] =
      node->traceState == PM_BOUNDARY ? 0.1 : 0.06;
}

bool wallIsNear(ProximityMapping_node_t* node) {
  return (node->ring_prox.data.values.ene > MIN_SENSOR_READ ||
          node->ring_prox.data.values.ese > MIN_SENSOR_READ ||
          node->ring_prox.data.values.nne > MIN_SENSOR_READ ||
          node->ring_prox.data.values.nnw > MIN_SENSOR_READ ||
          node->ring_prox.data.values.sse > MIN_SENSOR_READ ||
          node->ring_prox.data.values.ssw > MIN_SENSOR_READ ||
          node->ring_prox.data.values.wnw > MIN_SENSOR_READ ||
          node->ring_prox.data.values.wsw > MIN_SENSOR_READ);
}

void alignWithBoundary(ProximityMapping_node_t* node) {
  if (!wallIsNear(node)) {
    if (node->previousWallDirection == PM_BACK) {
      turn90Deg(node);
    } else {
      urtErrPrintf(
          "cannot align with boundary if no boundary near and previous "
          "wall direction not back..\n");
    }

    return;
  }

  unsigned int sensorDiff;
  // TODO: to defined value!
  bool freeSightCondition =
      node->ring_prox.data.values.nne < FREE_SENSOR_READ &&
      node->ring_prox.data.values.nnw < FREE_SENSOR_READ;
  bool wallNear = false;

  switch (node->wallDir) {
    case PM_BACK:
      sensorDiff = abs((int)node->ring_prox.data.values.ssw -
                       (int)node->ring_prox.data.values.sse);
      wallNear = node->ring_prox.data.values.ssw > MIN_SENSOR_READ ||
                 node->ring_prox.data.values.sse > MIN_SENSOR_READ;
      break;
    case PM_FRONT:
      sensorDiff = abs((int)node->ring_prox.data.values.nnw -
                       (int)node->ring_prox.data.values.nne);
      freeSightCondition = node->ring_prox.data.values.sse < FREE_SENSOR_READ &&
                           node->ring_prox.data.values.ssw < FREE_SENSOR_READ;
      wallNear = node->ring_prox.data.values.nnw > MIN_SENSOR_READ ||
                 node->ring_prox.data.values.nne > MIN_SENSOR_READ;

      break;
    case PM_LEFT:
      sensorDiff = abs((int)node->ring_prox.data.values.wsw -
                       (int)node->ring_prox.data.values.wnw);
      wallNear = node->ring_prox.data.values.wsw > MIN_SENSOR_READ ||
                 node->ring_prox.data.values.wnw > MIN_SENSOR_READ;
      break;
    case PM_RIGHT:
      sensorDiff = abs((int)node->ring_prox.data.values.ese -
                       (int)node->ring_prox.data.values.ene);
      wallNear = node->ring_prox.data.values.ese > MIN_SENSOR_READ ||
                 node->ring_prox.data.values.ene > MIN_SENSOR_READ;
      break;
    case PM_UNKNOWN:
    default:
      urtPrintf("cannot align with unknown wall direction!\n");
      return;
  }

  float minPhiDiff = 100.0;
  if (node->traceState == PM_BOUNDARY) {
    for (int i = 0; i < 5; i++) {
      float _phi = RIGHT_ANGLE_VALUES[i];
      if (fabs(_phi - node->mappingOdom.phi) < minPhiDiff) {
        minPhiDiff = fabs(_phi - node->mappingOdom.phi);
      }
    }
  } else {
    minPhiDiff = 0.0;
  }

  minPhiDiff = 0.0;

  if (node->wallDir != PM_UNKNOWN && freeSightCondition && wallNear) {
    if (minPhiDiff < 0.3 && sensorDiff < ALIGNED_THRESHOLD) {
      if (emptyStack(&node->stateStack)) {
        setState(node, PM_TRACE_BOUNDARY);
      } else {
        setState(node, popStack(&node->stateStack));
      }

      if (node->traceState == PM_BOUNDARY) align(node);

      return;
    }
    node->motor_data.data.rotation.vector[2] =
        node->wallDir == PM_LEFT || (node->wallDir == PM_BACK &&
                                     node->previousWallDirection == PM_LEFT)
            ? -0.6 * ROT_SPEED
            : 0.6 * ROT_SPEED;
  } else {
    node->motor_data.data.rotation.vector[2] =
        node->wallDir == PM_LEFT || (node->wallDir == PM_BACK &&
                                     node->previousWallDirection == PM_LEFT)
            ? -2 * ROT_SPEED
            : 2 * ROT_SPEED;
  }
}

void alignToGoal(ProximityMapping_node_t* node) {
  double alpha = atan2(node->goal.vec.y - node->mappingOdom.vec.y,
                       node->goal.vec.x - node->mappingOdom.vec.x);
  urtPrintf("alpha calculated: %f\n", alpha);
  node->goal.phi = fmod(alpha + 2 * M_PI, 2 * M_PI);
  setState(node, PM_ALIGN);
}

void checkAlignment(ProximityMapping_node_t* node) {
  if (fabs((double)(node->goal.phi - node->mappingOdom.phi)) < 0.05) {
    urtPrintf("reached goal orientation\n");
    if (emptyStack(&node->stateStack)) {
      setState(node, PM_MOVE_GOAL);
    } else {
      setState(node, popStack(&node->stateStack));
    }

    return;
  }

  float angleDiff = node->goal.phi - node->mappingOdom.phi;
  if (angleDiff < 0) {
    angleDiff += 2 * M_PI;
  }

  if (angleDiff > M_PI) {
    node->motor_data.data.rotation.vector[2] = -ROT_SPEED;
  } else {
    node->motor_data.data.rotation.vector[2] = ROT_SPEED;
  }
}

void moveToGoal(ProximityMapping_node_t* node) {
  if (fabs((double)(node->goal.vec.x - node->mappingOdom.vec.x)) +
          fabs((double)(node->goal.vec.y - node->mappingOdom.vec.y)) <
      0.02) {
    if (emptyStack(&node->stateStack)) {
      setState(node, PM_STOP);
    } else {
      setState(node, popStack(&node->stateStack));
    }

    urtPrintf("x %f, y %f\n", node->mappingOdom.vec.x, node->mappingOdom.vec.y);

    writePath(node, 0.0, 0.0);
    urtPrintf("x %f, node->mappingOdom.y %f\n", node->mappingOdom.vec.x,
              node->mappingOdom.vec.y);

    node->goalSet = false;

    return;
  }

  // if (node->ring_prox.data.values.nne > TARGET_DISTANCE ||
  //     node->ring_prox.data.values.nnw > TARGET_DISTANCE) {
  //   urtPrintf("realign..\n");
  //   // TODO: or avoid/identify obstacle..
  //   setState(node, PM_ALIGN_BOUNDARY);
  //   pushStack(&node->stateStack, PM_FIND_CORNER);

  //   if (node->ring_prox.data.values.nnw > node->ring_prox.data.values.nne) {
  //     setWallDir(node, PM_LEFT);
  //   } else {
  //     setWallDir(node, PM_RIGHT);
  //   }

  //   // node->findCornerStart = Vector2D(node->mappingOdom.vec);
  //   return;
  // }

  // TODO: obstacle avoidance
  // (if below estimated time -> obstacle)
  // float alpha = atan2((double)(node->goal.vec.y - node->mappingOdom.vec.y),
  //                     (double)(node->goal.vec.x - node->mappingOdom.vec.x));
  // float targetPhi = fmod(alpha + 2 * M_PI, 2 * M_PI);
  // float angleDiff = targetPhi - node->mappingOdom.phi;

  // if (angleDiff < 0) angleDiff += 2 * M_PI;
  // if (angleDiff > M_PI)
  //   node->motor_data.data.rotation.vector[2] = -0.2 * ROT_SPEED;
  // else
  //   node->motor_data.data.rotation.vector[2] = 0.2 * ROT_SPEED;

  node->motor_data.data.translation.axes[0] = 0.3;
}

void findCorner(ProximityMapping_node_t* node) {
  traceBoundary(node);

  if (node->state == PM_SET_DISTANCE_CORNER) {
    // TODO: if deviation to goal is too big -> assume obstacle!
    if (node->goalSet) {
      Vector2D_t* goalDeviation = sub(&node->goal.vec, &node->mappingOdom.vec);
      writePath(node, goalDeviation->x, goalDeviation->y);
      // assume we are at the node->goal now..
      node->mappingOdom.vec.x = node->goal.vec.x;
      node->mappingOdom.vec.y = node->goal.vec.y;

      setWallDir(node, node->previousWallDirection);

      if (emptyStack(&node->stateStack)) {
        setState(node, PM_STOP);
      } else {
        setState(node, popStack(&node->stateStack));
      }

      node->goalSet = false;

      free(goalDeviation);
    } else {
      for (int i = 0; i <= node->cornerIndex; i++) {
        Vector2D_t* point = &node->corners[i];
        float distance = fabs(node->mappingOdom.vec.x - point->x) +
                         fabs(node->mappingOdom.vec.y - point->y);
        urtPrintf("findCorner: distance to %dth corner: %f\n", i, distance);

        if (distance < 0.1) {
          Vector2D_t* goalDeviation = sub(point, &node->mappingOdom.vec);
          writePath(node, goalDeviation->x, goalDeviation->y);

          node->mappingOdom.vec.x = point->x;
          node->mappingOdom.vec.y = point->y;

          free(goalDeviation);
        }
      }
    }
  }
}

void stripCross(ProximityMapping_node_t* node) {
  // TODO: add obstacle avoidance/line correction if sideway sensors detect near
  // object..
  // TODO: to defined value!
  if (node->ring_prox.data.values.nne > TARGET_DISTANCE - 1000 ||
      node->ring_prox.data.values.nnw > TARGET_DISTANCE - 1000) {
    if (isNearBoundary(node, &node->mappingOdom.vec)) {
      pushStack(&node->stateStack, PM_STRIP_CORRECT);
      pushStack(&node->stateStack, PM_ALIGN_BOUNDARY);
      setState(node, PM_SET_DISTANCE);
      turnAround(node);
    } else {
      node->identifyObjectConsecutiveLeftTurns = 0;
      node->identifyObjectStart.vec = node->mappingOdom.vec;
      node->identifyObjectStart.phi = node->mappingOdom.phi;
      node->traceState = PM_IDENTIFY;

      // TODO: ~[1,2] blocks (TODO determine size..) in phi direction..
      float deltaX = 0.27 * cosf(node->mappingOdom.phi);
      float deltaY = 0.27 * sinf(node->mappingOdom.phi);
      // return to strip cross once identified and on other side
      // continue line of travel in forward direction..
      urtPrintf("identify object start (%f, %f)\n", node->mappingOdom.vec.x,
                node->mappingOdom.vec.y);
      urtPrintf("estimate end position of object to be at: (%f, %f)\n",
                node->mappingOdom.vec.x + deltaX,
                node->mappingOdom.vec.y + deltaY);
      urtPrintf("assume obstacle! NOT (completely) IMPLEMENTED!\n");

      setWallDir(node, PM_LEFT);
      pushStack(&node->stateStack, PM_ALIGN_BOUNDARY);
      setState(node, PM_SET_DISTANCE);
    }

    return;
  }

  node->motor_data.data.translation.axes[0] = 0.1;
}

void stripMap(ProximityMapping_node_t* node) {
  node->stripLimit += STRIP_LIMIT_INCREMENT;
  setState(node, PM_STRIP_TRACE);
  node->stripMapDistance = 0.0;
}

void findPath(ProximityMapping_node_t* node) {
  for (int i = 0; i < PF_MAX_PATH_LENGTH; i++) {
    node->pathfindingPath[i] = NULL;
  }

  int xiStart = round((node->mappingOdom.vec.x + robotOffsetX) / mapResolution);
  int yiStart = round((node->mappingOdom.vec.y + robotOffsetY) / mapResolution);
  int xiGoal = round((node->goal.vec.x + robotOffsetX) / mapResolution);
  int yiGoal = round((node->goal.vec.y + robotOffsetY) / mapResolution);

  Node_t* goal = &node->map[xiStart][yiStart];
  Node_t* start = &node->map[xiGoal][yiGoal];

  if (goal->value < 1 || start->value < 1) {
    urtErrPrintf("findPath: impossible path");
    return;
  }

  node->prioQueueN = 0;

  enqueue(node->prioQueue, start, 0,
          &node->prioQueueN);  // Füge Startpunkt der Warteschlange hinzu
  // Füge alle erreichbaren Punkte einmal der Warteschlange hinzu. Vom Start
  // aus.
  while (true) {
    if (node->prioQueueN < 1) {
      urtErrPrintf("findPath: no path found!");
      return;
    }

    Node_t* current = dequeue(node->prioQueue, &node->prioQueueN).data;

    if (current == goal) {
      break;
    } else {
      if ((current->i + current->j) % 2 == 0) {
        if (current->i + 1 < X &&
            node->cameFrom[current->i + 1][current->j] == NULL) {
          Node_t* NeighborUp = &node->map[current->i + 1][current->j];

          if (NeighborUp->value > 0) {
            int prio =
                1 + abs(current->i + 1 - goal->i) + abs(current->j - goal->j);
            node->cameFrom[current->i + 1][current->j] = current;
            enqueue(node->prioQueue, NeighborUp, prio, &node->prioQueueN);
          }
        }

        if (current->i - 1 >= 0 &&
            node->cameFrom[current->i - 1][current->j] == NULL) {
          Node_t* neighborDown = &node->map[current->i - 1][current->j];

          if (neighborDown->value > 0) {
            int prio =
                1 + abs(current->i - 1 - goal->i) + abs(current->j - goal->j);
            node->cameFrom[current->i - 1][current->j] = current;
            enqueue(node->prioQueue, neighborDown, prio, &node->prioQueueN);
          }
        }

        if (current->j + 1 < Y &&
            node->cameFrom[current->i][current->j + 1] == NULL) {
          Node_t* neighborRight = &node->map[current->i][current->j + 1];

          if (neighborRight->value > 0) {
            int prio =
                abs(current->i - goal->i) + abs(current->j + 1 - goal->j);
            node->cameFrom[current->i][current->j + 1] = current;
            enqueue(node->prioQueue, neighborRight, prio, &node->prioQueueN);
          }
        }

        if (current->j - 1 >= 0 &&
            node->cameFrom[current->i][current->j - 1] == NULL) {
          Node_t* neighborLeft = &node->map[current->i][current->j - 1];

          if (neighborLeft->value > 0) {
            int prio =
                abs(current->i - goal->i) + abs(current->j - 1 - goal->j);
            node->cameFrom[current->i][current->j - 1] = current;
            enqueue(node->prioQueue, neighborLeft, prio, &node->prioQueueN);
          }
        }
      } else {
        if (current->j + 1 < Y &&
            node->cameFrom[current->i][current->j + 1] == NULL) {
          Node_t* neighborRight = &node->map[current->i][current->j + 1];

          if (neighborRight->value > 0) {
            int prio =
                1 + abs(current->i - goal->i) + abs(current->j + 1 - goal->j);
            node->cameFrom[current->i][current->j + 1] = current;
            enqueue(node->prioQueue, neighborRight, prio, &node->prioQueueN);
          }
        }

        if (current->j - 1 >= 0 &&
            node->cameFrom[current->i][current->j - 1] == NULL) {
          Node_t* neighborLeft = &node->map[current->i][current->j - 1];

          if (neighborLeft->value > 0) {
            int prio =
                1 + abs(current->i - goal->i) + abs(current->j - 1 - goal->j);
            node->cameFrom[current->i][current->j - 1] = current;
            enqueue(node->prioQueue, neighborLeft, prio, &node->prioQueueN);
          }
        }

        if (current->i + 1 < X &&
            node->cameFrom[current->i + 1][current->j] == NULL) {
          Node_t* neighborUp = &node->map[current->i + 1][current->j];
          if (neighborUp->value > 0) {
            int prio =
                abs(current->i + 1 - goal->i) + abs(current->j - goal->j);
            node->cameFrom[current->i + 1][current->j] = current;
            enqueue(node->prioQueue, neighborUp, prio, &node->prioQueueN);
          }
        }

        if (current->i - 1 >= 0 &&
            node->cameFrom[current->i - 1][current->j] == NULL) {
          Node_t* neighborDown = &node->map[current->i - 1][current->j];
          if (neighborDown->value > 0) {
            int prio =
                abs(current->i - 1 - goal->i) + abs(current->j - goal->j);
            node->cameFrom[current->i - 1][current->j] = current;
            enqueue(node->prioQueue, neighborDown, prio, &node->prioQueueN);
          }
        }
      }
    }
  }

  Node_t* current = goal;
  int i = 0;
  while (current != start) {
    node->pathfindingPath[i++] = current;
    current = node->cameFrom[current->i][current->j];
  }
  // Path glaetten ohne ZigZag
  int k = 1;
  int z = 0;

  while (k++ < PF_MAX_PATH_LENGTH) {
    if (node->pathfindingPath[k] == NULL ||
        node->pathfindingPath[k - 1] == NULL ||
        node->pathfindingPath[k + 1] == NULL ||
        node->pathfindingPath[k + 2] == NULL) {
      Vector2D_t* point = &node->path[z++];
      point->x = start->i * mapResolution - robotOffsetX;
      point->y = start->j * mapResolution - robotOffsetY;
      break;
    }

    if (((abs(node->pathfindingPath[k - 1]->i - node->pathfindingPath[k]->i) ==
              0 &&
          abs(node->pathfindingPath[k - 1]->j - node->pathfindingPath[k]->j) !=
              0) &&
         (abs(node->pathfindingPath[k]->i - node->pathfindingPath[k + 1]->i) !=
              0 &&
          abs(node->pathfindingPath[k]->j - node->pathfindingPath[k + 1]->j) ==
              0) &&
         (abs(node->pathfindingPath[k + 1]->i -
              node->pathfindingPath[k + 2]->i) == 0 &&
          abs(node->pathfindingPath[k + 1]->j -
              node->pathfindingPath[k + 2]->j) != 0)) ||
        ((abs(node->pathfindingPath[k - 1]->j - node->pathfindingPath[k]->j) ==
              0 &&
          abs(node->pathfindingPath[k - 1]->i - node->pathfindingPath[k]->i) !=
              0) &&
         (abs(node->pathfindingPath[k]->j - node->pathfindingPath[k + 1]->j) !=
              0 &&
          abs(node->pathfindingPath[k]->i - node->pathfindingPath[k + 1]->i) ==
              0) &&
         (abs(node->pathfindingPath[k + 1]->j -
              node->pathfindingPath[k + 2]->j) == 0 &&
          abs(node->pathfindingPath[k + 1]->i -
              node->pathfindingPath[k + 2]->i) != 0))) {
      continue;
    } else if (node->pathfindingPath[k - 1]->i == node->pathfindingPath[k]->i &&
               node->pathfindingPath[k]->i == node->pathfindingPath[k + 1]->i) {
      continue;
    } else if (node->pathfindingPath[k - 1]->j == node->pathfindingPath[k]->j &&
               node->pathfindingPath[k]->j == node->pathfindingPath[k + 1]->j) {
      continue;
    } else {
      Vector2D_t* point = &node->path[z++];
      point->x = node->pathfindingPath[k]->i * mapResolution - robotOffsetX;
      point->y = node->pathfindingPath[k]->j * mapResolution - robotOffsetY;
    }
  }

  node->pathLength = z;
}

void mockMap(ProximityMapping_node_t* node) {
  for (int i = 20; i < 80; i++) {
    for (int j = 20; j < 80; j++) {
      if (i > 35 && i < 65 && j == 44) {
        continue;
      }

      node->map[i][j].value = 10;
    }
  }
}

//
// SHELL
//
int pmSetGains(ProximityMapping_node_t* node, BaseSequentialStream* stream,
               int argc, char* argv[]) {
  if (argc != 4) {
    chprintf(stream, "invalid args\n");
    return AOS_ERROR;
  }

  float gains[3] = {0};
  for (int i = 0; i < 3; i++) {
    gains[i] = atof(argv[i + 1]);

    if (!isfinite(gains[i]) || gains[i] < 0.0f) {
      chprintf(stream, "invalid args\n");
      return AOS_ERROR;
    }
  }

  node->_p = gains[0];
  node->_i = gains[1];
  node->_d = gains[2];

  chprintf(stream, "p, i, d set to (%f, %f, %f)", node->_p, node->_i, node->_d);

  return AOS_OK;
}

int pmPrintCorners(ProximityMapping_node_t* node, BaseSequentialStream* stream,
                   int argc, char* argv[]) {
  urtDebugAssert(node != NULL);

  (void)argc;
  (void)argv;

  chprintf(stream, "corners:\n");
  for (int i = 0; i < node->cornerIndex; i++) {
    chprintf(stream, "(%f, %f)\n", node->corners[i].x, node->corners[i].y);
  }

  return AOS_OK;
}

int pmPrintPath(ProximityMapping_node_t* node, BaseSequentialStream* stream,
                int argc, char* argv[]) {
  urtDebugAssert(node != NULL);

  (void)argc;
  (void)argv;

  chprintf(stream, "path:\n");
  for (int i = 0; i < node->pathLength; i++) {
    chprintf(stream, "(%f, %f)\n", node->path[i].x, node->path[i].y);
  }

  return AOS_OK;
}

int pmPrintMap(ProximityMapping_node_t* node, BaseSequentialStream* stream,
               int argc, char* argv[]) {
  urtDebugAssert(node != NULL);

  (void)argc;
  (void)argv;

  for (int i = 0; i < X; i++) {
    for (int j = 0; j < Y; j++) {
      int val = node->map[i][j].value;
      if (val > 0)
        chprintf(stream, "\xE2\x96\x88");
      else
        chprintf(stream, "\xE2\x96\x91");
    }

    chprintf(stream, "\n");
  }

  return AOS_OK;
}

//
// EVENTS
//
urt_osEventMask_t _ProximityMapping_Setup(
    urt_node_t* node, ProximityMapping_node_t* ProximityMapping) {
  urtDebugAssert(ProximityMapping != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_ProximityMapping_name);

#if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the floor proximity topic
  urt_topic_t* const floor_prox_topic =
      urtCoreGetTopic(ProximityMapping->floor_prox.topicid);
  urtDebugAssert(floor_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&ProximityMapping->floor_prox.nrt, floor_prox_topic,
                            PROXFLOOREVENT, NULL);

  // subscribe to the ring proximity topic
  urt_topic_t* const ring_prox_topic =
      urtCoreGetTopic(ProximityMapping->ring_prox.topicid);
  urtDebugAssert(ring_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&ProximityMapping->ring_prox.nrt, ring_prox_topic,
                            PROXRINGEVENT, NULL);

  // subscribe to the node->mappingOdometry topic
  urt_topic_t* const odom_topic =
      urtCoreGetTopic(ProximityMapping->odom_data.topicid);
  urtDebugAssert(odom_topic != NULL);
  urtNrtSubscriberSubscribe(&ProximityMapping->odom_data.nrt, odom_topic,
                            ODOMEVENT, NULL);

  // Subscribe to the touch sensor topic
  urt_topic_t* const touch_sensor_topic =
      urtCoreGetTopic(ProximityMapping->touch_data.topicid);
  urtDebugAssert(touch_sensor_topic != NULL);
  urtNrtSubscriberSubscribe(&ProximityMapping->touch_data.nrt,
                            touch_sensor_topic, TOUCHEVENT, NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  return PROXFLOOREVENT | PROXRINGEVENT | ODOMEVENT | TOUCHEVENT;
}

/**
 * @brief   Loop callback function for ProximityMapping nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] ProximityMapping    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _ProximityMapping_Loop(
    urt_node_t* node, urt_osEventMask_t event,
    ProximityMapping_node_t* ProximityMapping) {
  urtDebugAssert(ProximityMapping != NULL);
  (void)node;
  (void)event;

  getData(ProximityMapping);

  ProximityMapping->motor_data.data.rotation.vector[2] = 0.0;
  ProximityMapping->motor_data.data.translation.axes[0] = 0.0;

  switch (ProximityMapping->state) {
    case PM_ALIGN:
      checkAlignment(ProximityMapping);
      break;
    case PM_ALIGN_BOUNDARY:
      alignWithBoundary(ProximityMapping);
      break;
    case PM_ALIGN_GOAL:
      alignToGoal(ProximityMapping);
      break;
    case PM_FIND_CORNER:
      findCorner(ProximityMapping);
      break;
    case PM_FIND_WALL:
      findWall(ProximityMapping);
      break;
    case PM_INIT:
      if (ProximityMapping->touch_data.data.values.data[0] == 1) {
        setWallDir(ProximityMapping, PM_FRONT);
        setState(ProximityMapping, PM_ALIGN_BOUNDARY);
        pushStack(&ProximityMapping->stateStack, PM_RESET);
        pushStack(&ProximityMapping->stateStack, PM_SET_DISTANCE);
      }
      break;
    case PM_MOVE_GOAL:
      moveToGoal(ProximityMapping);
      break;
    case PM_RESET:
      ProximityMapping->recordedPathLength = 0;
      // TODO: reset node->mappingOdom..
      //  node->mappingOdom.vec.x = node->mappingOdom.vec.y =
      //  node->mappingOdom.phi = 0;
      align(ProximityMapping);
      setState(ProximityMapping, PM_ALIGN_BOUNDARY);
      pushStack(&ProximityMapping->stateStack, PM_TRACE_BOUNDARY);
      break;
    case PM_SET_DISTANCE:
      setDistance(ProximityMapping);
      break;
    case PM_SET_DISTANCE_CORNER:
      setDistanceCorner(ProximityMapping);
      break;
    case PM_SET_OUTER_CORNER:
      setOuterCorner(ProximityMapping);
      break;
    case PM_STOP:
      break;
    case PM_STRIP_CORRECT:
      findCorner(ProximityMapping);
      break;
    case PM_STRIP_CROSS:
      stripCross(ProximityMapping);
      break;
    case PM_STRIP_MAP:
      stripMap(ProximityMapping);
      break;
    case PM_STRIP_TRACE:
      traceBoundary(ProximityMapping);
      break;
    case PM_TRACE_BOUNDARY:
      traceBoundary(ProximityMapping);
      break;
    case PM_TURN_90:
      turn90Deg(ProximityMapping);
      break;
    case PM_FIND_PATH:
      if (ProximityMapping->touch_data.data.values.data[0] == 1) {
        ProximityMapping->goal.vec.x = 0.0;
        ProximityMapping->goal.vec.y = 0.5;

        findPath(ProximityMapping);
        setState(ProximityMapping, PM_FOLLOW_PATH);
      }
      break;
    case PM_FOLLOW_PATH:
      if (ProximityMapping->pathIndex >= ProximityMapping->pathLength) {
        setState(ProximityMapping, PM_STOP);
        break;
      }

      urtPrintf("path:\n");
      for (int i = 0; i < ProximityMapping->pathLength; i++) {
        urtPrintf("(%f, %f)\n", ProximityMapping->path[i].x,
                  ProximityMapping->path[i].y);
      }

      Vector2D_t* goal = &ProximityMapping->path[ProximityMapping->pathIndex++];

      ProximityMapping->goal.vec.x = goal->x;
      ProximityMapping->goal.vec.y = goal->y;

      setState(ProximityMapping, PM_ALIGN_GOAL);
      pushStack(&ProximityMapping->stateStack, PM_FOLLOW_PATH);
      pushStack(&ProximityMapping->stateStack, PM_MOVE_GOAL);
      break;
    case PM_IDENTIFY_OBJECT:
      roundObject(ProximityMapping);
      break;
    default:
      break;
  }

  if (ProximityMapping->state != PM_INIT) signalMotorRecord(ProximityMapping);

  return PROXFLOOREVENT | PROXRINGEVENT | ODOMEVENT | TOUCHEVENT;
}

/**
 * @brief   Shutdown callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 */
void _ProximityMapping_Shutdown(urt_node_t* node, urt_status_t reason,
                                ProximityMapping_node_t* ProximityMapping) {
  urtDebugAssert(ProximityMapping != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&ProximityMapping->ring_prox.nrt);
  urtNrtSubscriberUnsubscribe(&ProximityMapping->floor_prox.nrt);
  urtNrtSubscriberUnsubscribe(&ProximityMapping->odom_data.nrt);
  urtNrtSubscriberUnsubscribe(&ProximityMapping->touch_data.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void ProximityMappingInit(ProximityMapping_node_t* ProximityMapping,
                          urt_osThreadPrio_t prio) {
  urtDebugAssert(ProximityMapping != NULL);

#if (URT_CFG_PUBSUB_ENABLED == true)
  urtNrtSubscriberInit(&ProximityMapping->ring_prox.nrt);
  urtNrtSubscriberInit(&ProximityMapping->floor_prox.nrt);
  urtNrtSubscriberInit(&ProximityMapping->odom_data.nrt);
  urtNrtSubscriberInit(&ProximityMapping->touch_data.nrt);
#endif /* URT_CFG_PUBSUB_ENABLED == true */
#if (URT_CFG_RPC_ENABLED == true)
  urtNrtRequestInit(&ProximityMapping->motor_data.request,
                    &ProximityMapping->motor_data.data);
#endif /* (URT_CFG_RPC_ENABLED == true) */
  ProximityMapping->state = PM_FIND_PATH;
  ProximityMapping->traceState = PM_BOUNDARY;
  ProximityMapping->wallDir = PM_UNKNOWN;
  ProximityMapping->previousWallDirection = PM_UNKNOWN;
  ProximityMapping->mappingOdom.vec = *createVec(0.0, 0.0);
  ProximityMapping->stripRootCorner = NULL;

  ProximityMapping->_p = P;
  ProximityMapping->_i = I;
  ProximityMapping->_d = D;
  ProximityMapping->ODDirection = OD_STRAIGHT;
  ProximityMapping->ODStartTime = 0;

  for (int i = 0; i < MOTOR_BUFFER_SIZE; i++) {
    ProximityMapping->translationBuffer[i] = 0.0;
    ProximityMapping->rotationBuffer[i] = 0.0;
  }

  for (int i = 0; i < X; i++) {
    for (int j = 0; j < Y; j++) {
      ProximityMapping->cameFrom[i][j] = NULL;
      ProximityMapping->map[i][j].i = i;
      ProximityMapping->map[i][j].j = j;
    }
  }

  mockMap(ProximityMapping);

  ProximityMapping->mappingOdom.vec.x = 0.0;
  ProximityMapping->mappingOdom.vec.y = -0.5;

  ProximityMapping->previousRotation = 0.0;
  ProximityMapping->previousTranslation = 0.0;

  // initialize the node
  urtNodeInit(
      &ProximityMapping->node, (urt_osThread_t*)ProximityMapping->thread,
      sizeof(ProximityMapping->thread), prio,
      (long unsigned int (*)(struct urt_node*, void*))_ProximityMapping_Setup,
      ProximityMapping,
      (long unsigned int (*)(struct urt_node*, long unsigned int,
                             void*))_ProximityMapping_Loop,
      ProximityMapping,
      (void (*)(struct urt_node*, enum urt_status,
                void*))_ProximityMapping_Shutdown,
      ProximityMapping);
}
