/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo)
platform. Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    RandomWalk.h
 * @brief   Proximity Mapping..
 *
 * @addtogroup RandomWalk
 * @{
 */

#ifndef RandomWalk_H
#define RandomWalk_H

#include <urt.h>

#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/TouchSensordata.h"
#include "../../messagetypes/motiondata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(RandomWalk_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of RandomWalk threads.
 */
#define RandomWalk_STACKSIZE 1024
#endif /* !defined(RandomWalk_STACKSIZE) */

#define PROXRINGEVENT (urt_osEventMask_t)(1 << 1)
#define TOUCHEVENT (urt_osEventMask_t)(1 << 2)
#define AMBFLOOREVENT (urt_osEventMask_t)(1 << 3)
#define PROXFLOOREVENT (urt_osEventMask_t)(1 << 4)
// #define ODOMEVENT (urt_osEventMask_t)(1 << 5)

#define WALL 2780
#define ENEMY_PROX 5000
#define SPEED 0.45
#define ROT_SPEED 4.0
#define HUNT_SPEED 0.1
#define HUNT_ROT_SPEED 1.7
#define FRONT_MARKER_THRESHOLD 8000
#define WHEEL_MARKER_THRESHOLD 8000
#define MARKER_SLOWDOWN_THRESHOLD 17000
#define ROT_STATIC 200
#define ROT_RANDOM 200
#define TIMEOUT_MS 200

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

typedef enum TurnDirection { RW_LEFT, RW_RIGHT } TurnDirection_t;

typedef enum RandomWalkState {
  RW_INIT,
  RW_SEARCH,
  RW_TURN,
  RW_PROTECT,
  RW_STOP
} RandomWalkState_t;

/**
 * @brief   Trigger related data of the floor sensors.
 */
typedef struct floor {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  floor_sensors_t data;
} floor_t;

/**
 * @brief  Data of the motor
 */
typedef struct motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t *service;
} motor_t;

/**
 * @brief   Trigger related data of the ring sensors.
 */
typedef struct ring {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  ring_sensors_t data;
} ring_t;

/**
 * @brief  Data of the touch sensors
 */
typedef struct touch {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  touch_data_t data;
} touch_t;

/**
 * @brief   RandomWalk node.
 * @struct  RandomWalk_node
 */
typedef struct RandomWalk_node {
  URT_THREAD_MEMORY(thread, RandomWalk_STACKSIZE);
  urt_node_t node;

  floor_t floor_prox;
  floor_t floor_amb;
  ring_t ring_prox;
  motor_t motor_data;
  touch_t touch_data;

  RandomWalkState_t state;

  TurnDirection_t turnDirection;
  int msTurn;
  int lastFrontSensorRead;
  urt_osTime_t startTurn;
  urt_osTime_t timeout;
} RandomWalk_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/
#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
void RandomWalkInit(RandomWalk_node_t *RandomWalk, urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* HELLOWORLD_H */

/** @} */
