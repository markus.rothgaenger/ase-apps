/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo)
platform. Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "RandomWalk.h"

#include <amiroos.h>
#include <math.h>
#include <stdlib.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of RandomWalk nodes.
 */
static const char _RandomWalk_name[] = "RandomWalk";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

char* printState(RandomWalkState_t state) {
  switch (state) {
    case RW_TURN:
      return "RW_TURN";
    case RW_INIT:
      return "RW_INIT";
    case RW_STOP:
      return "RW_STOP";
    case RW_SEARCH:
      return "RW_SEARCH";
    case RW_PROTECT:
      return "RW_PROTECT";
    default:
      return "";
  }
}

void setState(RandomWalk_node_t* node, RandomWalkState_t _state) {
  node->state = _state;
  urtPrintf("state updated %s\n", printState(node->state));
}

void signalMotorService(RandomWalk_node_t* RandomWalk) {
  urtDebugAssert(RandomWalk != NULL);

  if (urtNrtRequestTryAcquire(&RandomWalk->motor_data.request) ==
      URT_STATUS_OK) {
    urtNrtRequestSubmit(&RandomWalk->motor_data.request,
                        RandomWalk->motor_data.service, 0);
  }
}

void getData(RandomWalk_node_t* RandomWalk) {
  urtDebugAssert(RandomWalk != NULL);

  // local variables
  urt_status_t status;

  // fetch NRT of the floor proximity data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &RandomWalk->floor_prox.nrt, &RandomWalk->floor_prox.data,
        sizeof(RandomWalk->floor_prox.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the ring proximity data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &RandomWalk->ring_prox.nrt, &RandomWalk->ring_prox.data,
        sizeof(RandomWalk->ring_prox.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the touch sensor data
  do {
    status = urtNrtSubscriberFetchNextMessage(
        &RandomWalk->touch_data.nrt, &RandomWalk->touch_data.data,
        sizeof(RandomWalk->touch_data.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the ambient sensor data

  do {
    status = urtNrtSubscriberFetchNextMessage(
        &RandomWalk->floor_amb.nrt, &RandomWalk->floor_amb.data,
        sizeof(RandomWalk->floor_amb.data), NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  return;
}

bool probablyFoundMarker(RandomWalk_node_t* node) {
  if (node->floor_prox.data.values.right_front < MARKER_SLOWDOWN_THRESHOLD ||
      node->floor_prox.data.values.left_front < MARKER_SLOWDOWN_THRESHOLD ||
      node->floor_prox.data.values.left_wheel < MARKER_SLOWDOWN_THRESHOLD ||
      node->floor_prox.data.values.right_wheel < MARKER_SLOWDOWN_THRESHOLD) {
    return true;
  }
  return false;
}

bool foundMarker(RandomWalk_node_t* node) {
  if (node->floor_prox.data.values.right_front < FRONT_MARKER_THRESHOLD ||
      node->floor_prox.data.values.left_front < FRONT_MARKER_THRESHOLD ||
      node->floor_prox.data.values.left_wheel < WHEEL_MARKER_THRESHOLD ||
      node->floor_prox.data.values.right_wheel < WHEEL_MARKER_THRESHOLD) {
    return true;
  }
  return false;
}

bool foundMarkerLeft(RandomWalk_node_t* node) {
  if (node->floor_prox.data.values.left_front < FRONT_MARKER_THRESHOLD ||
      node->floor_prox.data.values.left_wheel < WHEEL_MARKER_THRESHOLD) {
    return true;
  }
  return false;
}

bool foundMarkerRight(RandomWalk_node_t* node) {
  if (node->floor_prox.data.values.right_front < FRONT_MARKER_THRESHOLD ||
      node->floor_prox.data.values.right_wheel < WHEEL_MARKER_THRESHOLD) {
    return true;
  }
  return false;
}

void hunt(RandomWalk_node_t* node) {
  for (int i = 0; i < 8; i++) {
    if (node->ring_prox.data.data[i] > ENEMY_PROX) {
      setState(node, RW_STOP);
      return;
    }
  }

  float rotSpeed =
      node->turnDirection == RW_LEFT ? HUNT_ROT_SPEED : -HUNT_ROT_SPEED;

  if (!foundMarker(node))
    node->motor_data.data.rotation.vector[2] = rotSpeed;
  else
    node->motor_data.data.rotation.vector[2] = -0.35 * rotSpeed;

  node->motor_data.data.translation.axes[0] = HUNT_SPEED;
}

void turn(RandomWalk_node_t* node) {
  if (foundMarker(node)) {
    setState(node, RW_PROTECT);
    return;
  }

  int turnTime = round((urtTimeNow() - node->startTurn) /
                       ((double)MICROSECONDS_PER_MILLISECOND));

  if (turnTime > node->msTurn) {
    node->timeout = urtTimeNow() + TIMEOUT_MS * MICROSECONDS_PER_MILLISECOND;
    setState(node, RW_SEARCH);
  }

  node->motor_data.data.rotation.vector[2] =
      node->turnDirection == RW_LEFT ? ROT_SPEED : -ROT_SPEED;
}

void search(RandomWalk_node_t* node) {
  if (foundMarkerLeft(node)) {
    node->turnDirection = RW_LEFT;
    setState(node, RW_PROTECT);
    return;
  }
  if (foundMarkerRight(node)) {
    node->turnDirection = RW_RIGHT;
    setState(node, RW_PROTECT);
    return;
  }

  int maxSensorIndex = 0;
  uint16_t maxSensorRead = node->ring_prox.data.data[maxSensorRead];

  for (int i = 0; i < 8; i++) {
    if (i > 1 && i < 6) continue;

    if (node->ring_prox.data.data[i] > maxSensorRead) {
      maxSensorRead = node->ring_prox.data.data[i];
      maxSensorIndex = i;
    }
  }

  unsigned int currentFrontSensorRead =
      (node->ring_prox.data.values.nne + node->ring_prox.data.values.nnw +
       node->ring_prox.data.values.ene + node->ring_prox.data.values.wnw) /
      4.0;

  urt_osTime_t now = urtTimeNow();

  if (now > node->timeout &&
      currentFrontSensorRead - node->lastFrontSensorRead > 0.0 &&
      (node->ring_prox.data.values.nne > WALL ||
       node->ring_prox.data.values.nnw > WALL ||
       node->ring_prox.data.values.ene > WALL ||
       node->ring_prox.data.values.wnw > WALL)) {
    if (maxSensorIndex > 1) {
      node->turnDirection = RW_LEFT;
    } else {
      node->turnDirection = RW_RIGHT;
    }

    node->startTurn = now;
    node->msTurn = ROT_STATIC + (rand() % ROT_RANDOM);

    setState(node, RW_TURN);
    node->motor_data.data.translation.axes[0] = -SPEED;
    return;
  }

  node->lastFrontSensorRead = currentFrontSensorRead;

  float speedFactor = WALL / maxSensorRead;

  if (probablyFoundMarker(node))
    node->motor_data.data.translation.axes[0] = 0.2;
  else
    node->motor_data.data.translation.axes[0] = fmax(0.2, speedFactor * SPEED);
}

//
// EVENTS
//

urt_osEventMask_t _RandomWalk_Setup(urt_node_t* node,
                                    RandomWalk_node_t* RandomWalk) {
  urtDebugAssert(RandomWalk != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_RandomWalk_name);

#if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the floor proximity topic
  urt_topic_t* const floor_prox_topic =
      urtCoreGetTopic(RandomWalk->floor_prox.topicid);
  urtDebugAssert(floor_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&RandomWalk->floor_prox.nrt, floor_prox_topic,
                            PROXFLOOREVENT, NULL);

  urt_topic_t* const floor_amb_topic =
      urtCoreGetTopic(RandomWalk->floor_amb.topicid);
  urtDebugAssert(floor_amb_topic != NULL);
  urtNrtSubscriberSubscribe(&RandomWalk->floor_amb.nrt, floor_amb_topic,
                            AMBFLOOREVENT, NULL);

  // subscribe to the ring proximity topic
  urt_topic_t* const ring_prox_topic =
      urtCoreGetTopic(RandomWalk->ring_prox.topicid);
  urtDebugAssert(ring_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&RandomWalk->ring_prox.nrt, ring_prox_topic,
                            PROXRINGEVENT, NULL);

  // Subscribe to the touch sensor topic
  urt_topic_t* const touch_sensor_topic =
      urtCoreGetTopic(RandomWalk->touch_data.topicid);
  urtDebugAssert(touch_sensor_topic != NULL);
  urtNrtSubscriberSubscribe(&RandomWalk->touch_data.nrt, touch_sensor_topic,
                            TOUCHEVENT, NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  return PROXFLOOREVENT | PROXRINGEVENT | TOUCHEVENT | AMBFLOOREVENT;
}

/**
 * @brief   Loop callback function for RandomWalk nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] RandomWalk    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _RandomWalk_Loop(urt_node_t* node, urt_osEventMask_t event,
                                   RandomWalk_node_t* RandomWalk) {
  urtDebugAssert(RandomWalk != NULL);
  (void)node;
  (void)event;

  // get the proximity and ambient data of the ring and floor sensors and the
  // node->mappingOdometry data
  getData(RandomWalk);

  // TODO: reset function
  RandomWalk->motor_data.data.rotation.vector[2] = 0.0;
  RandomWalk->motor_data.data.translation.axes[0] = 0.0;

  if (RandomWalk->state != RW_INIT &&
      RandomWalk->touch_data.data.values.data[2] == 1 &&
      RandomWalk->touch_data.data.values.data[3] == 1) {
    setState(RandomWalk, RW_INIT);
  }

  // urtPrintf("%d %d %d %d\n", RandomWalk->floor_prox.data.values.left_wheel,
  //           RandomWalk->floor_prox.data.values.left_front,
  //           RandomWalk->floor_prox.data.values.right_front,
  //           RandomWalk->floor_prox.data.values.right_wheel);

  switch (RandomWalk->state) {
    case RW_INIT:
      if (RandomWalk->touch_data.data.values.data[0] == 1 &&
          RandomWalk->touch_data.data.values.data[1] == 1) {
        setState(RandomWalk, RW_SEARCH);
      }
      break;
    case RW_TURN:
      turn(RandomWalk);
      break;
    case RW_SEARCH:
      search(RandomWalk);
      break;
    case RW_PROTECT:
      hunt(RandomWalk);
      break;
    case RW_STOP:
    default:
      break;
  }

  signalMotorService(RandomWalk);

  return PROXFLOOREVENT | PROXRINGEVENT | TOUCHEVENT | AMBFLOOREVENT;
}

/**
 * @brief   Shutdown callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 */
void _RandomWalk_Shutdown(urt_node_t* node, urt_status_t reason,
                          RandomWalk_node_t* RandomWalk) {
  urtDebugAssert(RandomWalk != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&RandomWalk->floor_amb.nrt);
  urtNrtSubscriberUnsubscribe(&RandomWalk->ring_prox.nrt);
  urtNrtSubscriberUnsubscribe(&RandomWalk->floor_prox.nrt);
  urtNrtSubscriberUnsubscribe(&RandomWalk->touch_data.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void RandomWalkInit(RandomWalk_node_t* RandomWalk, urt_osThreadPrio_t prio) {
  urtDebugAssert(RandomWalk != NULL);

#if (URT_CFG_PUBSUB_ENABLED == true)
  urtNrtSubscriberInit(&RandomWalk->ring_prox.nrt);
  urtNrtSubscriberInit(&RandomWalk->floor_prox.nrt);
  urtNrtSubscriberInit(&RandomWalk->floor_amb.nrt);
  urtNrtSubscriberInit(&RandomWalk->touch_data.nrt);
#endif /* URT_CFG_PUBSUB_ENABLED == true */
#if (URT_CFG_RPC_ENABLED == true)
  urtNrtRequestInit(&RandomWalk->motor_data.request,
                    &RandomWalk->motor_data.data);
#endif /* (URT_CFG_RPC_ENABLED == true) */
  RandomWalk->state = RW_INIT;

  // initialize the node
  urtNodeInit(
      &RandomWalk->node, (urt_osThread_t*)RandomWalk->thread,
      sizeof(RandomWalk->thread), prio,
      (long unsigned int (*)(struct urt_node*, void*))_RandomWalk_Setup,
      RandomWalk,
      (long unsigned int (*)(struct urt_node*, long unsigned int,
                             void*))_RandomWalk_Loop,
      RandomWalk,
      (void (*)(struct urt_node*, enum urt_status, void*))_RandomWalk_Shutdown,
      RandomWalk);
}
