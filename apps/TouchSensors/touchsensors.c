/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <touchsensors.h>
#include <amiroos.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of touchsensors nodes.
 */
static const char _touchsensors_name[] = "TouchSensors";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _touchsensors_triggercb(void* params)
{
  (void)params;

  // broadcast ring event
  chEvtBroadcastFlagsI(&((touchsensors_node_t*)params)->trigger.source, (urt_osEventFlags_t)0);

  return;
}

/**
 * @brief   Setup callback function for touchsensors nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] touchsensors    Pointer to the touchsensors structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _touchsensors_Setup(urt_node_t* node, void* touchsensors)
{
  urtDebugAssert(touchsensors != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_touchsensors_name);

  // register trigger event
  urtEventRegister(&((touchsensors_node_t*)touchsensors)->trigger.source,
                   &((touchsensors_node_t*)touchsensors)->trigger.listener,
                   TRIGGEREVENT, 0);

  // soft reset of the touch sensors
  mpr121_lld_soft_reset(((touchsensors_node_t*)touchsensors)->driver_data.mprd,
                        ((touchsensors_node_t*)touchsensors)->driver_data.timeout);

  uint8_t wdata = MPR121_LLD_CDT_32 | MPR121_LLD_SFI_10 | MPR121_LLD_ESI_16;
  mpr121_lld_write_register(((touchsensors_node_t*)touchsensors)->driver_data.mprd,
                            MPR121_LLD_REGISTER_CONFIG_2, 0, 1,
                            &wdata,
                            ((touchsensors_node_t*)touchsensors)->driver_data.timeout);

  mpr121_lld_config_t cfg;
  cfg.registers.ele_config = MPR121_LLD_CL_ON_ALL | MPR121_LLD_ELEPROX_0 | 4;
  cfg.registers.config_1 = MPR121_LLD_FFI_18 | 16;
  cfg.registers.config_2 = MPR121_LLD_CDT_1 | MPR121_LLD_SFI_10 | MPR121_LLD_ESI_32;
  cfg.registers.up_side_limit = 0x96u;
  cfg.registers.low_side_limit = 0x58u;
  cfg.registers.target_level = 0x68u;
  cfg.registers.auto_config_1 = MPR121_LLD_FFI_18 | MPR121_LLD_RETRY_2 | MPR121_LLD_BVA_ON_ALL | MPR121_LLD_AC_RECONF_EN;

  mpr121_lld_write_config(((touchsensors_node_t*)touchsensors)->driver_data.mprd,
                          cfg,
                          ((touchsensors_node_t*)touchsensors)->driver_data.timeout);

  // activate the timer
  aosTimerPeriodicInterval(&((touchsensors_node_t*)touchsensors)->trigger.timer,
                           (1.0f / ((touchsensors_node_t*)touchsensors)->driver_data.frequency) * MICROSECONDS_PER_SECOND,
                           _touchsensors_triggercb, touchsensors);

  return TRIGGEREVENT;
}

/**
 * @brief   Loop callback function for touchsensors nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] touchsensors   Pointer to the touchsensors structure.
 *                    Must not be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _touchsensors_Loop(urt_node_t* node, urt_osEventMask_t event, void* touchsensors)
{
  urtDebugAssert(touchsensors != NULL);

  (void)node;

  switch(event) {
    case TRIGGEREVENT:
    {
      mpr121_lld_read_register(((touchsensors_node_t*)touchsensors)->driver_data.mprd,
                               MPR121_LLD_REGISTER_TOUCH_STATUS,
                               0, 1,
                               &((touchsensors_node_t*)touchsensors)->driver_data.tstate,
                               ((touchsensors_node_t*)touchsensors)->driver_data.timeout);
      ((touchsensors_node_t*)touchsensors)->driver_data.touch_data.data[0] = ((touchsensors_node_t*)touchsensors)->driver_data.tstate & 1;
      ((touchsensors_node_t*)touchsensors)->driver_data.touch_data.data[1] = (((touchsensors_node_t*)touchsensors)->driver_data.tstate >> 1) & 1;
      ((touchsensors_node_t*)touchsensors)->driver_data.touch_data.data[2] = (((touchsensors_node_t*)touchsensors)->driver_data.tstate >> 2) & 1;
      ((touchsensors_node_t*)touchsensors)->driver_data.touch_data.data[3] = (((touchsensors_node_t*)touchsensors)->driver_data.tstate >> 3) & 1;

      urt_status_t status;
      // publish the touch sensor data
      status = urtPublisherTryPublish(&((touchsensors_node_t*)touchsensors)->publisher,
                        &((touchsensors_node_t*)touchsensors)->driver_data.touch_data,
                        sizeof(((touchsensors_node_t*)touchsensors)->driver_data.touch_data),
                        urtTimeNow());
      break; 
    }
    default: break;
  }

  return TRIGGEREVENT;
}

/**
 * @brief   Shutdown callback function for touchsensors nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] master  Pointer to the touchsensors structure.
 *                    Must nor be NULL.
 */
void _touchsensors_Shutdown(urt_node_t* node, urt_status_t reason, void* touchsensors)
{
  urtDebugAssert(touchsensors != NULL);

  (void)node;
  (void)reason;

  // unregister trigger event
  urtEventUnregister(&((touchsensors_node_t*)touchsensors)->trigger.source,
                     &((touchsensors_node_t*)touchsensors)->trigger.listener);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void touchsensorsInit(touchsensors_node_t* touchsensors, urt_topicid_t touch_topicid, urt_osThreadPrio_t prio)
{
  urtDebugAssert(touchsensors != NULL);

  // set/initialize event data
  urtEventSourceInit(&touchsensors->trigger.source);
  urtEventListenerInit(&touchsensors->trigger.listener);
  // initialize the timer
  urtTimerInit(&touchsensors->trigger.timer);
#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize the publisher for the touch sensor data
  urtPublisherInit(&touchsensors->publisher, urtCoreGetTopic(touch_topicid), NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  touchsensors->driver_data.mprd = &moduleLldTouch;
  touchsensors->driver_data.timeout = MICROSECONDS_PER_SECOND;

  // initialize the node
  urtNodeInit(&touchsensors->node, (urt_osThread_t*)touchsensors->thread, sizeof(touchsensors->thread), prio,
              _touchsensors_Setup, touchsensors,
              _touchsensors_Loop, touchsensors,
              _touchsensors_Shutdown, touchsensors);

  return;
}
