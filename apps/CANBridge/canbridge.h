/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CANBRIDGE_H
#define CANBRIDGE_H

#include <amiroos.h>
#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/positiondata.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(CAN_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of can threads.
 */
#define CAN_STACKSIZE             256
#endif /* !defined(CAN_STACKSIZE) */

/**
 * @brief   Event mask to set for the can light service.
 */
#define CANLIGHTSERVICEEVENT          (urt_osEventMask_t)(1<< 1)

/**
 * @brief   Event mask to set for the can motor service.
 */
#define CANMOTORSERVICEEVENT          (urt_osEventMask_t)(1<< 2)

/**
 * @brief   Event mask which is notified when a new frame is in the queue.
 */
#define CANFRAMEEVENT                 (urt_osEventMask_t)(1<< 3)

/**
 * @brief   Event mask which is notified when new odometry data are there.
 */
#define ODOMETRYEVENT                 (urt_osEventMask_t)(1<< 4)

/**
 * @brief   Types of the can frame
 */
#define CAN_REQUEST                                        1
#define CAN_TOPIC_DATA                                     2

// IDs must be 2^x so that they can be separated by the CANBridge filter
#define PROXIMITY_RING_TOPICID        1 //Topic identifier for the proximity ring topic
#define AMBIENT_RING_TOPICID          2 //Topic identifier for the ambient ring topic
#define PROXIMITY_FLOOR_TOPICID       4 //Topic identifier for the proximity floor topic
#define AMBIENT_FLOOR_TOPICID         8 //Topic identifier for the ambient floor topic
#define ODOM_TOPICID                  16 //Topic identifier for the odometry topicid
#define DMC_SERVICE_ID                32 //Service identifier of the DMC service
#define LIGHT_SERVICE_ID              64 //Service identifier of the light service
#define TOUCH_SENSORS_TOPICID         128 //Topic identifier for the touch sensor topic


/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Event flag to identify a floor can frame event
 */
#define CAN_PROX_FLOORFRAME                       (urt_osEventFlags_t)(1 << 1)
#define CAN_AMB_FLOORFRAME                        (urt_osEventFlags_t)(1 << 2)

/**
 * @brief   Event flag to identify a ring can frame event
 */
#define CAN_PROX_RINGFRAME                        (urt_osEventFlags_t)(1 << 3)
#define CAN_AMB_RINGFRAME                         (urt_osEventFlags_t)(1 << 4)

/**
 * @brief   Event flag to identify a odom frame event
 */
#define CAN_ODOM_FRAME                            (urt_osEventFlags_t)(1 << 5)

/**
 * @brief   Event flag to identify a request can frame event
 */
#define CAN_LIGHT_REQUESTFRAME                    (urt_osEventFlags_t)(1 << 6)
#define CAN_MOTOR_REQUESTFRAME                    (urt_osEventFlags_t)(1 << 7)


typedef union id_map {
  uint32_t raw;
  struct {
    uint32_t cnt : 11;
    uint32_t id : 16;
    uint32_t type : 2;
  };
}id_map_t;

#if (CAN_REQUEST_ENABLE == true || CAN_MOTOR_SERVICE_ENABLE == true)
/**
 * @brief   Motion via the x Euler axes and the yaw Euler angles.
 * @details Simplified motion representing only the relevant movements for transmitting the frame
 */
typedef struct simple_motion
{
  /**
   * @brief Translation along the x Euler axes.
   */
  float x_translation;

  /**
   * @brief Rotation along the yaw Euler angle.
   */
  float yaw_rotation;
} simple_motion_t;
#endif /* (CAN_REQUEST_ENABLE == true || CAN_MOTOR_SERVICE_ENABLE == true) */

#if (CAN_PUB_ODOM_ENABLE == true || defined(__DOXYGEN__))
typedef struct pub_odom {
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  urt_publisher_t pub;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
  urt_topicid_t topicid; //Id of the topic where the data which are transmitted over the CAN Bus should be published
  urt_osEventMask_t mask; //mask when this publisher should publish (CAN callback)
  size_t size; //size of the published data

  position_cv_si odom_data[2];
  urt_osTime_t timestamp[2];
  position_cv_si* data_read; //pointer to the odom data which should be published
}pub_odom_t;
#endif /* CAN_PUB_ODOM_ENABLE == true */

#if (CAN_PUB_PROX_FLOOR_ENABLE == true || CAN_PUB_AMB_FLOOR_ENABLE == true) || defined(__DOXYGEN__)
typedef struct pub_floor{
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  urt_publisher_t pub;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
  urt_topicid_t topicid; //Id of the topic where the data which are transmitted over the CAN Bus should be published
  urt_osEventMask_t mask; //mask when this publisher should publish (CAN callback)
  size_t size; //size of the published data

  floor_data_t floor_data[2];
  urt_osTime_t timestamp[2];
  floor_data_t* data_read; //pointer to the floor data which should be published
}pub_floor_t;
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */

#if (CAN_PUB_PROX_RING_ENABLE == true || CAN_PUB_AMB_RING_ENABLE == true) || defined(__DOXYGEN__)
typedef struct pub_ring{
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  urt_publisher_t pub;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
  urt_topicid_t topicid; //Id of the topic where the data which are transmitted over the CAN Bus should be published
  urt_osEventMask_t mask; //mask when this publisher should publish (CAN callback)
  size_t size; //size of the published data

  ring_data_t ring_data[2];
  urt_osTime_t timestamp[2];
  ring_data_t* data_read; //pointer to the ring data which should be published
}pub_ring_t;
#endif /* (CAN_PUB_RING_ENABLE == true) */

#if (CAN_SUB_PROX_ENABLE == true || CAN_SUB_AMB_ENABLE == true) || defined(__DOXYGEN__)
typedef struct sub {
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  urt_nrtsubscriber_t nrt_sub;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
  urt_topicid_t topicid; //Id of the topic where the data which should be transmitted over the CAN Bus are published on
  urt_osEventMask_t mask; //mask where this subscriber subscribes to (CAN Loop)
  size_t size; //size of the received data

  /**
   * @brief Data which should be transmitted over the CAN Bus
   */
  union {
      floor_data_t floor_data;
      ring_data_t ring_data;
  };
  urt_osTime_t timestamp; //Timestamp of the data which should be transmitted
}sub_t;
#endif /* (CAN_SUB_ENABLE == true) */

#if (CAN_SUB_ODOM_ENABLE == true) || defined(__DOXYGEN__)
typedef struct sub_odom {
#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  urt_nrtsubscriber_t nrt_sub;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */
  urt_topicid_t topicid; //Id of the topic where the data which should be transmitted over the CAN Bus are published on
  urt_osEventMask_t mask; //mask where this subscriber subscribes to (CAN Loop)
  size_t size; //size of the received data

  /**
   * @brief Data which should be transmitted over the CAN Bus
   */
  position_cv_si odom_data;
  urt_osTime_t timestamp; //Timestamp of the data which should be transmitted
}sub_odom_t;
#endif /* (CAN_SUB_ODOM_ENABLE == true) */


#if (CAN_LIGHT_SERVICE_ENABLE == true) || defined(__DOXYGEN__)
typedef struct service{
#if (URT_CFG_RPC_ENABLED == true) || defined(__DOXYGEN__)
  urt_service_t service;
  urt_serviceid_t id;
#endif /* (URT_CFG_RPC_ENABLED == true) */
  /**
   * @brief Data which should be transmitted over the CAN Bus
   */
  union {
    light_led_data_t lightData;
    motion_ev_csi motionData;
  };
}service_t;
#endif /* (CAN_SERVICE_ENABLE == true) */

#if (CAN_REQUEST_ENABLE == true) || defined(__DOXYGEN__)
typedef struct request{
#if (URT_CFG_RPC_ENABLED == true) || defined(__DOXYGEN__)
  urt_nrtrequest_t nrt_request;
  light_led_data_t request_payload;
  urt_service_t* serviceToSignal;
#endif /* (URT_CFG_RPC_ENABLED == true) */

  /**
   * @brief Data which are received from the CANBus
   */
  union {
    light_led_data_t lightData[2];
    simple_motion_t motionData[2];
  };

  /**
   * @brief  Pointer to the request data which should be requested
   */
  union {
    light_led_data_t* data_read_light;
    simple_motion_t* data_read_motion;
  };
}request_t;
#endif /* (CAN_REQUEST_ENABLE == true) */

/**
 * @brief   canLogic node.
 * @struct  canLogic_node
 */
typedef struct can_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, CAN_STACKSIZE);

  /**
   * @brief Filter IDs where the CAN Bus should listen/transmit
   */
  aos_fbcan_filter_t can_filter;

  /**
   * @brief frame ids
   */
  id_map_t filter_map;

  /**
   * @brief used to signal the node when the frame data are transmitted completely over the CAN Bus
   */
  struct {
    urt_osEventSource_t source;
    urt_osEventListener_t listener;
    urt_osEventMask_t mask; //mask where the node loop listens to
  }frame_data;

  /**
   * @brief   Node object.
   */
  urt_node_t node;

#if (CAN_PUB_PROX_FLOOR_ENABLE == true || CAN_PUB_AMB_FLOOR_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief Publisher data of the ambient and proximity floor data.
   * @details 0 = Proximity, 1 = Ambient
   */
  pub_floor_t pub_floor_data[2];
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */

#if (CAN_PUB_PROX_RING_ENABLE == true || CAN_PUB_AMB_RING_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief Publisher data of the ambient and proximity ring data.
   * @details 0 = Proximity, 1 = Ambient
   */
  pub_ring_t pub_ring_data[2];
#endif /* (CAN_PUB_RING_ENABLE == true) */

#if (CAN_PUB_ODOM_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief Publisher data of the odometry data.
   */
  pub_odom_t pub_odom_data;
#endif /* (CAN_PUB_ODOM_ENABLE == true) */

#if (CAN_SUB_PROX_ENABLE == true || CAN_SUB_AMB_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief Subscriber data of the ambient and proximity data.
   * @details 0 = Proximity, 1 = Ambient
   */
  sub_t sub_data[2];
#endif /* (CAN_SUB_PROX_ENABLE == true || CAN_SUB_AMB_ENABLE == true) */

#if (CAN_SUB_ODOM_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief Subscriber data of the odometry data.
   */
  sub_odom_t sub_odom_data;
#endif /* (CAN_SUB_ODOM_ENABLE == true) */

#if (CAN_LIGHT_SERVICE_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief service_data
   */
  service_t light_service_data;
#endif /* (CAN_SERVICE_ENABLE == true) */

#if (CAN_MOTOR_SERVICE_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief service_data
   */
  service_t motor_service_data;
#endif /* (CAN_SERVICE_ENABLE == true) */

#if (CAN_REQUEST_ENABLE == true) || defined(__DOXYGEN__)
  /**
   * @brief request_data
   */
  request_t request_data;
#endif /* (CAN_REQUEST_ENABLE == true) */
} can_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void canLogicInit(can_node_t* can, urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* CANBRIDGE_H */

/** @} */
