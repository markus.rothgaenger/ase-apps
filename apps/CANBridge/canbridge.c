/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    canbridge.c
 * @brief   CANBridge to send and receive the sensor data
 *
 * @addtogroup CANBrdige
 * @{
 */


#include "canbridge.h"

#include <stdlib.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of ring nodes.
 */
static const char _can_name[] = "CANBridge";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

#if (URT_CFG_PUBSUB_ENABLED == true && (CAN_SUB_AMB_ENABLE == true || CAN_SUB_PROX_ENABLE == true))
static void transmitSubscriber(can_node_t* can, int type)
{
  // local variable
  urt_status_t sub_status;

  // fetch NRT
  do {
    sub_status = urtNrtSubscriberFetchNextMessage(&can->sub_data[type].nrt_sub,
                                                  &can->sub_data[type].ring_data.values,
                                                  can->sub_data[type].size,
                                                  &can->sub_data[type].timestamp,
                                                  NULL);


    if (sub_status != URT_STATUS_FETCH_NOMESSAGE) {
      can->filter_map.id = can->sub_data[type].topicid;
      can->filter_map.type = CAN_TOPIC_DATA;
      can->filter_map.cnt = can->sub_data[type].size/sizeof(((CANTxFrame*)0)->data8) +1; //One frame for the timestamp

      while (can->filter_map.cnt != 0) {
        can->filter_map.cnt--;
        // send data from the subscriber over the CAN Bus
        CANTxFrame frame = {
          {
            /* DLC */ sizeof(frame.data8),
            /* RTR */ CAN_RTR_DATA,
            /* IDE */ CAN_IDE_EXT,
          },
          /* ID */ {{}},
          /* data */ {},
        };
        frame.EID = can->filter_map.raw;

        // first frame holds the timestamp
        if (can->filter_map.cnt == 0) {
          memcpy(frame.data8, &can->sub_data[type].timestamp, sizeof(frame.data8));
        } else {
          memcpy(frame.data8, &can->sub_data[type].ring_data.values.data[(can->filter_map.cnt -1)*4], sizeof(frame.data8));
        }

        // send the frame
        aosFBCanTransmitTimeout(&MODULE_HAL_CAN, &frame, TIME_MAX_INTERVAL);
      }
    }
  } while (sub_status != URT_STATUS_FETCH_NOMESSAGE);

  return;
}
#endif /* (URT_CFG_PUBSUB_ENABLED == true && (CAN_SUB_AMB_ENABLE == true || CAN_SUB_PROX_ENABLE == true)) */

#if (URT_CFG_PUBSUB_ENABLED == true && (CAN_SUB_ODOM_ENABLE == true))
static void transmitOdomSubscriber(can_node_t* can)
{
  // local variable
  urt_status_t sub_status;

  // fetch NRT
  do {
    sub_status = urtNrtSubscriberFetchNextMessage(&can->sub_odom_data.nrt_sub,
                                                  &can->sub_odom_data.odom_data,
                                                  can->sub_odom_data.size,
                                                  &can->sub_odom_data.timestamp,
                                                  NULL);


    if (sub_status != URT_STATUS_FETCH_NOMESSAGE) {
      can->filter_map.id = can->sub_odom_data.topicid;
      can->filter_map.type = CAN_TOPIC_DATA;
      can->filter_map.cnt = can->sub_odom_data.size/sizeof(((CANTxFrame*)0)->data8); //One frame for the timestamp

      while (can->filter_map.cnt != 0) {
        can->filter_map.cnt--;
        // send data from the subscriber over the CAN Bus
        CANTxFrame frame = {
          {
            /* DLC */ sizeof(frame.data8),
            /* RTR */ CAN_RTR_DATA,
            /* IDE */ CAN_IDE_EXT,
          },
          /* ID */ {{}},
          /* data */ {},
        };
        frame.EID = can->filter_map.raw;

        // first frame holds the timestamp
        if (can->filter_map.cnt == 0) {
          memcpy(frame.data8, &can->sub_odom_data.timestamp, sizeof(frame.data8));
        } else if (can->filter_map.cnt == 1) {
          memcpy(frame.data8, &can->sub_odom_data.odom_data.location, sizeof(frame.data8));
        } else if (can->filter_map.cnt == 2) {
          memcpy(frame.data8, &can->sub_odom_data.odom_data.oriantation.angle, sizeof(frame.data8));
        }

        // send the frame
        aosFBCanTransmitTimeout(&MODULE_HAL_CAN, &frame, TIME_MAX_INTERVAL);
      }
    }
  } while (sub_status != URT_STATUS_FETCH_NOMESSAGE);

  return;
}
#endif /* (URT_CFG_PUBSUB_ENABLED == true && (CAN_SUB_ODOM_ENABLE == true)) */

#if (URT_CFG_RPC_ENABLED == true && (CAN_MOTOR_SERVICE_ENABLE == true || CAN_LIGHT_SERVICE_ENABLE == true))
static void transmitRequest(can_node_t* can, service_t* service_data)
{
  // local variables
  urt_service_dispatched_t dispatched = urtServiceDispatch(&service_data->service,
                                                           &service_data->motionData,
                                                           sizeof(service_data->motionData));
  urt_service_dispatched_t* disp;
  disp = &dispatched;

  while(disp->request != NULL) {
    can->filter_map.cnt = 0;
    can->filter_map.id = service_data->service.id;
    can->filter_map.type = CAN_REQUEST;

    // send data over the CAN Bus
    CANTxFrame frame = {
      {
        /* DLC */ sizeof(frame.data8),
        /* RTR */ CAN_RTR_DATA,
        /* IDE */ CAN_IDE_EXT,
      },
      /* ID */ {{}},
      /* data */ {},
    };
    frame.EID = can->filter_map.raw;

    if (can->filter_map.id == DMC_SERVICE_ID) {
      // copy the motor data into the frame
      memcpy(&frame.data8[0], &service_data->motionData.translation.axes[0], sizeof(frame.data8));
      memcpy(&frame.data8[4], &service_data->motionData.rotation.vector[2], sizeof(frame.data8));
    } else {
      // copy the light data into the frame
      memcpy(&frame.data8, &service_data->lightData.colors, sizeof(frame.data8));
    }

    id_map_t temp;
    temp.raw = frame.EID;
    // send the frame
    aosFBCanTransmitTimeout(&MODULE_HAL_CAN, &frame, TIME_MAX_INTERVAL);

    // signal the request back
    if (disp->request != NULL) {
      if (urtServiceTryAcquireRequest(&service_data->service, disp) == URT_STATUS_OK) {
        urtServiceRespond(disp);
      }
    }

    urt_service_dispatched_t dispatched = urtServiceDispatch(&service_data->service, &service_data->motionData, sizeof(service_data->motionData));
    disp = &dispatched;
  }

  return;
}
#endif /* (URT_CFG_RPC_ENABLED == true && CAN_MOTOR_SERVICE_ENABLE == true) */

#if (URT_CFG_PUBSUB_ENABLED == true && (CAN_PUB_PROX_RING_ENABLE == true || CAN_PUB_AMB_RING_ENABLE == true))
static void getFrameRingData(const CANRxFrame* frame, can_node_t* can, int type)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  if (can->pub_ring_data[type].data_read == &can->pub_ring_data[type].ring_data[0]) {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_ring_data[type].timestamp[1], frame->data8, sizeof(frame->data8));
    } else {
      memcpy(&can->pub_ring_data[type].ring_data[1].values.data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
    }
  } else {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_ring_data[type].timestamp[0], frame->data8, sizeof(frame->data8));
    } else {
      memcpy(&can->pub_ring_data[type].ring_data[0].values.data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
    }
  }
  if (frame_map.cnt == 0) {
    // set the pointer to the data which should be published
    if (can->pub_ring_data[type].data_read == &can->pub_ring_data[type].ring_data[0]) {
      can->pub_ring_data[type].data_read = &can->pub_ring_data[type].ring_data[1];
    } else {
      can->pub_ring_data[type].data_read = &can->pub_ring_data[type].ring_data[0];
    }

    //Broadcast CANFRAMEEVENT so the node is signaled
    if (type == 0) {
      chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, CAN_PROX_RINGFRAME);
    } else {
      chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, CAN_AMB_RINGFRAME);
    }
  }
  return;
}
#endif /* (URT_CFG_PUBSUB_ENABLED == true && (CAN_PUB_RING_ENABLE == true)) */

#if (URT_CFG_PUBSUB_ENABLED == true && CAN_PUB_ODOM_ENABLE == true)
static void getFrameOdomData(const CANRxFrame* frame, can_node_t* can)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  if (can->pub_odom_data.data_read == &can->pub_odom_data.odom_data[0]) {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_odom_data.timestamp[1], frame->data8, sizeof(frame->data8));
    } else if (frame_map.cnt == 1) {
      memcpy(&can->pub_odom_data.odom_data[1].location, frame->data8, sizeof(frame->data8));
    } else if (frame_map.cnt == 2) {
      memcpy(&can->pub_odom_data.odom_data[1].oriantation.angle, frame->data8, sizeof(frame->data8));
    }
  } else {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_odom_data.timestamp[0], frame->data8, sizeof(frame->data8));
    } else if (frame_map.cnt == 1){
      memcpy(&can->pub_odom_data.odom_data[0].location, frame->data8, sizeof(frame->data8));
    } else if (frame_map.cnt == 2) {
      memcpy(&can->pub_odom_data.odom_data[0].oriantation.angle, frame->data8, sizeof(frame->data8));
    }
  }

  if (frame_map.cnt == 0) {
    // set the pointer to the data which should be published
    if (can->pub_odom_data.data_read == &can->pub_odom_data.odom_data[0]) {
      can->pub_odom_data.data_read = &can->pub_odom_data.odom_data[1];
    } else {
      can->pub_odom_data.data_read = &can->pub_odom_data.odom_data[0];
    }

    //Broadcast CAN_ODOM_FRAME event so the node is signaled
    chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, CAN_ODOM_FRAME);
  }
  return;
}
#endif /* (URT_CFG_PUBSUB_ENABLED == true && CAN_PUB_ODOM_ENABLE == true) */

#if (URT_CFG_PUBSUB_ENABLED == true && (CAN_PUB_PROX_FLOOR_ENABLE == true || CAN_PUB_AMB_FLOOR_ENABLE == true))
static void getFrameFloorData(const CANRxFrame* frame, can_node_t* can, int type)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  if (can->pub_floor_data[type].data_read == &can->pub_floor_data[type].floor_data[0]) {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_floor_data[type].timestamp[1], frame->data8, sizeof(frame->data8));
    } else {
      memcpy(&can->pub_floor_data[type].floor_data[1].values.data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
    }
  } else {
    if (frame_map.cnt == 0) {
      memcpy(&can->pub_floor_data[type].timestamp[0], frame->data8, sizeof(frame->data8));
    } else {
      memcpy(&can->pub_floor_data[type].floor_data[0].values.data[(frame_map.cnt-1)*4], frame->data8, sizeof(frame->data8));
    }
  }

  if (frame_map.cnt == 0) {
    // set the pointer to the data which should be published
    if (can->pub_floor_data[type].data_read == &can->pub_floor_data[type].floor_data[0]) {
      can->pub_floor_data[type].data_read = &can->pub_floor_data[type].floor_data[1];
    } else {
      can->pub_floor_data[type].data_read = &can->pub_floor_data[type].floor_data[0];
    }

    //Broadcast CANFRAMEEVENT so the node is signaled
    if (type == 0) {
      chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, CAN_PROX_FLOORFRAME);
    } else {
      chEvtBroadcastFlagsI(&((can_node_t*)can)->frame_data.source, CAN_AMB_FLOORFRAME);
    }
  }
  return;
}
#endif /* (URT_CFG_PUBSUB_ENABLED == true && (CAN_PUB_FLOOR_ENABLE == true)) */

#if (URT_CFG_RPC_ENABLED == true && (CAN_REQUEST_ENABLE == true))
static void getFrameRequestData(const CANRxFrame* frame, can_node_t* can)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  if (frame_map.id == DMC_SERVICE_ID) {
    //Copy the frame data into the motor data
    if (can->request_data.data_read_motion == &can->request_data.motionData[0]) {
      memcpy(&can->request_data.motionData[1], frame->data8, sizeof(frame->data8));
      can->request_data.data_read_motion = &can->request_data.motionData[0];
    }
    else {
      memcpy(&can->request_data.motionData[0], frame->data8, sizeof(frame->data8));
    }
    if (frame_map.cnt == 0) {
      // set the pointer to the data which should be requested
      if (can->request_data.data_read_motion == &can->request_data.motionData[0]) {
        can->request_data.data_read_motion = &can->request_data.motionData[1];
      } else {
        can->request_data.data_read_motion = &can->request_data.motionData[0];
      }
      chEvtBroadcastFlagsI(&can->frame_data.source, CAN_MOTOR_REQUESTFRAME);
    }

  } else if (frame_map.id == LIGHT_SERVICE_ID) {
    //Copy the frame data into the light data
    if (can->request_data.data_read_light == &can->request_data.lightData[0]) {
      memcpy(&can->request_data.lightData[1].colors, frame->data8, sizeof(frame->data8));
    } else {
      memcpy(&can->request_data.lightData[0].colors, frame->data8, sizeof(frame->data8));
    }

    if (frame_map.cnt == 0) {
      // set the pointer to the data which should be requested
      if (can->request_data.data_read_light == &can->request_data.lightData[0]) {
        can->request_data.data_read_light = &can->request_data.lightData[1];
      } else {
        can->request_data.data_read_light = &can->request_data.lightData[0];
      }
      chEvtBroadcastFlagsI(&can->frame_data.source, CAN_LIGHT_REQUESTFRAME);
    }
  }

  return;
}
#endif /* (URT_CFG_RPC_ENABLED == true && (CAN_REQUEST_ENABLE == true)) */

/**
 * @brief Callback type for FBCAN receive filters
 * @param frame
 * @param params
 */
static void _canCallback(const CANRxFrame* frame, void* can)
{
  id_map_t frame_map;
  frame_map.raw = frame->EID;

  switch (frame_map.type) {
  case CAN_TOPIC_DATA: {
    switch(frame_map.id) {
    case ODOM_TOPICID: {
#if (CAN_PUB_ODOM_ENABLE == true)
      getFrameOdomData(frame, ((can_node_t*)can));
#endif /* (CAN_PUB_ODOM_ENABLE == true) */
      break;
    }
    case PROXIMITY_RING_TOPICID: {
#if (CAN_PUB_PROX_RING_ENABLE == true)
      getFrameRingData(frame, ((can_node_t*)can), (int)0);
#endif /* (CAN_PUB_RING_ENABLE == true) */
      break;
    }
    case AMBIENT_RING_TOPICID: {
#if (CAN_PUB_AMB_RING_ENABLE == true)
      getFrameRingData(frame, ((can_node_t*)can), (int)1);
#endif /* CAN_PUB_AMB_RING_ENABLE == true) */
      break;
    }
    case PROXIMITY_FLOOR_TOPICID: {
#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
      getFrameFloorData(frame, ((can_node_t*)can), (int)0);
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */
      break;
    }
    case AMBIENT_FLOOR_TOPICID: {
#if (CAN_PUB_AMB_FLOOR_ENABLE == true)
      getFrameFloorData(frame, ((can_node_t*)can), (int)1);
#endif /* (CAN_PUB_AMB_FLOOR_ENABLE == true) */
      break;
    }
    default:break;
    }
    break;
  }

  case CAN_REQUEST: {
#if (CAN_REQUEST_ENABLE == true)
    getFrameRequestData(frame, ((can_node_t*)can));
#endif /* (CAN_REQUEST_ENABLE == true) */
    break;
  }
  default: break;
  }

  return;
}


/**
 * @brief   Setup callback function for CAN nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] can    Pointer to the can structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _can_Setup(urt_node_t* node, void* can)
{
  urtDebugAssert(can != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_can_name);

  // set callback of the can filter
  ((can_node_t*)can)->can_filter.callback = _canCallback;
  // add CAN Bus filter to the CAN driver
  aosFBCanAddFilter(&MODULE_HAL_CAN, &((can_node_t*)can)->can_filter);
  ((can_node_t*)can)->frame_data.mask = CANFRAMEEVENT;

#if (URT_CFG_PUBSUB_ENABLED == true)
#if (CAN_SUB_PROX_ENABLE == true)
  // initialize the proximity subscriber
  urtNrtSubscriberInit(&((can_node_t*)can)->sub_data[0].nrt_sub);
  // subscribe to the proximity topic
  urt_topic_t* const sub_prox_topic = urtCoreGetTopic(((can_node_t*)can)->sub_data[0].topicid);
  urtDebugAssert(sub_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&((can_node_t*)can)->sub_data[0].nrt_sub, sub_prox_topic, ((can_node_t*)can)->sub_data[0].mask, NULL);
  // node should also listen to the proximity subscriber mask
  ((can_node_t*)can)->frame_data.mask |= ((can_node_t*)can)->sub_data[0].mask;
#endif /* (CAN_SUB_ENABLE == true) */

#if (CAN_SUB_AMB_ENABLE == true)
  // initialize the ambient subscriber
  urtNrtSubscriberInit(&((can_node_t*)can)->sub_data[1].nrt_sub);
  // subscribe to the ambient topic
  urt_topic_t* const sub_amb_topic = urtCoreGetTopic(((can_node_t*)can)->sub_data[1].topicid);
  urtDebugAssert(sub_amb_topic != NULL);
  urtNrtSubscriberSubscribe(&((can_node_t*)can)->sub_data[1].nrt_sub, sub_amb_topic, ((can_node_t*)can)->sub_data[1].mask, NULL);
  // node should also listen to the ambient subscriber mask
  ((can_node_t*)can)->frame_data.mask |= ((can_node_t*)can)->sub_data[1].mask;
#endif /* (CAN_SUB_AMB_ENABLE == true) */

#if (CAN_SUB_ODOM_ENABLE == true)
  // initialize the odometry subscriber
  urtNrtSubscriberInit(&((can_node_t*)can)->sub_odom_data.nrt_sub);
  // subscribe to the odometry topic
  urt_topic_t* const sub_odom_topic = urtCoreGetTopic(((can_node_t*)can)->sub_odom_data.topicid);
  urtDebugAssert(sub_odom_topic != NULL);
  urtNrtSubscriberSubscribe(&((can_node_t*)can)->sub_odom_data.nrt_sub, sub_odom_topic, ((can_node_t*)can)->sub_odom_data.mask, NULL);
  // node should also listen to the odometry subscriber mask
  ((can_node_t*)can)->frame_data.mask |= ((can_node_t*)can)->sub_odom_data.mask;
#endif /* (CAN_SUB_ODOM_ENABLE == true) */

// initialize the floor publisher
#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
  urtPublisherInit(&((can_node_t*)can)->pub_floor_data[0].pub,
                   urtCoreGetTopic(((can_node_t*)can)->pub_floor_data[0].topicid), NULL);
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */
#if (CAN_PUB_AMB_FLOOR_ENABLE == true)
  urtPublisherInit(&((can_node_t*)can)->pub_floor_data[1].pub,
                   urtCoreGetTopic(((can_node_t*)can)->pub_floor_data[1].topicid), NULL);
#endif /* (CAN_PUB_AMB_FLOOR_ENABLE == true) */

// initialize the odom publisher
#if (CAN_PUB_ODOM_ENABLE == true)
  urtPublisherInit(&((can_node_t*)can)->pub_odom_data.pub,
                   urtCoreGetTopic(((can_node_t*)can)->pub_odom_data.topicid), NULL);
#endif /* (CAN_PUB_ODOM_ENABLE == true) */

// initialize the ring publisher
#if (CAN_PUB_PROX_RING_ENABLE == true)
  urtPublisherInit(&((can_node_t*)can)->pub_ring_data[0].pub,
                   urtCoreGetTopic(((can_node_t*)can)->pub_ring_data[0].topicid), NULL);
#endif /* (CAN_PUB_RING_ENABLE == true) */
#if (CAN_PUB_AMB_RING_ENABLE == true)
  urtPublisherInit(&((can_node_t*)can)->pub_ring_data[1].pub,
                   urtCoreGetTopic(((can_node_t*)can)->pub_ring_data[1].topicid), NULL);
#endif /* (CAN_PUB_PROX_RING_ENABLE == true) */
#endif /* URT_CFG_PUBSUB_ENABLED == true */


#if (URT_CFG_RPC_ENABLED == true)
#if (CAN_LIGHT_SERVICE_ENABLE == true)
  // initialize the light service
  urtServiceInit(&((can_node_t*)can)->light_service_data.service,
                 ((can_node_t*)can)->light_service_data.id,
                 ((can_node_t*)can)->node.thread,
                 CANLIGHTSERVICEEVENT);

  // node should also listen to the service mask
  ((can_node_t*)can)->frame_data.mask |= CANLIGHTSERVICEEVENT;
#endif /* (CAN_SERVICE_ENABLE == true) */
#if (CAN_MOTOR_SERVICE_ENABLE == true)
  // initialize the motor service
  urtServiceInit(&((can_node_t*)can)->motor_service_data.service,
                 ((can_node_t*)can)->motor_service_data.id,
                 ((can_node_t*)can)->node.thread,
                 CANMOTORSERVICEEVENT);

  // node should also listen to the service mask
  ((can_node_t*)can)->frame_data.mask |= CANMOTORSERVICEEVENT;
#endif /* (CAN_MOTOR_SERVICE_ENABLE == true) */

#if (CAN_REQUEST_ENABLE == true)
  // initialize the request
  urtNrtRequestInit(&((can_node_t*)can)->request_data.nrt_request, &((can_node_t*)can)->request_data.request_payload);
#endif /* (CAN_REQUEST_ENABLE == true) */
#endif /* (URT_CFG_RPC_ENABLED == true) */

  // register the queue event to the event listener
  urtEventRegister(&((can_node_t*)can)->frame_data.source, &((can_node_t*)can)->frame_data.listener, CANFRAMEEVENT,
                   CAN_MOTOR_REQUESTFRAME | CAN_LIGHT_REQUESTFRAME | CAN_PROX_FLOORFRAME
                   | CAN_AMB_FLOORFRAME | CAN_PROX_RINGFRAME | CAN_AMB_RINGFRAME | CAN_ODOM_FRAME);

  return ((can_node_t*)can)->frame_data.mask;
}


/**
 * @brief   Loop callback function for CAN nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] can    Pointer to the can structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _can_Loop(urt_node_t* node, urt_osEventMask_t event, void* can)
{
  urtDebugAssert(can != NULL);

  (void)node;

#if (CAN_SUB_PROX_ENABLE == true)
  if (event == ((can_node_t*)can)->sub_data[0].mask) {
    transmitSubscriber(((can_node_t*)can),(int)0);
  }
#endif /*(CAN_SUB_PROX_ENABLE == true)*/

#if (CAN_SUB_AMB_ENABLE == true)
  if (event == ((can_node_t*)can)->sub_data[1].mask) {
    transmitSubscriber(((can_node_t*)can), (int)1);
  }
#endif /*(CAN_SUB_AMB_ENABLE == true)*/

#if (CAN_SUB_ODOM_ENABLE == true)
  if (event == ((can_node_t*)can)->sub_odom_data.mask) {
    transmitOdomSubscriber((can_node_t*)can);
  }
#endif /* (CAN_SUB_ODOM_ENABLE == true) */

#if (CAN_LIGHT_SERVICE_ENABLE == true)
  if (event == CANLIGHTSERVICEEVENT) {
    transmitRequest(((can_node_t*)can), &((can_node_t*)can)->light_service_data);
  }
#endif /* (CAN_SERVICE_ENABLE == true) */

#if (CAN_MOTOR_SERVICE_ENABLE == true)
  if (event == CANMOTORSERVICEEVENT) {
    transmitRequest(((can_node_t*)can), &((can_node_t*)can)->motor_service_data);
  }
#endif /* (CAN_MOTOR_SERVICE_ENABLE == true) */

  if (event == CANFRAMEEVENT) {
    // get flags
    urt_osEventFlags_t flags = urtEventListenerClearFlags(&((can_node_t*)can)->frame_data.listener,
                                                          CAN_PROX_FLOORFRAME | CAN_AMB_FLOORFRAME | CAN_PROX_RINGFRAME
                                                          | CAN_AMB_RINGFRAME | CAN_MOTOR_REQUESTFRAME | CAN_LIGHT_REQUESTFRAME
                                                          | CAN_ODOM_FRAME);

    if (flags & CAN_PROX_FLOORFRAME) {
#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
      chSysLock();
      const floor_data_t floorData = *(((can_node_t*)can)->pub_floor_data[0].data_read);
      int timestamp_count;
      if (&floorData == &((can_node_t*)can)->pub_floor_data[0].floor_data[0]) {
        timestamp_count = 0;
      } else {
        timestamp_count = 1;
      }
      chSysUnlock();
      //publish the floor data on the floor topic
      urtPublisherTryPublish(&((can_node_t*)can)->pub_floor_data[0].pub,
                             &floorData.values,
                             ((can_node_t*)can)->pub_floor_data[0].size,
                             ((can_node_t*)can)->pub_floor_data[0].timestamp[timestamp_count]);
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */
    }

    if (flags & CAN_AMB_FLOORFRAME) {
#if (CAN_PUB_PROX_FLOOR_ENABLE == true)
      chSysLock();
      const floor_data_t floorData = *(((can_node_t*)can)->pub_floor_data[1].data_read);
      int timestamp_count;
      if (&floorData == &((can_node_t*)can)->pub_floor_data[1].floor_data[0]) {
        timestamp_count = 0;
      } else {
        timestamp_count = 1;
      }
      chSysUnlock();
      //publish the floor data on the floor topic
      urtPublisherTryPublish(&((can_node_t*)can)->pub_floor_data[1].pub,
                             &floorData.values,
                             ((can_node_t*)can)->pub_floor_data[1].size,
                             ((can_node_t*)can)->pub_floor_data[1].timestamp[timestamp_count]);
#endif /* (CAN_PUB_FLOOR_ENABLE == true) */
    }

    if (flags & CAN_ODOM_FRAME) {
#if (CAN_PUB_ODOM_ENABLE == true)
      chSysLock();
      const position_cv_si odomData = *(((can_node_t*)can)->pub_odom_data.data_read);
      int timestamp_count;
      if (&odomData == &((can_node_t*)can)->pub_odom_data.odom_data[0]) {
        timestamp_count = 0;
      } else {
        timestamp_count = 1;
      }
      chSysUnlock();
      //publish the odom data on the odom topic
      urtPublisherTryPublish(&((can_node_t*)can)->pub_odom_data.pub,
                             &odomData,
                             ((can_node_t*)can)->pub_odom_data.size,
                             ((can_node_t*)can)->pub_odom_data.timestamp[timestamp_count]);
#endif /* (CAN_PUB_ODOM_ENABLE == true) */
    }

    if (flags & CAN_PROX_RINGFRAME) {
#if (CAN_PUB_PROX_RING_ENABLE == true)
      chSysLock();
      const ring_data_t ringData = *(((can_node_t*)can)->pub_ring_data[0].data_read);
      int timestamp_count;
      if (&ringData == &((can_node_t*)can)->pub_ring_data[0].ring_data[0]) {
        timestamp_count = 0;
      } else {
        timestamp_count = 1;
      }
      chSysUnlock();
      //publish the ring data on the ring topic
      urtPublisherTryPublish(&((can_node_t*)can)->pub_ring_data[0].pub,
                             &ringData.values,
                             ((can_node_t*)can)->pub_ring_data[0].size,
                             ((can_node_t*)can)->pub_ring_data[0].timestamp[timestamp_count]);
#endif /*(CAN_PUB_RING_ENABLE == true)*/
    }

    if (flags & CAN_AMB_RINGFRAME) {
#if (CAN_PUB_PROX_RING_ENABLE == true)
      chSysLock();
      const ring_data_t ringData = *(((can_node_t*)can)->pub_ring_data[1].data_read);
      int timestamp_count;
      if (&ringData == &((can_node_t*)can)->pub_ring_data[1].ring_data[0]) {
        timestamp_count = 0;
      } else {
        timestamp_count = 1;
      }
      chSysUnlock();
      //publish the ring data on the ring topic
      urtPublisherTryPublish(&((can_node_t*)can)->pub_ring_data[1].pub,
                             &ringData.values,
                             ((can_node_t*)can)->pub_ring_data[1].size,
                             ((can_node_t*)can)->pub_ring_data[1].timestamp[timestamp_count]);
#endif /*(CAN_PUB_RING_ENABLE == true)*/
    }

    if (flags & CAN_LIGHT_REQUESTFRAME) {
#if (CAN_REQUEST_ENABLE == true)
      //Signal the service with the nrt request
      if (urtNrtRequestTryAcquire(&((can_node_t*)can)->request_data.nrt_request) == URT_STATUS_OK) {
        chSysLock();
        light_led_data_t* payload = urtNrtRequestGetPayload(&((can_node_t*)can)->request_data.nrt_request);
        *payload = *((can_node_t*)can)->request_data.data_read_light;
        chSysUnlock();

        urtNrtRequestSubmit(&((can_node_t*)can)->request_data.nrt_request,
                            ((can_node_t*)can)->request_data.serviceToSignal,
                            0);
      }
#endif /* (CAN_REQUEST_ENABLE == true) */
    }

    if (flags & CAN_MOTOR_REQUESTFRAME) {
#if (CAN_REQUEST_ENABLE == true)
      //Signal the service with the nrt request
      if (urtNrtRequestTryAcquire(&((can_node_t*)can)->request_data.nrt_request) == URT_STATUS_OK) {   
        chSysLock();
        ((motion_ev_csi*)urtNrtRequestGetPayload(&((can_node_t*)can)->request_data.nrt_request))->translation.axes[0] =
            ((can_node_t*)can)->request_data.data_read_motion->x_translation;
        ((motion_ev_csi*)urtNrtRequestGetPayload(&((can_node_t*)can)->request_data.nrt_request))->rotation.vector[2] =
            ((can_node_t*)can)->request_data.data_read_motion->yaw_rotation;
        chSysUnlock();

        urtNrtRequestSubmit(&((can_node_t*)can)->request_data.nrt_request,
                            ((can_node_t*)can)->request_data.serviceToSignal,
                            0);
      }
#endif /* (CAN_REQUEST_ENABLE == true) */
    }
  }

  return ((can_node_t*)can)->frame_data.mask;
}

/**
 * @brief   Shutdown callback function for can nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] can    Pointer to the can structure.
 *                    Must nor be NULL.
 */
void _can_Shutdown(urt_node_t* node, urt_status_t reason, void* can)
{
  urtDebugAssert(can != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true && CAN_SUB_PROX_ENABLE == true)
  // unsubscribe topics
#if (CAN_SUB_PROX_ENABLE == true)
  urtNrtSubscriberUnsubscribe(&((can_node_t*)can)->sub_data[0].nrt_sub);
#endif /* (CAN_SUB_PROX_ENABLE == true) */
#if (CAN_SUB_AMB_ENABLE == true)
  urtNrtSubscriberUnsubscribe(&((can_node_t*)can)->sub_data[1].nrt_sub);
#endif /* (CAN_SUB_AMB_ENABLE == true) */
#if (CAN_SUB_ODOM_ENABLE == true)
  urtNrtSubscriberUnsubscribe(&((can_node_t*)can)->sub_odom_data.nrt_sub);
#endif /* (CAN_SUB_ODOM_ENABLE == true) */
#endif /* (URT_CFG_PUBSUB_ENABLED == true && CAN_SUB_ENABLE == true) */

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/


void canLogicInit(can_node_t* can, urt_osThreadPrio_t prio)
{
  // initialize the node
  urtNodeInit(&can->node, (urt_osThread_t*)can->thread, sizeof(can->thread), prio,
              _can_Setup, can,
              _can_Loop, can,
              _can_Shutdown, can);

  // set/initialize event data
  urtEventSourceInit(&can->frame_data.source);
  urtEventListenerInit(&can->frame_data.listener);

  return;
}

/** @} */
