/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <linefollowing.h>
#include <amiroos.h>
#include <stdlib.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of linefollowing nodes.
 */
static const char _linefollowing_name[] = "LineFollowing";

/**
 * Line Following Setup
 */
float rpmFuzzyCtrl[2] = {0};
color_t ledColors[8] = {0};

// The different classes (or members) of color discrimination
// BLACK is the line itselfe
// GREY is the boarder between the line and the surface
// WHITE is the common surface
typedef enum{
  BLACK,
  GREY,
  WHITE
}colorMember_t;

enum floorSensorIdx {
  WHEEL_LEFT,
  FRONT_LEFT,
  FRONT_RIGHT,
  WHEEL_RIGHT
};

enum wheelIdx {
  LEFT_WHEEL,
  RIGHT_WHEEL
};

const float rpmForward[2] = {0.1,0};
//Motor controller crashes with rpmSoftLeft and rpmHardLeft
const float rpmSoftLeft[2] = {0.06,0.3};
const float rpmHardLeft[2] = {0.03,0.5};
const float rpmSoftRight[2] = {rpmSoftLeft[0],(-1)*rpmSoftLeft[1]};
const float rpmHardRight[2] = {rpmHardLeft[0],(-1)*rpmHardLeft[1]};
const float rpmTurnLeft[2] = {0, 0.5};
const float rpmTurnRight[2] = {rpmTurnLeft[0],(-1)*rpmTurnLeft[1]};
const float rpmHalt[2] = {0, 0};

// Definition of the fuzzyfication function
//  | Membership
// 1|_B__   G    __W__
//  |    \  /\  /
//  |     \/  \/
//  |_____/\__/\______ Sensor values
// SEE MATLAB SCRIPT "fuzzyRule.m" for adjusting the values
// All values are "raw sensor values"
/* Use these values for white ground surface (e.g. paper) */

const uint16_t blackStartFalling = 9000; // Where the black curve starts falling
const uint16_t blackOff = 15000; // Where no more black is detected
const uint16_t whiteStartRising = 20000; // Where the white curve starts rising
const uint16_t whiteOn = 25000; // Where the white curve has reached the maximum value

const uint16_t greyMax = (whiteOn + blackStartFalling) / 2; // Where grey has its maximum
const uint16_t greyStartRising = blackStartFalling; // Where grey starts rising
const uint16_t greyOff = whiteOn; // Where grey is completely off again

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

void lf_signalLightService(void* linefollowing, color_t* colors) {
  urtDebugAssert(linefollowing != NULL);

  memcpy(&((linefollowing_node_t*)linefollowing)->light_led.data.colors,
         colors,
         sizeof(((linefollowing_node_t*)linefollowing)->light_led.data.colors));

  //Signal the service
  if (urtNrtRequestTryAcquire(&((linefollowing_node_t*)linefollowing)->light_led.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&((linefollowing_node_t*)linefollowing)->light_led.request,
                        ((linefollowing_node_t*)linefollowing)->light_led.service,
                        0);
  }
  return;
}

//Signal the service with the new motor data
void lf_signalMotorService(linefollowing_node_t* linefollowing, float* rpm) {
  urtDebugAssert(linefollowing != NULL);

  linefollowing->motor_data.data.translation.axes[0] = rpm[0];
  linefollowing->motor_data.data.rotation.vector[2] = rpm[1];
  if (urtNrtRequestTryAcquire(&linefollowing->motor_data.request) == URT_STATUS_OK) {
    urtNrtRequestSubmit(&linefollowing->motor_data.request,
                        linefollowing->motor_data.service,
                        0);
  }
  return;
}

void lf_getData(void* linefollowing) {
  urtDebugAssert(linefollowing != NULL);

  //local variables
  urt_status_t status;

  // fetch NRT of the touch sensor data
  do {
    status = urtNrtSubscriberFetchNextMessage(&((linefollowing_node_t*)linefollowing)->touch_data.nrt,
                                              &((linefollowing_node_t*)linefollowing)->touch_data.data,
                                              sizeof(((linefollowing_node_t*)linefollowing)->touch_data.data),
                                              NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  // fetch NRT of the floor proximity data
  do {
    status = urtNrtSubscriberFetchNextMessage(&((linefollowing_node_t*)linefollowing)->floor_prox.nrt,
                                              &((linefollowing_node_t*)linefollowing)->floor_prox.data,
                                              sizeof(((linefollowing_node_t*)linefollowing)->floor_prox.data),
                                              NULL, NULL);
  } while (status != URT_STATUS_FETCH_NOMESSAGE);

  return;
}

// Return the color, which has the highest fuzzy value
colorMember_t lf_getMember(float* fuzzyValue) {
  colorMember_t member;
  if (fuzzyValue[BLACK] > fuzzyValue[GREY] && fuzzyValue[BLACK] > fuzzyValue[WHITE]) {
    member = BLACK;
  } else if (fuzzyValue[GREY] > fuzzyValue[WHITE]) {
    member = GREY;
  } else {
    member = WHITE;
  }

  return member;
}

// Copy the speed from the source to the target array
void copyRpmSpeed(float* source, float* target) {
  target[LEFT_WHEEL] = source[LEFT_WHEEL];
  target[RIGHT_WHEEL] = source[RIGHT_WHEEL];
}

// Get a crisp output for the steering commands
void lf_defuzzyfication(colorMember_t* member, float *rpmFuzzyCtrl) {
  // all sensors are equal
  if (member[WHEEL_LEFT] == member[FRONT_LEFT] &&
      member[FRONT_LEFT] == member[FRONT_RIGHT] &&
      member[FRONT_RIGHT] == member[WHEEL_RIGHT]) {
    // something is wrong -> stop
    copyRpmSpeed(rpmHalt, rpmFuzzyCtrl);
    urtPrintf("HALT: all equal \n");
  // both front sensor detect a line
  } else if (member[FRONT_LEFT] == BLACK &&
      member[FRONT_RIGHT] == BLACK) {
    // straight
    copyRpmSpeed(rpmForward, rpmFuzzyCtrl);
    urtPrintf("FORWARD \n");
  // exact one front sensor detects a line
  } else if (member[FRONT_LEFT] == BLACK ||
             member[FRONT_RIGHT] == BLACK) {
    // soft correction
    if (member[FRONT_LEFT] == GREY) {
      // soft right
      copyRpmSpeed(rpmSoftRight, rpmFuzzyCtrl);
      urtPrintf("SOFT RIGHT \n");
    } else if (member[FRONT_LEFT] == WHITE) {
      // hard right
      copyRpmSpeed(rpmHardRight, rpmFuzzyCtrl);
      urtPrintf("HARD RIGHT \n");
    } else if (member[FRONT_RIGHT] == GREY) {
      // soft left
      copyRpmSpeed(rpmSoftLeft, rpmFuzzyCtrl);
      urtPrintf("SOFT LEFT \n");
    } else if (member[FRONT_RIGHT] == WHITE) {
      // hard left
      copyRpmSpeed(rpmHardLeft, rpmFuzzyCtrl);
      urtPrintf("HARD LEFT \n");
    }
  // both wheel sensors detect a line
  } else if (member[WHEEL_LEFT] == BLACK &&
             member[WHEEL_RIGHT] == BLACK) {
    // something is wrong -> stop
    copyRpmSpeed(rpmHalt, rpmFuzzyCtrl);
    urtPrintf("HALT: wheel line \n");
  // exactly one wheel sensor detects a line
  } else if (member[WHEEL_LEFT] == BLACK ||
             member[WHEEL_RIGHT] == BLACK) {
    if (member[WHEEL_LEFT] == BLACK) {
      // turn left
      copyRpmSpeed(rpmTurnLeft, rpmFuzzyCtrl);
      urtPrintf("TURN LEFT \n");
    } else if (member[WHEEL_RIGHT] == BLACK) {
      // turn right
      copyRpmSpeed(rpmTurnRight, rpmFuzzyCtrl);
      urtPrintf("TURN RIGHT \n");
    }
  // both front sensors may detect a line
  } else if (member[FRONT_LEFT] == GREY &&
             member[FRONT_RIGHT] == GREY) {
    if (member[WHEEL_LEFT] == GREY) {
      // turn left
      copyRpmSpeed(rpmTurnLeft, rpmFuzzyCtrl);
      urtPrintf("TURN LEFT \n");
    } else if (member[WHEEL_RIGHT] == GREY) {
      // turn right
      copyRpmSpeed(rpmTurnRight, rpmFuzzyCtrl);
      urtPrintf("TURN RIGHT \n");
    }
  // exactly one front sensor may detect a line
  } else if (member[FRONT_LEFT] == GREY ||
             member[FRONT_RIGHT] == GREY) {
    if (member[FRONT_LEFT] == GREY) {
      // turn left
      copyRpmSpeed(rpmTurnLeft, rpmFuzzyCtrl);
      urtPrintf("TURN LEFT \n");
    } else if (member[FRONT_RIGHT] == GREY) {
      // turn right
      copyRpmSpeed(rpmTurnRight, rpmFuzzyCtrl);
      urtPrintf("TURN RIGHT \n");
    }
  // both wheel sensors may detect a line
  } else if (member[WHEEL_LEFT] == GREY &&
             member[WHEEL_RIGHT] == GREY) {
    // something is wrong -> stop
    copyRpmSpeed(rpmHalt, rpmFuzzyCtrl);
    urtPrintf("HALT: wheel line \n");
  // exactly one wheel sensor may detect a line
  } else if (member[WHEEL_LEFT] == GREY ||
             member[WHEEL_RIGHT] == GREY) {
    if (member[WHEEL_LEFT] == GREY) {
      // turn left
      copyRpmSpeed(rpmTurnLeft, rpmFuzzyCtrl);
      urtPrintf("TURN LEFT \n");
    } else if (member[WHEEL_RIGHT] == GREY) {
      // turn right
      copyRpmSpeed(rpmTurnRight, rpmFuzzyCtrl);
      urtPrintf("TURN RIGHT \n");
    }
  // no sensor detects anything
  } else {
    // line is lost -> stop
    copyRpmSpeed(rpmHalt, rpmFuzzyCtrl);
    urtPrintf("HALT: line lost \n");
  }

  return;
}

// Fuzzyfication of the sensor values
void lf_fuzzyfication(uint16_t sensorValue, float* fuzziedValue) {
  if (sensorValue < blackStartFalling ) {
    // Only black value
    fuzziedValue[BLACK] = 1.0f;
    fuzziedValue[GREY] = 0.0f;
    fuzziedValue[WHITE] = 0.0f;
  } else if (sensorValue > whiteOn ) {
    // Only white value
    fuzziedValue[BLACK] = 0.0f;
    fuzziedValue[GREY] = 0.0f;
    fuzziedValue[WHITE] = 1.0f;
  } else if ( sensorValue < greyMax) {
    // Some greyisch value between black and grey

    // Black is going down
    if ( sensorValue > blackOff) {
      fuzziedValue[BLACK] = 0.0f;
    } else {
      fuzziedValue[BLACK] = (((float) (sensorValue-blackOff)) / ((float) (blackStartFalling-blackOff)));
    }
    // Grey is going up
    if ( sensorValue < greyStartRising) {
      fuzziedValue[GREY] = 0.0f;
    } else {
      fuzziedValue[GREY] = (((float) (sensorValue-greyStartRising)) / ((float) (greyMax-greyStartRising)));
    }
    // White is absent
    fuzziedValue[WHITE] = 0.0f;

  } else if ( sensorValue >= greyMax) {
    // Some greyisch value between grey and white

    // Black is absent
    fuzziedValue[BLACK] = 0.0f;
    // Grey is going down
    if ( sensorValue < greyOff) {
      fuzziedValue[GREY] = (((float) (sensorValue-greyOff)) / ((float) (greyMax-greyOff)));
    } else {
      fuzziedValue[GREY] = 0.0f;
    }
    // White is going up
    if ( sensorValue < whiteStartRising) {
      fuzziedValue[WHITE] = 0.0f;
    } else {
      fuzziedValue[WHITE] = (((float) (sensorValue-whiteStartRising)) / ((float) (whiteOn-whiteStartRising)));
    }
  }

  return;
}

void lf_lineFollowing(linefollowing_node_t* linefollowing)
{
  // FUZZYFICATION
  // First we need to get the fuzzy value for our 3 values {BLACK, GREY, WHITE}
  uint16_t leftWheelFuzzyMemberValues[3], leftFrontFuzzyMemberValues[3], rightFrontFuzzyMemberValues[3], rightWheelFuzzyMemberValues[3];
  lf_fuzzyfication(linefollowing->floor_prox.data.values.left_wheel, leftWheelFuzzyMemberValues);
  lf_fuzzyfication(linefollowing->floor_prox.data.values.left_front, leftFrontFuzzyMemberValues);
  lf_fuzzyfication(linefollowing->floor_prox.data.values.right_front, rightFrontFuzzyMemberValues);
  lf_fuzzyfication(linefollowing->floor_prox.data.values.right_wheel, rightWheelFuzzyMemberValues);

  // INFERENCE RULE DEFINITION
  // Get the member for each sensor
  colorMember_t member[4];
  member[WHEEL_LEFT] = lf_getMember(leftWheelFuzzyMemberValues);
  member[FRONT_LEFT] = lf_getMember(leftFrontFuzzyMemberValues);
  member[FRONT_RIGHT] = lf_getMember(rightFrontFuzzyMemberValues);
  member[WHEEL_RIGHT] = lf_getMember(rightWheelFuzzyMemberValues);

  // DEFUZZYFICATION
  lf_defuzzyfication(member, rpmFuzzyCtrl);
  return;
}

urt_osEventMask_t _linefollowing_Setup(urt_node_t* node, void* linefollowing)
{
  urtDebugAssert(linefollowing != NULL);
  (void)node;

  // set thread name
  chRegSetThreadName(_linefollowing_name);

#if (URT_CFG_PUBSUB_ENABLED == true)
  // subscribe to the floor proximity topic
  urt_topic_t* const floor_prox_topic = urtCoreGetTopic(((linefollowing_node_t*)linefollowing)->floor_prox.topicid);
  urtDebugAssert(floor_prox_topic != NULL);
  urtNrtSubscriberSubscribe(&((linefollowing_node_t*)linefollowing)->floor_prox.nrt, floor_prox_topic, PROXFLOOREVENT, NULL);

  //Subscribe to the touch sensor topic
  urt_topic_t* const touch_sensor_topic = urtCoreGetTopic(((linefollowing_node_t*)linefollowing)->touch_data.topicid);
  urtDebugAssert(touch_sensor_topic != NULL);
  urtNrtSubscriberSubscribe(&((linefollowing_node_t*)linefollowing)->touch_data.nrt, touch_sensor_topic, TOUCHEVENT, NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  return PROXFLOOREVENT | TOUCHEVENT;
}

/**
 * @brief   Loop callback function for linefollowing nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] linefollowing    Pointer to the maze structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _linefollowing_Loop(urt_node_t* node, urt_osEventMask_t event, void* linefollowing)
{
  urtDebugAssert(linefollowing != NULL);
  (void)node;
  (void)event;

  // get the proximity and ambient data of the ring and floor sensors
  lf_getData(linefollowing);

  switch (((linefollowing_node_t*)linefollowing)->state) {
  case LF_IDLE:
    if (((linefollowing_node_t*)linefollowing)->touch_data.data.values.data[1] == 1
        && ((linefollowing_node_t*)linefollowing)->touch_data.data.values.data[2] == 1) {
      urtPrintf("LINEFOLLOWING\n");
      ((linefollowing_node_t*)linefollowing)->state = LF_LINEFOLLOWING;
      ledColors[3] = GREEN;
      ledColors[4] = GREEN;
      ledColors[5] = OFF;
      lf_signalLightService(linefollowing, ledColors);
    }
    break;
  case LF_LINEFOLLOWING:
    if (((linefollowing_node_t*)linefollowing)->touch_data.data.values.data[0] == 1
        && ((linefollowing_node_t*)linefollowing)->touch_data.data.values.data[3] == 1) {
      urtPrintf("IDLE\n");
      copyRpmSpeed(rpmHalt, rpmFuzzyCtrl);
      lf_signalMotorService(linefollowing, rpmFuzzyCtrl);
      ledColors[3] = OFF;
      ledColors[4] = RED;
      ledColors[5] = RED;
      lf_signalLightService(linefollowing, ledColors);
      ((linefollowing_node_t*)linefollowing)->state = LF_IDLE;
    } else {
      lf_lineFollowing((linefollowing_node_t*)linefollowing);
      lf_signalMotorService(linefollowing, rpmFuzzyCtrl);
      urtThreadMSleep(500);
    }
    break;
  default:break;
  }

  return PROXFLOOREVENT | TOUCHEVENT;
}

/**
 * @brief   Shutdown callback function for maze nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] maze    Pointer to the maze structure.
 *                    Must nor be NULL.
 */
void _linefollowing_Shutdown(urt_node_t* node, urt_status_t reason, void* linefollowing)
{
  urtDebugAssert(linefollowing != NULL);

  (void)node;
  (void)reason;

#if (URT_CFG_PUBSUB_ENABLED == true)
  // unsubscribe from topics
  urtNrtSubscriberUnsubscribe(&((linefollowing_node_t*)linefollowing)->floor_prox.nrt);
  urtNrtSubscriberUnsubscribe(&((linefollowing_node_t*)linefollowing)->touch_data.nrt);
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  return;
}



/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void linefollowingInit(linefollowing_node_t* linefollowing,
                    urt_osThreadPrio_t prio)
{
  urtDebugAssert(linefollowing != NULL);

#if (URT_CFG_PUBSUB_ENABLED == true)
  urtNrtSubscriberInit(&linefollowing->floor_prox.nrt);
  urtNrtSubscriberInit(&linefollowing->touch_data.nrt);
#endif /* URT_CFG_PUBSUB_ENABLED == true */
#if (URT_CFG_RPC_ENABLED == true)
  urtNrtRequestInit(&linefollowing->motor_data.request, &linefollowing->motor_data.data);
  urtNrtRequestInit(&linefollowing->light_led.request, &linefollowing->light_led.data);
#endif /* (URT_CFG_RPC_ENABLED == true) */

  linefollowing->state = LF_IDLE;

  // initialize the node
  urtNodeInit(&linefollowing->node, (urt_osThread_t*)linefollowing->thread, sizeof(linefollowing->thread), prio,
              _linefollowing_Setup, linefollowing,
              _linefollowing_Loop, linefollowing,
              _linefollowing_Shutdown, linefollowing);

  return;
}


