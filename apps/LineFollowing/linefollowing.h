/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    linefollowing.h
 * @brief   A application that let the AMiRo follow a line.
 *
 * @addtogroup LineFollowing
 * @{
 */

#ifndef LINEFOLLOWING_H
#define LINEFOLLOWING_H

#include <urt.h>
#include "../../messagetypes/DWD_floordata.h"
#include "../../messagetypes/motiondata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/positiondata.h"
#include "../../messagetypes/TouchSensordata.h"


/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(LINEFOLLOWING_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of linefollowing threads.
 */
#define LINEFOLLOWING_STACKSIZE             512
#endif /* !defined(LINEFOLLOWING_STACKSIZE) */

#define PROXFLOOREVENT                (urt_osEventMask_t)(1<< 1)
#define TOUCHEVENT                    (urt_osEventMask_t)(1<< 2)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief Line Following states
 */
typedef enum lf_state {
  LF_IDLE,
  LF_LINEFOLLOWING
}lf_state_t;

/**
* @brief   Trigger related data of the floor sensors.
*/
typedef struct lf_floor{
 urt_topicid_t topicid;
 urt_nrtsubscriber_t nrt;
 floor_sensors_t data;
}lf_floor_t;

/**
* @brief  Data of the motor
*/
typedef struct lf_motor {
  motion_ev_csi data;
  urt_nrtrequest_t request;
  urt_service_t* service;
}lf_motor_t;

/**
* @brief  Data of the light
*/
typedef struct lf_light {
  urt_nrtrequest_t request;
  urt_service_t* service;
  light_led_data_t data;
}lf_light_t;

/**
* @brief  Data of the touch sensors
*/
typedef struct lf_touchsensors {
  urt_topicid_t topicid;
  urt_nrtsubscriber_t nrt;
  touch_data_t data;
}lf_touchsensors_t;


/**
 * @brief   LineFollowing node.
 * @struct  linefollowing_node
 */
typedef struct linefollowing_node {

  URT_THREAD_MEMORY(thread, LINEFOLLOWING_STACKSIZE);
  urt_node_t node;

  lf_floor_t floor_prox;
  lf_motor_t motor_data;
  lf_light_t light_led;
  lf_touchsensors_t touch_data;
  lf_state_t state;
} linefollowing_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void linefollowingInit(linefollowing_node_t* linefollowing,urt_osThreadPrio_t prio);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* LINEFOLLOWING_H */

/** @} */
