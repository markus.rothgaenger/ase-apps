/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    helloworld.h
 * @brief   A simple "Hello world!" application.
 *
 * @addtogroup HelloWorld
 * @{
 */

#ifndef RING_H
#define RING_H

#include <urt.h>
#include "../../messagetypes/ProximitySensordata.h"
#include "../../messagetypes/LightRing_leddata.h"
#include "../../messagetypes/motiondata.h"

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

#if !defined(RING_STACKSIZE) || defined(__DOXYGEN__)
/**
 * @brief   Stack size of ring threads.
 */
#define RING_STACKSIZE             256
#endif /* !defined(RING_STACKSIZE) */

/**
 * @brief   Event flag to identify trigger events related to publish-subscribe.
 */
#define TRIGGERFLAG_RING           (urt_osEventFlags_t)(1 << 0)

/**
 * @brief   Event mask to set on a trigger event.
 */
#define TRIGGEREVENT                (urt_osEventMask_t)(1<< 1)

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Ring node.
 * @struct  ring_node
 */
typedef struct ring_node {
  /**
   * @brief   Thread memory.
   */
  URT_THREAD_MEMORY(thread, RING_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;


  /**
   * @brief   Driver related data.
   */
  struct {
    /**
     * @brief  driver for SSE,SSW,WSW,WNW
     */
    VCNL4020Driver* driverLeft;

    /**
     * @brief  mux for SSE,SSW,WSW,WNW
     */
    PCA9544ADriver* muxLeft;

    /**
     * @brief  driver for NNW,NNE,ENE,ESE
     */
    VCNL4020Driver* driverRight;

    /**
     * @brief  mux for NNW,NNE,ENE,ESE
     */
    PCA9544ADriver* muxRight;

    /**
     * @brief timeout of the functions
     */
    apalTime_t timeout;

    /**
     * @brief node frequency in Hz.
     */
    float frequency;

    /**
     * @brief Proximity data of the vcnl4020 driver
     */
    ring_sensors_t prox_data;

    /**
     * @brief Ambient data of the vcnl4020 driver
     */
    ring_sensors_t amb_data;
  } driver_data;

  /**
   * @brief   Trigger related data.
   */
  struct {
    /**
     * @brief   Pointer to the trigger event source.
     */
    urt_osEventSource_t source;

    /**
     * @brief   Event listener for trigger events.
     */
    urt_osEventListener_t listener;

    /**
     * @brief   Timer to trigger ring data.
     */
    aos_timer_t timer;
  } trigger;

#if (URT_CFG_PUBSUB_ENABLED == true) || defined(__DOXYGEN__)
  /**
   * @brief   Publisher to publish the proximity/ambient ring data.
   */
  urt_publisher_t prox_publisher;
  urt_publisher_t amb_publisher;
#endif /* (URT_CFG_PUBSUB_ENABLED == true) */

  int callback_test;
} ring_node_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined(__cplusplus)
extern "C" {
#endif /* defined(__cplusplus) */
  void ringInit(ring_node_t* ring, urt_topicid_t amb_topicid, urt_topicid_t prox_topicid, urt_osThreadPrio_t prio);
  void getProximityTest(BaseSequentialStream *stream, ring_node_t *ring);
#if defined(__cplusplus)
}
#endif /* defined(__cplusplus) */

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* RING_H */

/** @} */
