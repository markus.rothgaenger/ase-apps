/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <ring.h>
#include <amiroos.h>
#include <stdlib.h>

/******************************************************************************/
/* LOCAL DEFINITIONS                                                          */
/******************************************************************************/

/******************************************************************************/
/* EXPORTED VARIABLES                                                         */
/******************************************************************************/

/******************************************************************************/
/* LOCAL TYPES                                                                */
/******************************************************************************/

/******************************************************************************/
/* LOCAL VARIABLES                                                            */
/******************************************************************************/

/**
 * @brief   Name of ring nodes.
 */
static const char _ring_name[] = "Ring";

/******************************************************************************/
/* LOCAL FUNCTIONS                                                            */
/******************************************************************************/

uint8_t* mapProxRGB(uint16_t prox_value) {
  static uint8_t rgb[3];
  prox_value = prox_value/1000;

  rgb[0] = 0.04* (prox_value*prox_value) + 100 > 255 ? 255 : 0.04* (prox_value*prox_value) + 100;
  rgb[1] = (-0.04)* (prox_value*prox_value) + 170 > 255 ? 255 : (-0.04)* (prox_value*prox_value) + 170;
  rgb[2] = 0; //Blue value is always 0

  return rgb;
}

/**
 * @brief   Callback function for the trigger timer.
 *
 * @param[in] flags   Flags to emit for the trigger event.
 *                    Pointer is reinterpreted as integer value.
 */
static void _ring_triggercb(void* params)
{
  (void)params;

  // broadcast ring event
  chEvtBroadcastFlagsI(&((ring_node_t*)params)->trigger.source, (urt_osEventFlags_t)0);

  return;
}

/**
 * @brief   Setup callback function for ring nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] ring    Pointer to the ring structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _ring_Setup(urt_node_t* node, void* ring)
{
  urtDebugAssert(ring != NULL);
  (void) node;

  // set thread name
  chRegSetThreadName(_ring_name);

  //Power testing
  bq241xx_lld_enable_t en[3];
  bq241xx_lld_get_enabled(&moduleLldBatteryChargerFront, &en[0]);
  bq241xx_lld_set_enabled(&moduleLldBatteryChargerFront, (en[0] == BQ241xx_LLD_ENABLED) ? BQ241xx_LLD_DISABLED : BQ241xx_LLD_ENABLED);
  bq241xx_lld_get_enabled(&moduleLldBatteryChargerRear, &en[0]);
  bq241xx_lld_set_enabled(&moduleLldBatteryChargerRear, (en[0] == BQ241xx_LLD_ENABLED) ? BQ241xx_LLD_DISABLED : BQ241xx_LLD_ENABLED);

  vcnl4020_lld_proxratereg_t proxrate;
  if (((ring_node_t*)ring)->driver_data.frequency <= 1.95) {
    proxrate = VCNL4020_LLD_PROXRATEREG_1_95_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 3.90625) {
    proxrate = VCNL4020_LLD_PROXRATEREG_3_90625_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 7.8125) {
    proxrate = VCNL4020_LLD_PROXRATEREG_7_8125_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 15.625) {
    proxrate = VCNL4020_LLD_PROXRATEREG_15_625_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 31.25) {
    proxrate = VCNL4020_LLD_PROXRATEREG_31_25_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 62.5) {
    proxrate = VCNL4020_LLD_PROXRATEREG_62_5_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 125) {
    proxrate = VCNL4020_LLD_PROXRATEREG_125_HZ;
  } else if (((ring_node_t*)ring)->driver_data.frequency <= 250) {
    proxrate = VCNL4020_LLD_PROXRATEREG_250_HZ;
  }

  //Start and initialize every led from the "left" side
  for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {
    pca9544a_lld_setchannel(((ring_node_t*)ring)->driver_data.muxLeft, channel, MICROSECONDS_PER_SECOND);
    // set driver frequency
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverLeft,
                          VCNL4020_LLD_REGADDR_PROXRATE,
                          proxrate,
                          ((ring_node_t*)ring)->driver_data.timeout);
    // set led current
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverLeft,
                          VCNL4020_LLD_REGADDR_LEDCURRENT,
                          VCNL4020_LLD_LEDCURRENTREG_200_mA,
                          ((ring_node_t*)ring)->driver_data.timeout);
    // start led
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverLeft,
                          VCNL4020_LLD_REGADDR_CMD,
                          (VCNL4020_LLD_CMDREG_ALSEN | VCNL4020_LLD_CMDREG_PROXEN | VCNL4020_LLD_CMDREG_SELFTIMED),
                          ((ring_node_t*)ring)->driver_data.timeout);
  }

  //Start and initialize every led from the "right" side
  for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {
    pca9544a_lld_setchannel(((ring_node_t*)ring)->driver_data.muxRight, channel, MICROSECONDS_PER_SECOND);
    // set driver frequency
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverRight,
                          VCNL4020_LLD_REGADDR_PROXRATE,
                          proxrate,
                          ((ring_node_t*)ring)->driver_data.timeout);
    // set led current
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverRight,
                          VCNL4020_LLD_REGADDR_LEDCURRENT,
                          VCNL4020_LLD_LEDCURRENTREG_200_mA,
                          ((ring_node_t*)ring)->driver_data.timeout);
    // start led
    vcnl4020_lld_writereg(((ring_node_t*)ring)->driver_data.driverRight,
                          VCNL4020_LLD_REGADDR_CMD,
                          (VCNL4020_LLD_CMDREG_ALSEN | VCNL4020_LLD_CMDREG_PROXEN | VCNL4020_LLD_CMDREG_SELFTIMED),
                          ((ring_node_t*)ring)->driver_data.timeout);
  }

  // register trigger event
  urtEventRegister(&((ring_node_t*)ring)->trigger.source, &((ring_node_t*)ring)->trigger.listener, TRIGGEREVENT, 0);

  // activate the timer
  aosTimerPeriodicInterval(&((ring_node_t*)ring)->trigger.timer, (1.0f / ((ring_node_t*)ring)->driver_data.frequency) * MICROSECONDS_PER_SECOND, _ring_triggercb, ring);

  return TRIGGEREVENT;
}

/**
 * @brief   Loop callback function for ring nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] ring  Pointer to the ring structure.
 *                    Must nor be NULL.
 *
 * @return  Event mask to listen for next.
 */
urt_osEventMask_t _ring_Loop(urt_node_t* node, urt_osEventMask_t event, void* ring)
{
  urtDebugAssert(ring != NULL);

  (void)node;

  ((ring_node_t*)ring)->callback_test += 1;

  switch(event) {
    case TRIGGEREVENT:
    {
      //TODO: 2 threads for reading data here (more efficient). 1 for left, 1 for right

      //Get data for every led on the "left" and "right" side
      for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {

        int realChannelRight = channel;
        int realChannelLeft = channel;
        switch(channel) {
        case 0: {
          realChannelLeft = 4;
          realChannelRight = 0;
          break;
        }
        case 1: {
          realChannelLeft = 3;
          realChannelRight = 7;
          break;
        }
        case 2: {
          realChannelLeft = 1;
          realChannelRight = 5;
          break;
        }
        case 3: {
          realChannelLeft = 2;
          realChannelRight = 6;
          break;
        }
        default: break;
        }

        pca9544a_lld_setchannel(((ring_node_t*)ring)->driver_data.muxLeft, channel, MICROSECONDS_PER_SECOND);
        vcnl4020_lld_readalsandprox(((ring_node_t*)ring)->driver_data.driverLeft, &((ring_node_t*)ring)->driver_data.amb_data.data[realChannelLeft], &((ring_node_t*)ring)->driver_data.prox_data.data[realChannelLeft], ((ring_node_t*)ring)->driver_data.timeout);

        pca9544a_lld_setchannel(((ring_node_t*)ring)->driver_data.muxRight, channel, MICROSECONDS_PER_SECOND);
        vcnl4020_lld_readalsandprox(((ring_node_t*)ring)->driver_data.driverRight, &((ring_node_t*)ring)->driver_data.amb_data.data[realChannelRight], &((ring_node_t*)ring)->driver_data.prox_data.data[realChannelRight], ((ring_node_t*)ring)->driver_data.timeout);
      }

      // publish the proximity and ambient data
      urtPublisherTryPublish(&((ring_node_t*)ring)->prox_publisher,
                             &((ring_node_t*)ring)->driver_data.prox_data,
                             sizeof(((ring_node_t*)ring)->driver_data.prox_data),
                             urtTimeNow());
      urtPublisherTryPublish(&((ring_node_t*)ring)->amb_publisher, &((ring_node_t*)ring)->driver_data.amb_data, sizeof(((ring_node_t*)ring)->driver_data.amb_data), urtTimeNow());

      break;
    }
    default: break;
  }

  return TRIGGEREVENT;
}

/**
 * @brief   Shutdown callback function for ring nodes.
 *
 * @param[in] node    Pointer to the node object.
 *                    Must not be NULL.
 * @param[in] master  Pointer to the ring structure.
 *                    Must nor be NULL.
 */
void _ring_Shutdown(urt_node_t* node, urt_status_t reason, void* ring)
{
  urtDebugAssert(ring != NULL);

  (void)node;
  (void)reason;

  // unregister trigger event
  urtEventUnregister(&((ring_node_t*)ring)->trigger.source, &((ring_node_t*)ring)->trigger.listener);

  return;
}

/******************************************************************************/
/* EXPORTED FUNCTIONS                                                         */
/******************************************************************************/

void getProximityTest(BaseSequentialStream* stream, ring_node_t* ring)
{  
  uint16_t amb, prox;

//  for (int channel = PCA9544A_LLD_CH0; channel <= PCA9544A_LLD_CH3; channel++) {
  int channel = 0;
  pca9544a_lld_setchannel(((ring_node_t*)ring)->driver_data.muxLeft, channel, MICROSECONDS_PER_SECOND);
  vcnl4020_lld_readalsandprox(((ring_node_t*)ring)->driver_data.driverLeft, &amb, &prox, ((ring_node_t*)ring)->driver_data.timeout);
  chprintf(stream, "channel: 0x%04X AMB %u \t PROX %u \n", channel, amb, prox);
  return;
}

void ringInit(ring_node_t* ring, urt_topicid_t amb_topicid, urt_topicid_t prox_topicid, urt_osThreadPrio_t prio)
{
  urtDebugAssert(ring != NULL);

  // set/initialize event data
  urtEventSourceInit(&ring->trigger.source);
  urtEventListenerInit(&ring->trigger.listener);
  // initialize the timer
  urtTimerInit(&ring->trigger.timer);
#if (URT_CFG_PUBSUB_ENABLED == true)
  // initialize the publisher for the proximity/ambient ring data
  urtPublisherInit(&ring->prox_publisher, urtCoreGetTopic(prox_topicid), NULL);
  urtPublisherInit(&ring->amb_publisher, urtCoreGetTopic(amb_topicid), NULL);
#endif /* URT_CFG_PUBSUB_ENABLED == true */

  // set the driver
  ring->driver_data.driverLeft = &moduleLldProximity1;
  ring->driver_data.driverRight = &moduleLldProximity2;
  // set the mux
  ring->driver_data.muxLeft = &moduleLldI2cMultiplexer1;
  ring->driver_data.muxRight = &moduleLldI2cMultiplexer2;
  // set the timeout
  ring->driver_data.timeout = MICROSECONDS_PER_SECOND;

  // initialize the node
  urtNodeInit(&ring->node, (urt_osThread_t*)ring->thread, sizeof(ring->thread), prio,
              _ring_Setup, ring,
              _ring_Loop, ring,
              _ring_Shutdown, ring);
}
