/*
AMiRo-Apps is a collection of applications for the Autonomous Mini Robot (AMiRo) platform.
Copyright (C) 2018..2021  Thomas Schöpping et al.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * @file    differentialmotionestimator.h
 * @brief   Motion estimator for differential kinematics.
 *
 * @addtogroup DifferentialMotionEstimator
 * @{
 */

#ifndef DifferentialMotionEstimator_H
#define DifferentialMotionEstimator_H

#include <motiondata.h>
#include <amiroos.h>
#include <urt.h>

/******************************************************************************/
/* CONSTANTS                                                                  */
/******************************************************************************/

/**
 * @brief   Size of the DME node thread stack.
 */
#define DME_STACKSIZE                             256

/******************************************************************************/
/* SETTINGS                                                                   */
/******************************************************************************/

/******************************************************************************/
/* CHECKS                                                                     */
/******************************************************************************/

#if (MOTIONDATA_AXES < 3)
# error "MOTIONDATA_AXES is expected to be 3 or greater."
#endif

/******************************************************************************/
/* DATA STRUCTURES AND TYPES                                                  */
/******************************************************************************/

/**
 * @brief   Payload type for motion messages.
 */
typedef motion_ev_csi dme_motionpayload_t;

/**
 * @brief   QEI descriptor type.
 */
struct dme_qeidescriptor {
  /**
   * @brief   Pointer to QEI driver.
   */
  apalQEIDriver_t* driver;

  /**
   * @brief   QEI increments per wheel revolution.
   */
  apalQEICount_t increments_per_revolution;
};

/**
 * @brief   Wheel descriptor type.
 */
struct dme_wheeldescriptor {
  /**
   * @brief   Cirumference of the wheel in µm.
   */
  uint32_t circumference;

  /**
   * @brief   Spacial offset of the wheel from the center of motion in µm.
   */
  uint32_t offset;

  /**
   * @brief   QEI related information.
   */
  struct dme_qeidescriptor qei;
};

/**
 * @brief   QEI data.
 */
struct dme_qeidata {
  /**
   * @brief   Range of the QEI.
   */
  apalQEICount_t range;

  /**
   * @brief   Most recently measured position.
   */
  apalQEICount_t position;
};

/**
 * @brief   DME configuration structure.
 *
 * @struct dme_config
 */
typedef struct dme_config {
  /**
   * @brief   Configuration for the left wheel.
   */
  struct dme_wheeldescriptor left;

  /**
   * @brief   Configuration for the right wheel.
   */
  struct dme_wheeldescriptor right;

  /**
   * @brief   Frequency as which to measure QEI input.
   */
  urt_delay_t interval;
} dme_config_t;

/**
 * @brief   Differfential Motion Estimator (DME) structure.
 *
 * @struct dme
 */
typedef struct dme {
  /**
   * @brief   Node thread memory.
   */
  URT_THREAD_MEMORY(thread, DME_STACKSIZE);

  /**
   * @brief   Node object.
   */
  urt_node_t node;

  /**
   * @brief   Publisher to privide motion information.
   */
  urt_publisher_t publisher;

  /**
   * @brief   Timer to trigger periodic measurements.
   */
  aos_timer_t trigger;

  /**
   * @brief   QEI related data.
   */
  struct {
    /**
     * @brief   Time of the most recent measurement.
     */
    urt_osTime_t time;

    /**
     * @brief   QEI information for the left wheel.
     */
    struct dme_qeidata left;

    /**
     * @brief   QEI information for the right wheel.
     */
    struct dme_qeidata right;

    int callback;
  } qei;

  /**
   * @brief   Pointer to the associated configuration.
   */
  const dme_config_t* config;

  float callback; //Callback for testing
} dme_t;

/******************************************************************************/
/* MACROS                                                                     */
/******************************************************************************/

/******************************************************************************/
/* EXTERN DECLARATIONS                                                        */
/******************************************************************************/

#if defined (__cplusplus)
extern "C" {
#endif
  void dmeInit(dme_t* dme, const dme_config_t* config, urt_topicid_t topic, urt_osThreadPrio_t prio);
#if defined (__cplusplus)
}
#endif

/******************************************************************************/
/* INLINE FUNCTIONS                                                           */
/******************************************************************************/

/******************************************************************************/
/* SUBSYSTEMS                                                                 */
/******************************************************************************/

#endif /* DifferentialMotionEstimator_H */

/** @} */
